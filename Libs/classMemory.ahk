﻿#NoEnv

ProcessName = Calculator.exe         ; Имя процесса.
Address = 0x0042E1CE              ; Адрес, куда писать.
Data = 215125125                  ; Что писать.
Size = 4                          ; Сколько байт писать.

VarSetCapacity(Buf, Size, 0)      ; Буфер, для записываемых данных.

NumPut(Data, Buf, 0, "UInt")      ; Данные - в буфер.

PROCESS_VM_WRITE = 0x20           ; Права на процесс.
PROCESS_VM_OPERATION = 0x8

Process, Exist, %ProcessName%     ; Поиск процесса.

If(!ErrorLevel) {
  MsgBox, Процесс не найден.
  ExitApp
}

PID := ErrorLevel                 ; Идентификатор процесса будет в ErrorLevel.

hProcess := DllCall("OpenProcess", "UInt", PROCESS_VM_WRITE | PROCESS_VM_OPERATION
                                 , "Int",  False
                                 , "UInt", PID)
If(!hProcess) {
  MsgBox, Не удалось открыть процесс.
  ExitApp
}

Ret := DllCall("WriteProcessMemory", "UInt", hProcess
                                   , "UInt", Address
                                   , "UInt", &Buf
                                   , "UInt", Size
                                   , "UInt", 0)

DllCall("CloseHandle", "UInt", hProcess) ; Освобождение хэндла процесса.

If(!Ret) {
  MsgBox, Не удалось записать.
  ExitApp
}

MsgBox, Готово.