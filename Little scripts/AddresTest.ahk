F1::
    Try
        SetActorHP(99)
    Catch e
        MsgBox,, Error, % e.Message
    return

SetActorHP(value)
{
    baseAddress := ProcessReadMemory(0xB7CD98, "gta_sa.exe", "Ptr", A_PtrSize)
    ProcessWriteMemory(value, baseAddress + 0x2E4, "gta_sa.exe", "Float")
}


ProcessReadMemory(address, processIDorName, type := "Int", numBytes := 4) {
    VarSetCapacity(buf, numBytes, 0)

    Process Exist, %processIDorName%
    if !processID := ErrorLevel
        throw Exception("Invalid process name or process ID:`n`n""" . processIDorName . """")

    if !processHandle := DllCall("OpenProcess", "Int", 24, "UInt", 0, "UInt", processID, "Ptr")
        throw Exception("Failed to open process.`n`nError code:`t" . A_LastError)

    result := DllCall("ReadProcessMemory", "Ptr", processHandle, "Ptr", address, "Ptr", &buf, "Ptr", numBytes, "PtrP", numBytesRead, "UInt")

    if !DllCall("CloseHandle", "Ptr", processHandle, "UInt") && !result
        throw Exception("Failed to close process handle.`n`nError code:`t" . A_LastError)

    if !result
        throw Exception("Failed to read process memory.`n`nError code:`t" . A_LastError)

    if !numBytesRead
        throw Exception("Read 0 bytes from the`n`nprocess:`t" . processIDorName . "`naddress:`t" . address)

    return (type = "Str")
        ? StrGet(&buf, numBytes)
        : NumGet(buf, type)
}

ProcessWriteMemory(data, address, processIDorName, type := "Int", numBytes := 4) {
    VarSetCapacity(buf, numBytes, 0)
    (type = "Str")
        ? StrPut(data, &buf, numBytes)
        : NumPut(data, buf, type)

    Process Exist, %processIDorName%
    if !processID := ErrorLevel
        throw Exception("Invalid process name or process ID:`n`n""" . processIDorName . """")

    if !processHandle := DllCall("OpenProcess", "Int", 40, "UInt", 0, "UInt", processID, "Ptr")
        throw Exception("Failed to open process.`n`nError code:`t" . A_LastError)

    result := DllCall("WriteProcessMemory", "Ptr", processHandle, "Ptr", address, "Ptr", &buf, "Ptr", numBytes, "UInt", 0, "UInt")

    if !DllCall("CloseHandle", "Ptr", processHandle, "UInt") && !result
        throw Exception("Failed to close process handle.`n`nError code:`t" . A_LastError)

    if !result
        throw Exception("Failed to write process memory.`n`nError code:`t" . A_LastError)

    return result
}