#MaxThreads 3
#MaxThreadsPerHotkey 3
Start:=0
IniRead, SaveSpeed, Setting.ini, Setting, ClickSpeed, 30

Menu, Submenu2, Add, Speed, Sub1MenuHandl1

Menu, MyMenu, Add, ClickerMenu, MenuHandl1
Menu, MyMenu, Add  ; Добавить разделитель.
Menu, MyMenu, Add, Clicker Job, MenuHandl2
Menu, MyMenu, Add, Settings, :Submenu2
menu, MyMenu, Disable, ClickerMenu
return
SetTimer, Click, off
SetTimer, Click, %SaveSpeed%

XButton2::Menu, MyMenu, Show 
MenuHandl1:
return
MenuHandl2:
if (Start == 0)
{
	Start := 1
	menu, MyMenu, ToggleCheck, Clicker Job
	SetTimer, Click, %SaveSpeed%
}
else {
	menu, MyMenu, ToggleCheck, Clicker Job
	Start := 0
	SetTimer, Click, off
}
return

Sub1MenuHandl1:
Gui, Destroy
SetTimer, Click, off
Gui, Submit
Gui, Add, Text, x12 y9 w150 h30 +Center, Write Clicker speed:
Gui, Add, Edit, x12 y39 w150 h20 vSpeed, %SaveSpeed%
Gui, Add, Button, x37 y72 w100 h30 gOK, OK!
Gui, Show, w179 h123, Speed
return

OK:
Gui, Submit
SaveSpeed := Speed
IniWrite, %Speed%, Setting.ini, Setting, ClickSpeed
Gui, Destroy
return

Click:
ControlClick, x970 y600, Clicker Heroes
return