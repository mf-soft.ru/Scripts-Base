; Using lbbrowse3.dll
; Some test HTML
html1 = <body><img src="Posters/post1.png"></body>
html2 = <body style="background-color:black; color:white">Bienvenidos!</body>
html3 = <body style="background-color:orange; color:blue">Guten Tag!</body>
html4 = <body style="background-color:brown; color:yellow">Aloha!</body>
html5 = <body style="background-color:green; color:black">Konichiwa!</body>

; Load the lbbrowse3 DLL
lbbHandle := DllCall("LoadLibrary", "str", "lbbrowse3.dll")

; Create some GUI
Gui, Add, ListBox, x10 y10 w110 h400 vhtmlvar ghtmlchange, 1||2|3|4|5
Gui, Show, x150 y130 h415 w430, lbbbrowse3 speed tester
WinGet, mainGuiHandle, ID, A

DLLCall("lbbrowse3\CreateBrowser"
        , "uint", mainGuiHandle
        , "Int", "130"
        , "Int", "10"
        , "Int", "240"
        , "Int", "395"
        , "Str", "about:blank")
DllCall("lbbrowse3\ShowStatusbar"
        , "int", 0)

; Get the party started
GoSub, htmlchange
OnExit, Cleanup
Return

; DLLCall that sets the browser control
htmlchange:
  Gui, submit, nohide
  DllCall("lbbrowse3\BrowserString", "str", html%htmlvar%)
return

cleanup:
GuiClose:
  Gui, Destroy
  DllCall("lbbrowse3\DestroyBrowser")
  DllCall("FreeLibrary", "UInt", lbbHandle)
ExitApp