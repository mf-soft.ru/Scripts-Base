; ������ �������� �����
; ����������� ���� ������, ����� �������� ����������� ����� 
; �������� ����� (���� ����������� � ���� ��������).
; ������ ������� ������ 1.0.36 ��� ������, �.�. ���������� ListView.

SetBatchLines -1
SplashTextOn,,, ���� ����������...

; ����������� ������������ ������� �������� ����, ��������, �� ���������� ��
; � ����� �������, �� ��� ����������� ��� �������.
; ����� ������������� ��������� ���������� (�� �������): CUSTOM, BOOLEANMETER, SIGNEDMETER,
; PEAKMETER, UNSIGNEDMETER, BOOLEAN, BUTTON, DECIBELS, SIGNED, UNSIGNED, PERCENT, SLIDER,
; FADER, SINGLESELECT, MUX, MULTIPLESELECT, MIXER, MICROTIME, MILLITIME

ControlTypes = VOLUME,ONOFF,MUTE,MONO,LOUDNESS,STEREOENH,BASSBOOST,PAN,QSOUNDPAN,BASS,TREBLE,EQUALIZER
,0x00000000,0x10010000,0x10020000,0x10020001,0x10030000,0x20010000,0x21010000,0x30040000,0x30020000
,0x30030000,0x30050000,0x40020000,0x50030000,0x70010000,0x70010001,0x71010000,0x71010001,0x60030000,0x61030000

ComponentTypes = MASTER,HEADPHONES,DIGITAL,LINE,MICROPHONE,SYNTH,CD,TELEPHONE,PCSPEAKER,WAVE,AUX,ANALOG,N/A

; �������� ListView � ���������� � �������� �����:
Gui, Add, Listview, w400 h400 vMyListView, ���������|���������|��������|������
LV_ModifyCol(4, "Integer")
SetFormat, Float, 0.2  ; ���������� ����� ������ ����� ������� �� ����.

Loop  ; ��� ������� �� ������������ � ������� �������� ���������� ��� �����������.
{
    CurrMixer := A_Index
    SoundGet, Setting,,, %CurrMixer%
    if ErrorLevel = Can't Open Specified Mixer  ; ����� ������ ������ ��������,
        break                                   ; ��� ������ ����������.

    ; ��� ������� ���������� ������� ���������� ���������� ����������� � ���� ��������:
    Loop, parse, ComponentTypes, `,
    {
        CurrComponent := A_LoopField
        ; ������� ����������, ���������� �� ���������:
        SoundGet, Setting, %CurrComponent%,, %CurrMixer%
        if ErrorLevel = Mixer Doesn't Support This Component Type  ; ���� �� ����������,
            continue  ; ��������� ��������� ��� ����������.
        Loop  ; ��� ������� ���������� ���������� �������� ���� ��������.
        {
            CurrInstance := A_Index
            ; ������� ���������, ���������� �� ������ ��������� ����� ����������:
            SoundGet, Setting, %CurrComponent%:%CurrInstance%,, %CurrMixer%
            ; �������� �� ��� ��������� ������ ��������� ������� ��������
            ; �� ����� ������ �������.
            if ErrorLevel in Mixer Doesn't Have That Many of That Component Type
                ,Invalid Control Type or Component Type
                break  ; ������ ��� ����������� ����� ����������.

            ; �������� ������� �������� ������� ���� �������� ��� �������
            ; ���������� ������� ����������:
            Loop, parse, ControlTypes, `,
            {
                CurrControl := A_LoopField
                SoundGet, Setting, %CurrComponent%:%CurrInstance%, %CurrControl%, %CurrMixer%
                ; �������� �� ��� ��������� ������ ��������� ������� ��������
                ; �� ����� ������ �������.
                if ErrorLevel in Component Doesn't Support This Control Type
                    ,Invalid Control Type or Component Type
                    continue
                if ErrorLevel  ; ���� ����� ������, �������� � � �����������.
                    Setting := ErrorLevel
                ComponentString := CurrComponent
                if CurrInstance > 1
                    ComponentString = %ComponentString%:%CurrInstance%
                LV_Add("", ComponentString, CurrControl, Setting, CurrMixer)
            }  ; ��� ������� ���� ��������.
        }  ; ��� ������� ���������� ����������.
    }  ; ��� ������� ���� �����������.
}  ; ��� ������� �������.

Loop % LV_GetCount("Col")  ; ��������� ������ ������� � ������ �����������.
    LV_ModifyCol(A_Index, "AutoHdr")

SplashTextOff
Gui, Show
return

GuiClose:
ExitApp