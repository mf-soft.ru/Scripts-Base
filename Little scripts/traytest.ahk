mwt_Hotkey = #h ;Win+H
mwt_HotkeyRestoreAll = ^!r ;Ctrl+Alt+R

;��������� ������ ������� � ���� � ������������ ������
Menu, Tray, Icon, %A_WinDir%\system32\Shell32.dll, 99
;������������ ���������� ����, ������� ����� ���� ������
mwt_MaxWindows = 30
;������� �������, ���������� ������� ����
mwt_Hotkey = #� ;Win+H
;������� �������, ����������������� ��� ������� ����
mwt_HotkeyRestoreAll = ^!� ;Ctrl+Alt+R
;��� ��������� ������� "���������" ������ ���� ������ ������� � ����;
;���� ��� ������ �����, ������� "Y" ������ "N"
mwt_StandardMenu = N
;��������� ��������� ��������� �������� ����������� ��������� �
;�������� ����� ����������� ������� ������� ������ ���,
;����� �� ������ ������ ������ �����, ��� ���� ����
#HotkeyModifierTimeout 100
SetWinDelay 10
SetKeyDelay 0
;����� ���� �������� �� ����� ������ ���������� ��������
#SingleInstance
;�������� �� �������� ������
DetectHiddenWindows, On
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;���������� ������� �������, ���������� ������� ����
Hotkey, #h, mwt_Minimize
;���������� ������� �������, ����������������� ��� ������� ����
Hotkey, ^!r, mwt_RestoreAll
;��� ���������� �������� ���������� ���������� ��� ������� ����, ���� ������� ����
OnExit, mwt_RestoreAllThenExit
;��������� ���� ������ ������� � ����
if mwt_StandardMenu = Y
	Menu, Tray, Add
else
{
	Menu, Tray, NoStandard
	Menu, Tray, Add, ����� � �������������� ���� ������� ����, mwt_RestoreAllThenExit
}
Menu, Tray, Add, �������������� ���� ������� ����, mwt_RestoreAll
Menu, Tray, Add
;����������� ������ ����
if a_AhkVersion = ;����� (��� ������ ����� 1.0.22)
	mwt_MaxLength = 100
else
	mwt_MaxLength = 260
return ;����� ������ ����-���������� �������
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;������ ���������� ����������, ����� ������ ������� �������, ���������� ������� ����
mwt_Minimize:
if mwt_WindowCount >= %mwt_MaxWindows%
{
	MsgBox �� ����� ���� ������ ������������ ������, ��� %mwt_MaxWindows% ����.
	return
}
;�������� �������� ���� � ���������� ��� ��� "��������� ��������� ����",
;�.�. ���� �� ��������� ��� ���� ���������� ������� �������, ��������� � ������
WinWait, A,, 2
if ErrorLevel <> 0
	return
;�������� ���������� �� �������� ����
WinGet, mwt_ActiveID, ID ;����� (HWND) ����
WinGetTitle, mwt_ActiveTitle ;��������� ����
WinGetClass, mwt_ActiveClass ;��� ������ ����
WinGet, mwt_ActivePID, PID ;������������� ��������, �������� ����������� ����
WinGet, mwt_List, List, ahk_pid %mwt_ActivePID% ;������ ���� ���� ����� ��������
WinGet, mwt_ActiveProcess, ProcessName ;��� ��������, �������� ����������� ����
;������� ���� � ������ ����� �������� ������
if mwt_ActiveClass in Shell_TrayWnd,Progman
	return
;��������� ������� ���� �� ����� �������������� ���, ������������
;� ������ ���� (��� ����� ������������ ����� Alt+Escape)
Send, !{esc}
;���������� �������� ������ ���� �������� (������ �� ����������),
;������� �������� ��� ���� �������� �������� �� ������ WS_VISIBLE,
;mwt_WindowsIDs - ������ ������� ����������� ����,
;���������� �������� | (������������ �����)
mwt_WindowsIDs = ""
;��������� - ������ ������, �������� ����� ������ ������� ���� ��������
If mwt_ActiveProcess = explorer.exe
{
	WinHide, ahk_id %mwt_ActiveID%
	mwt_WindowsIDs = %mwt_WindowsIDs%|%mwt_ActiveID%
}
;�������� ��� ������� ���� ��������
Else
{
	Loop, %mwt_List%
	{
		mwt_id := mwt_List%A_Index%
		WinGet, style, Style, ahk_id %mwt_id%
		if ( style & 0x10000000 ) ;WS_VISIBLE
		{
			WinHide, ahk_id %mwt_id%
			;Result := DllCall("ShowWindow", "UInt", mwt_id, "Int", "0")
			mwt_WindowsIDs = %mwt_WindowsIDs%|%mwt_id%
		}
	}
}
;���� ��������� ���� ����, ���������� ��� ������,
;�.�. ��� ����� �������� ��������� ������ ����
if mwt_ActiveTitle =
	mwt_ActiveTitle = ahk_class %mwt_ActiveClass%
;�������� ��������� ���� �� ������������ ����� mwt_MaxLength
StringLeft, mwt_ActiveTitle, mwt_ActiveTitle, %mwt_MaxLength%
;����������� ������������ ��������� ������ ����:
Loop, %mwt_MaxWindows%
{
	if mwt_WindowTitle%a_index% = %mwt_ActiveTitle%
	{
		;����� ����� �� ���������, ��� ��� ��������� �� ��������.
		StringTrimLeft, mwt_ActiveIDShort, mwt_ActiveID, 2
		StringLen, mwt_ActiveIDShortLength, mwt_ActiveIDShort
		StringLen, mwt_ActiveTitleLength, mwt_ActiveTitle
		mwt_ActiveTitleLength += %mwt_ActiveIDShortLength%
		mwt_ActiveTitleLength += 1
		if mwt_ActiveTitleLength > %mwt_MaxLength%
		{
			TrimCount = %mwt_ActiveTitleLength%
			TrimCount -= %mwt_MaxLength%
			StringTrimRight, mwt_ActiveTitle, mwt_ActiveTitle, %TrimCount%
		}
		;������ ���������� ���������:
		mwt_ActiveTitle = %mwt_ActiveTitle% %mwt_ActiveIDShort%
		break
	}
}
;�� ������ ������ ��������, ��� �� ����� ���� ��� � ������ ����� �������
mwt_AlreadyExists = n
Loop, %mwt_MaxWindows%
{
	if mwt_WindowID%a_index% = %mwt_ActiveID%
	{
		mwt_AlreadyExists = y
		break
	}
}
;������� ���� � ������ � � ����
if mwt_AlreadyExists = n
{
	Menu, Tray, add, %mwt_ActiveTitle%, RestoreFromTrayMenu
	mwt_WindowCount += 1
	Loop, %mwt_MaxWindows% ;����� ���������� �������� � �������
	{
		if mwt_WindowID%a_index% = ;������� ��������
		{
			mwt_WindowID%a_index% = %mwt_ActiveID%
			mwt_WindowTitle%a_index% = %mwt_ActiveTitle%
			mwt_WindowPID%a_index% = %mwt_ActivePID%
			mwt_WinIDs%a_index% = %mwt_WindowsIDs%
			break
		}
	}
}
return
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;������ ���������� ����������, ����� ������� ������� ���� ������ ������� � ����,
;��������������� ����������� �������� ����
RestoreFromTrayMenu:
Menu, Tray, delete, %A_ThisMenuItem%
;����� ���� �� ��� ����������� ���������, ������������ ��� �������� ������ ����
Loop, %mwt_MaxWindows%
{
	if mwt_WindowTitle%a_index% = %A_ThisMenuItem%
	{
		StringTrimRight, IDToRestore, mwt_WindowID%a_index%, 0
		StringTrimRight, PIDToRestore, mwt_WindowPID%a_index%, 0
		mwt_WindowsIDs := mwt_WinIDs%a_index%
		Loop, parse, mwt_WindowsIDs, |
			WinShow, ahk_id %A_LoopField%
		WinActivate ahk_id %IDToRestore%
		mwt_WindowID%a_index% = ;������� �������� ��������
		mwt_WindowTitle%a_index% =
		mwt_WindowPID%a_index% =
		mwt_WinIDs%a_index% =
		mwt_WindowCount -= 1
		break
	}
}
return
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;������ ���������� ����������, ����� ������� ������� ���� ������ ������� � ����
;"����� � �������������� ���� ������� ����"
mwt_RestoreAllThenExit:
Gosub, mwt_RestoreAll
ExitApp ;���������� ������ �������
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;������ ���������� ����������, ����� ������� ������� ���� ������ ������� � ����
;"�������������� ���� ������� ����"
mwt_RestoreAll:
Loop, %mwt_MaxWindows%
{
	if mwt_WindowID%a_index% <>
	{
		StringTrimRight, IDToRestore, mwt_WindowID%a_index%, 0
		StringTrimRight, PIDToRestore, mwt_WindowPID%a_index%, 0
		mwt_WindowsIDs := mwt_WinIDs%a_index%
		Loop, parse, mwt_WindowsIDs, |
			WinShow, ahk_id %A_LoopField%
		WinActivate ahk_id %IDToRestore%
		StringTrimRight, MenuToRemove, mwt_WindowTitle%a_index%, 0
		Menu, Tray, delete, %MenuToRemove%
		mwt_WindowID%a_index% =
		mwt_WindowTitle%a_index% =
		mwt_WindowPID%a_index% =
		mwt_WinIDs%a_index% =
		mwt_WindowCount -= 1
	}
	if mwt_WindowCount = 0
		break
}
return