;
; SciTE4AutoHotkey MsgBox Creator
;

#SingleInstance Ignore
#NoTrayIcon
#NoEnv
SetWorkingDir, %A_ScriptDir%

oSciTE := GetSciTEInstance()
if !oSciTE
{
	MsgBox, 16, MsgBox Creator, Cannot find SciTE!
	ExitApp
}

scHwnd := oSciTE.SciTEHandle

; Main icon
Menu, Tray, Icon, ..\toolicon.icl, 12

HTTP := ComObjCreate("WinHTTP.WinHTTPRequest.5.1")
HTTP.SetTimeouts(2000,2000,2000,2000)
HTTP.Open("GET", "http://sa-mp.com/")
HTTP.Send()
checkversion :=  HTTP.ResponseText
RegExMatch(checkversion, "<b>SA-MP ([0-9.]{3,9})</b></font></td>", Res)
StringReplace, Res1, Res1, ., , All

IfNotExist, %A_MyDocuments%/GTA San Andreas User Files/UDF_037.ahk
{
	MsgBox, 0, Scite4AHK, Scite4AHK �� ����� � ��� ���� UDF_037.ahk `n������ ���������� ���������� �����
	URL := "https://raw.githubusercontent.com/McFree/Scite/master/UDF_037.ahk"
	;URL := "https://downloader.disk.yandex.ru/disk/5023c51e7ed54ff94a74de3c5ca4fd66c5285c4b52f3b7d0e5bdad14f107cc8d/57b92e2e/QMm9IKZNnvckvbQfHk7Wa394gyxh2zBlVb1_nJKTOdTCFcm1dxyaU7n9kfxtbdASqCVloH_bRw3jL3wzwPV87Q%3D%3D?uid=0&filename=UDF_037.ahk&disposition=attachment&hash=lox/3j5N30PPmOIA%2BTSbC4udYdL8OygGUOryJPc14t8%3D&limit=0&content_type=application%2Foctet-stream&fsize=160680&hid=8126b2570161bde28fd9ce0aeb624a09&media_type=data&tknv=v2"
	UrlDownloadToFile, %URL%, %A_MyDocuments%\GTA San Andreas User Files\UDF_037.ahk
}
IfNotExist, %A_MyDocuments%/GTA San Andreas User Files/API.dll
{
	MsgBox, 0, Scite4AHK, Scite4AHK �� ����� � ��� ���� API.dll `n������ ���������� ���������� �����
	URL := "https://downloader.disk.yandex.ru/disk/da130bb0496adaf4fb4d346f12e8d4e8462bfde42041a8dd9e4457547b26a326/57b9330a/EhzUrnK1AMVuH056jjG2RbXrcxrSYDlUlaXmV22WNICmtoQxZTy3JRZDH_5_42YgPT-K4nEJvsyujGkJsUdwqA%3D%3D?uid=178568709&filename=API.dll&disposition=attachment&hash=&limit=0&content_type=application%2Fx-msdownload&fsize=499712&hid=6243ef624f58e43f809260cf906de7c0&media_type=executable&tknv=v2&etag=ced56ca7cb59e6a9dda65662c4b0c246"
	UrlDownloadToFile, %URL%, %A_MyDocuments%\GTA San Andreas User Files\API.dll
}
oSciTE.InsertText("; ��� ���������� SAMP_UDF ��� " Res1 " ������ SA:MP`n")
oSciTE.InsertText("#Include " A_MyDocuments "\GTA San Andreas User Files\UDF_" Res1 ".ahk")
