RegRead, Adress, HKEY_CURRENT_USER, Software\SAMP, gta_sa_exe 
RegExMatch(Adress, "(.*)gta\_sa\.exe", Adress) 
Adress1 .= "gta_sa.exe"
CurrList := 0

IniRead, A_Nick, %A_MyDocuments%/GTA San Andreas User Files/SSL_Settings.set, Main, PlayerNick
if (A_Nick == "ERROR")
	RegRead, A_Nick, HKEY_CURRENT_USER, Software\SAMP, PlayerName

IniRead, ServersNum, %A_MyDocuments%/GTA San Andreas User Files/SSL_Settings.set, Main, ServersNum

Loop, % ServersNum
{
	IniRead, Server_IP, %A_MyDocuments%/GTA San Andreas User Files/SSL_Settings.set, Server%ServersNum%, IP 
	IniRead, Server_Pass, %A_MyDocuments%/GTA San Andreas User Files/SSL_Settings.set, Server%ServersNum%, Pass
	IniRead, Server_Nick, %A_MyDocuments%/GTA San Andreas User Files/SSL_Settings.set, Server%ServersNum%, Nick
	LV_Add("", %Server_IP%, %Server_Pass%, %Server_Nick%)
}

Gui, Add, ListView, x2 y2 w330 h170 gMyListView,  IP:PORT|PASS|Nick
Gui, Add, GroupBox, x342 y2 w180 h50 , NickName
Gui, Add, Edit, x352 y22 w160 h20 vA_Nick, %A_Nick%
Gui, Add, GroupBox, x342 y62 w180 h110 , Server
Gui, Add, Text, x352 y82 w160 h20 , Name:
Gui, Add, Edit, x382 y102 w130 h20 vA_IP, %A_IP%
Gui, Add, Text, x352 y102 w30 h20 , IP:
Gui, Add, Text, x352 y132 w40 h20 , Pass:
Gui, Add, Edit, x392 y132 w120 h20 vA_PASS, %A_PASS%
Gui, Add, Button, x442 y182 w80 h30 gAdd, Add
Gui, Add, Button, x342 y182 w80 h30 gRemove, Delete
Gui, Add, Button, x12 y182 w310 h30 gEnter, Enter
Gui, Show, w534 h226, Scite Samp Launcher  By McFree
return

GuiClose:
ExitApp

Add:
Gui, Submit, NoHide
LV_Add("", A_IP, A_PASS, A_Nick)
ServersNum++
IniWrite, %A_IP%, %A_MyDocuments%/GTA San Andreas User Files/SSL_Settings.set, Server%ServersNum%, IP 
IniWrite, %A_PASS%, %A_MyDocuments%/GTA San Andreas User Files/SSL_Settings.set, Server%ServersNum%, Pass 
IniWrite, %A_Nick%, %A_MyDocuments%/GTA San Andreas User Files/SSL_Settings.set, Server%ServersNum%, Nick 
return

Remove:
LV_Delete(CurrList)
IniDelete, %A_MyDocuments%/GTA San Andreas User Files/SSL_Settings.set, Server%CurrList%, IP 
IniDelete, %A_MyDocuments%/GTA San Andreas User Files/SSL_Settings.set, Server%CurrList%, Pass 
IniDelete, %A_MyDocuments%/GTA San Andreas User Files/SSL_Settings.set, Server%CurrList%, Nick 
Gui, Submit, NoHide
return

Enter:
IniWrite, %A_Nick%, %A_MyDocuments%/GTA San Andreas User Files/SSL_Settings.set, Main, PlayerNick
;Run, samp://%A_IP%
return

MyListView:
CurrList := A_GuiEvent
LV_GetText(RowIP, A_EventInfo, 1)  ; Get the text from the row's first field.
LV_GetText(RowPASS, A_EventInfo, 2)  ; Get the text from the row's first field.
LV_GetText(RowNick, A_EventInfo, 3)  ; Get the text from the row's first field.
A_IP := RowIP
A_PASS := RowPASS
A_Nick := RowNick
return