#UseHook
#NoEnv
#IfWinActive GTA:SA:MP
#SingleInstance force
#Include E:\Flash\Dev\Scripts\adminhelper-for-samp-rp-master
SetWorkingDir E:\Flash\Dev\Scripts\adminhelper-for-samp-rp-master


;; Configs

#Include scripts\ConfigReader.ahk


;; Libraries

#Include libraries\JSON.ahk
#Include libraries\SAMP-UDF-Addon.ahk
#Include libraries\SAMP-UDF-Ex.ahk
#Include libraries\Zip.ahk


;; Modules Funcs

#Include modules\Chatlog\Funcs.ahk
#Include modules\CMD\Funcs.ahk
#Include modules\Events\Funcs.ahk
#Include modules\HotKeyRegister\Funcs.ahk
#Include modules\IgnoreList\Funcs.ahk
#Include modules\SAMP-NearbyPlayers\Funcs.ahk
#Include modules\SAMP-UsersListUpdater\Funcs.ahk
#Include modules\SendChatSavingMessage\Funcs.ahk


;; Events Funcs

#Include events\DayZ\Funcs.ahk
#Include events\Parachute\Funcs.ahk


;; Plugins Funcs

#Include plugins\TagName\Funcs.ahk



;; GUI

#Include GUI.ahk


;; Modules Binds

#Include modules\CMD\Binds.ahk



;; Binds

#Include *i UserBinds.ahk


Return



;; Modules Labels

#Include modules\Chatlog\Labels.ahk



;; Events Labels

#Include events\DayZ\Labels.ahk
#Include events\Parachute\Labels.ahk


;; Plugins Labels

#Include plugins\TagName\Labels.ahk


