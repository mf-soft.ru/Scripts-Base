#SingleInstance force
#IfWinActive Criminal Russia Multiplayer Ревизия G
Menu, Tray, add, Показать, Show,
Menu, Tray, Default, Показать,
Menu, Tray, add, Скрыть, Hide,
Menu, Tray, add
Menu, Tray, add, Выход, GuiClose,
Menu, Tray, NoStandard
MenuNum := 0
Gui, Add, Tab, x-4 y0 w630 h380 , 1|2|3|4|5|6|7|8|9|10
Gui, Tab
Gui, Font, bold,
Gui, Add, Text, x6 y50 w70 h20 , Клавиша
Gui, Add, Text, x106 y50 w280 h20 , Бинд (строка с текстом или командой)
Gui, Add, Text, x556 y50 w60 h20 , Enter
loop, 10
{
Gui, Tab, %A_Index%
Gui, Font, norm,
Ypos := 70
loop, 10
{
MenuNum++
Gui, Add, Hotkey, x6 y%Ypos% w90 h20 vHot%MenuNum%,
Gui, Add, Edit, x106 y%Ypos% w440 h20 vBind%MenuNum%,
Gui, Add, CheckBox, x556 y%Ypos% w20 h20 vEnterOn%MenuNum%,
Ypos := Ypos + 30
}
}
Gui, Tab
Gui, Add, DropDownList, x6 y390 w150 h20 r6 gLoadProfil vDrop, RP-Инструктор|RP-Таксист|Мой профиль 1|Мой профиль 2|Мой профиль 3|Мой профиль 4
Gui, Add, Button, x166 y390 w80 h20 gSaveBind, Сохранить
Gui, Add, Button, x256 y390 w80 h20 gBinderActive, Применить
Gui, Add, Button, x346 y390 w80 h20 gHide, Скрыть
Gui, Add, Button, x436 y390 w80 h20 gBindDel, Сброс
Gui, Add, Button, x526 y390 w80 h20 gGuiClose, Выход
Gui, Show, center h420 w624, KronkaBinder
IniRead, Profil, %A_WorkingDir%\Settings.ini, Main, Profile
GuiControl, ChooseString, Drop, %Profil%
Goto, LoadProfil
Return
Show:
Gui, Show
return
Hide:
Gui, Hide
return
LoadProfil:
GuiControlGet, Drop
if Drop=
{
GuiControl, ChooseString, Drop, RP-Инструктор
Goto, AutoSchool
}
if (Drop = "RP-Инструктор")
Goto, AutoSchool
if (Drop = "RP-Таксист")
Goto, Taxi
if (Drop = "Мой профиль 1")
Goto, MyProfile1
if (Drop = "Мой профиль 2")
Goto, MyProfile2
if (Drop = "Мой профиль 3")
Goto, MyProfile3
if (Drop = "Мой профиль 4")
Goto, MyProfile4
return
SaveBind:
Gui, Submit, Nohide
GuiControlGet, Drop
if (Drop = "RP-Инструктор")
Goto, SaveAutoSchool
if (Drop = "RP-Таксист")
Goto, SaveTaxi
if (Drop = "Мой профиль 1")
Goto, SaveMyProfile1
if (Drop = "Мой профиль 2")
Goto, SaveMyProfile2
if (Drop = "Мой профиль 3")
Goto, SaveMyProfile3
if (Drop = "Мой профиль 4")
Goto, SaveMyProfile4
return
SaveAutoSchool:
loop 100
{
IniWrite, % Hot%A_Index%, %A_WorkingDir%\Profiles\RP-Инструктор.profile, Key, HotKey%A_Index%
IniWrite, % Bind%A_Index%, %A_WorkingDir%\Profiles\RP-Инструктор.profile, Key, Bind%A_Index%
IniWrite, % EnterOn%A_Index%, %A_WorkingDir%\Profiles\RP-Инструктор.profile, Key, Enter%A_Index%
}
return
SaveTaxi:
loop 100
{
IniWrite, % Hot%A_Index%, %A_WorkingDir%\Profiles\RP-Таксист.profile, Key, HotKey%A_Index%
IniWrite, % Bind%A_Index%, %A_WorkingDir%\Profiles\RP-Таксист.profile, Key, Bind%A_Index%
IniWrite, % EnterOn%A_Index%, %A_WorkingDir%\Profiles\RP-Таксист.profile, Key, Enter%A_Index%
}
return
SaveMyProfile1:
loop 100
{
IniWrite, % Hot%A_Index%, %A_WorkingDir%\Profiles\Профиль 1.profile, Key, HotKey%A_Index%
IniWrite, % Bind%A_Index%, %A_WorkingDir%\Profiles\Профиль 1.profile, Key, Bind%A_Index%
IniWrite, % EnterOn%A_Index%, %A_WorkingDir%\Profiles\Профиль 1.profile, Key, Enter%A_Index%
}
return
SaveMyProfile2:
loop 100
{
IniWrite, % Hot%A_Index%, %A_WorkingDir%\Profiles\Профиль 2.profile, Key, HotKey%A_Index%
IniWrite, % Bind%A_Index%, %A_WorkingDir%\Profiles\Профиль 2.profile, Key, Bind%A_Index%
IniWrite, % EnterOn%A_Index%, %A_WorkingDir%\Profiles\Профиль 2.profile, Key, Enter%A_Index%
}
return
SaveMyProfile3:
loop 100
{
IniWrite, % Hot%A_Index%, %A_WorkingDir%\Profiles\Профиль 3.profile, Key, HotKey%A_Index%
IniWrite, % Bind%A_Index%, %A_WorkingDir%\Profiles\Профиль 3.profile, Key, Bind%A_Index%
IniWrite, % EnterOn%A_Index%, %A_WorkingDir%\Profiles\Профиль 3.profile, Key, Enter%A_Index%
}
return
SaveMyProfile4:
loop 100
{
IniWrite, % Hot%A_Index%, %A_WorkingDir%\Profiles\Профиль 4.profile, Key, HotKey%A_Index%
IniWrite, % Bind%A_Index%, %A_WorkingDir%\Profiles\Профиль 4.profile, Key, Bind%A_Index%
IniWrite, % EnterOn%A_Index%, %A_WorkingDir%\Profiles\Профиль 4.profile, Key, Enter%A_Index%
}
return
AutoSchool:
loop 100
{
IniRead, VarHot%A_Index%, %A_WorkingDir%\Profiles\RP-Инструктор.profile, Key, HotKey%A_Index%, %A_Space%
IniRead, VarBind%A_Index%, %A_WorkingDir%\Profiles\RP-Инструктор.profile, Key, Bind%A_Index%, %A_Space%
IniRead, VarEnter%A_Index%, %A_WorkingDir%\Profiles\RP-Инструктор.profile, Key, Enter%A_Index%, %A_Space%
GuiControl,, Hot%A_Index%, % varHot%A_Index%
GuiControl,, Bind%A_Index%, % varBind%A_Index%
GuiControl,, EnterOn%A_Index%, % varEnter%A_Index%
}
Goto, BinderActive
return
Taxi:
loop 100
{
IniRead, VarHot%A_Index%, %A_WorkingDir%\Profiles\RP-Таксист.profile, Key, HotKey%A_Index%, %A_Space%
IniRead, VarBind%A_Index%, %A_WorkingDir%\Profiles\RP-Таксист.profile, Key, Bind%A_Index%, %A_Space%
IniRead, VarEnter%A_Index%, %A_WorkingDir%\Profiles\RP-Таксист.profile, Key, Enter%A_Index%, %A_Space%
GuiControl,, Hot%A_Index%, % varHot%A_Index%
GuiControl,, Bind%A_Index%, % varBind%A_Index%
GuiControl,, EnterOn%A_Index%, % varEnter%A_Index%
}
Goto, BinderActive
return
MyProfile1:
loop 100
{
IniRead, VarHot%A_Index%, %A_WorkingDir%\Profiles\Профиль 1.profile, Key, HotKey%A_Index%, %A_Space%
IniRead, VarBind%A_Index%, %A_WorkingDir%\Profiles\Профиль 1.profile, Key, Bind%A_Index%, %A_Space%
IniRead, VarEnter%A_Index%, %A_WorkingDir%\Profiles\Профиль 1.profile, Key, Enter%A_Index%, %A_Space%
GuiControl,, Hot%A_Index%, % varHot%A_Index%
GuiControl,, Bind%A_Index%, % varBind%A_Index%
GuiControl,, EnterOn%A_Index%, % varEnter%A_Index%
}
Goto, BinderActive
return
MyProfile2:
loop 100
{
IniRead, VarHot%A_Index%, %A_WorkingDir%\Profiles\Профиль 2.profile, Key, HotKey%A_Index%, %A_Space%
IniRead, VarBind%A_Index%, %A_WorkingDir%\Profiles\Профиль 2.profile, Key, Bind%A_Index%, %A_Space%
IniRead, VarEnter%A_Index%, %A_WorkingDir%\Profiles\Профиль 2.profile, Key, Enter%A_Index%, %A_Space%
GuiControl,, Hot%A_Index%, % varHot%A_Index%
GuiControl,, Bind%A_Index%, % varBind%A_Index%
GuiControl,, EnterOn%A_Index%, % varEnter%A_Index%
}
Goto, BinderActive
return
MyProfile3:
loop 100
{
IniRead, VarHot%A_Index%, %A_WorkingDir%\Profiles\Профиль 3.profile, Key, HotKey%A_Index%, %A_Space%
IniRead, VarBind%A_Index%, %A_WorkingDir%\Profiles\Профиль 3.profile, Key, Bind%A_Index%, %A_Space%
IniRead, VarEnter%A_Index%, %A_WorkingDir%\Profiles\Профиль 3.profile, Key, Enter%A_Index%, %A_Space%
GuiControl,, Hot%A_Index%, % varHot%A_Index%
GuiControl,, Bind%A_Index%, % varBind%A_Index%
GuiControl,, EnterOn%A_Index%, % varEnter%A_Index%
}
Goto, BinderActive
return
MyProfile4:
loop 100
{
IniRead, VarHot%A_Index%, %A_WorkingDir%\Profiles\Профиль 4.profile, Key, HotKey%A_Index%, %A_Space%
IniRead, VarBind%A_Index%, %A_WorkingDir%\Profiles\Профиль 4.profile, Key, Bind%A_Index%, %A_Space%
IniRead, VarEnter%A_Index%, %A_WorkingDir%\Profiles\Профиль 4.profile, Key, Enter%A_Index%, %A_Space%
GuiControl,, Hot%A_Index%, % varHot%A_Index%
GuiControl,, Bind%A_Index%, % varBind%A_Index%
GuiControl,, EnterOn%A_Index%, % varEnter%A_Index%
}
Goto, BinderActive
return
BinderActive:
loop 100
{
Hotkey, % Hot%A_Index%, Off, UseErrorLevel
}
Gui, Submit, NoHide
loop 100
{
Hotkey, % Hot%A_Index%, Activ%A_Index%, On, UseErrorLevel
}
return
Activ1:
if EnterOn1 > 0
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind1%{enter}
}
Else
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind1%
}
Return
Activ2:
if EnterOn2 > 0
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind2%{enter}
}
Else
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind2%
}
Return
Activ3:
if EnterOn3 > 0
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind3%{enter}
}
Else
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind3%
}
Return
Activ4:
if EnterOn4 > 0
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind4%{enter}
}
Else
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind4%
}
Return
Activ5:
if EnterOn5 > 0
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind5%{enter}
}
Else
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind5%
}
Return
Activ6:
if EnterOn6 > 0
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind6%{enter}
}
Else
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind6%
}
Return
Activ7:
if EnterOn7 > 0
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind7%{enter}
}
Else
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind7%
}
Return
Activ8:
if EnterOn8 > 0
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind8%{enter}
}
Else
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind8%
}
Return
Activ9:
if EnterOn9 > 0
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind9%{enter}
}
Else
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind9%
}
Return
Activ10:
if EnterOn10 > 0
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind10%{enter}
}
Else
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind10%
}
Return
Activ11:
if EnterOn11 > 0
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind11%{enter}
}
Else
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind11%
}
Return
Activ12:
if EnterOn12 > 0
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind12%{enter}
}
Else
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind12%
}
Return
Activ13:
if EnterOn13 > 0
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind13%{enter}
}
Else
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind13%
}
Return
Activ14:
if EnterOn14 > 0
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind14%{enter}
}
Else
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind14%
}
Return
Activ15:
if EnterOn15 > 0
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind15%{enter}
}
Else
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind15%
}
Return
Activ16:
if EnterOn16 > 0
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind16%{enter}
}
Else
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind16%
}
Return
Activ17:
if EnterOn17 > 0
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind17%{enter}
}
Else
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind17%
}
Return
Activ18:
if EnterOn18 > 0
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind18%{enter}
}
Else
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind18%
}
Return
Activ19:
if EnterOn19 > 0
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind19%{enter}
}
Else
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind19%
}
Return
Activ20:
if EnterOn20 > 0
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind20%{enter}
}
Else
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind20%
}
Return
Activ21:
if EnterOn21 > 0
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind21%{enter}
}
Else
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind21%
}
Return
Activ22:
if EnterOn22 > 0
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind22%{enter}
}
Else
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind22%
}
Return
Activ23:
if EnterOn23 > 0
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind23%{enter}
}
Else
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind23%
}
Return
Activ24:
if EnterOn24 > 0
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind24%{enter}
}
Else
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind24%
}
Return
Activ25:
if EnterOn25 > 0
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind25%{enter}
}
Else
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind25%
}
Return
Activ26:
if EnterOn26 > 0
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind26%{enter}
}
Else
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind26%
}
Return
Activ27:
if EnterOn27 > 0
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind27%{enter}
}
Else
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind27%
}
Return
Activ28:
if EnterOn28 > 0
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind28%{enter}
}
Else
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind28%
}
Return
Activ29:
if EnterOn29 > 0
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind29%{enter}
}
Else
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind29%
}
Return
Activ30:
if EnterOn30 > 0
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind30%{enter}
}
Else
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind30%
}
Return
Activ31:
if EnterOn31 > 0
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind31%{enter}
}
Else
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind31%
}
Return
Activ32:
if EnterOn32 > 0
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind32%{enter}
}
Else
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind32%
}
Return
Activ33:
if EnterOn33 > 0
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind33%{enter}
}
Else
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind33%
}
Return
Activ34:
if EnterOn34 > 0
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind34%{enter}
}
Else
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind34%
}
Return
Activ35:
if EnterOn35 > 0
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind35%{enter}
}
Else
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind35%
}
Return
Activ36:
if EnterOn36 > 0
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind36%{enter}
}
Else
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind36%
}
Return
Activ37:
if EnterOn37 > 0
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind37%{enter}
}
Else
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind37%
}
Return
Activ38:
if EnterOn38 > 0
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind38%{enter}
}
Else
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind38%
}
Return
Activ39:
if EnterOn39 > 0
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind9%{enter}
}
Else
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind39%
}
Return
Activ40:
if EnterOn40 > 0
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind40%{enter}
}
Else
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind40%
}
Return
Activ41:
if EnterOn41 > 0
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind41%{enter}
}
Else
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind41%
}
Return
Activ42:
if EnterOn42 > 0
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind42%{enter}
}
Else
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind42%
}
Return
Activ43:
if EnterOn43 > 0
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind43%{enter}
}
Else
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind43%
}
Return
Activ44:
if EnterOn44 > 0
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind44%{enter}
}
Else
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind44%
}
Return
Activ45:
if EnterOn45 > 0
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind45%{enter}
}
Else
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind45%
}
Return
Activ46:
if EnterOn46 > 0
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind46%{enter}
}
Else
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind46%
}
Return
Activ47:
if EnterOn47 > 0
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind47%{enter}
}
Else
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind47%
}
Return
Activ48:
if EnterOn48 > 0
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind48%{enter}
}
Else
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind48%
}
Return
Activ49:
if EnterOn49 > 0
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind49%{enter}
}
Else
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind49%
}
Return
Activ50:
if EnterOn50 > 0
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind50%{enter}
}
Else
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind50%
}
Return
Activ51:
if EnterOn51 > 0
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind51%{enter}
}
Else
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind51%
}
Return
Activ52:
if EnterOn52 > 0
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind52%{enter}
}
Else
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind52%
}
Return
Activ53:
if EnterOn53 > 0
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind53%{enter}
}
Else
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind53%
}
Return
Activ54:
if EnterOn54 > 0
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind54%{enter}
}
Else
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind54%
}
Return
Activ55:
if EnterOn55 > 0
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind55%{enter}
}
Else
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind55%
}
Return
Activ56:
if EnterOn56 > 0
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind56%{enter}
}
Else
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind56%
}
Return
Activ57:
if EnterOn57 > 0
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind57%{enter}
}
Else
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind57%
}
Return
Activ58:
if EnterOn58 > 0
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind58%{enter}
}
Else
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind58%
}
Return
Activ59:
if EnterOn59 > 0
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind59%{enter}
}
Else
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind59%
}
Return
Activ60:
if EnterOn60 > 0
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind60%{enter}
}
Else
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind60%
}
Return
Activ61:
if EnterOn61 > 0
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind61%{enter}
}
Else
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind61%
}
Return
Activ62:
if EnterOn62 > 0
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind62%{enter}
}
Else
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind62%
}
Return
Activ63:
if EnterOn63 > 0
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind63%{enter}
}
Else
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind63%
}
Return
Activ64:
if EnterOn64 > 0
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind64%{enter}
}
Else
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind64%
}
Return
Activ65:
if EnterOn65 > 0
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind65%{enter}
}
Else
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind65%
}
Return
Activ66:
if EnterOn66 > 0
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind66%{enter}
}
Else
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind66%
}
Return
Activ67:
if EnterOn67 > 0
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind67%{enter}
}
Else
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind67%
}
Return
Activ68:
if EnterOn68 > 0
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind68%{enter}
}
Else
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind68%
}
Return
Activ69:
if EnterOn69 > 0
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind69%{enter}
}
Else
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind69%
}
Return
Activ70:
if EnterOn70 > 0
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind70%{enter}
}
Else
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind70%
}
Return
Activ71:
if EnterOn71 > 0
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind71%{enter}
}
Else
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind71%
}
Return
Activ72:
if EnterOn72 > 0
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind72%{enter}
}
Else
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind72%
}
Return
Activ73:
if EnterOn73 > 0
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind73%{enter}
}
Else
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind73%
}
Return
Activ74:
if EnterOn74 > 0
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind74%{enter}
}
Else
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind74%
}
Return
Activ75:
if EnterOn75 > 0
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind75%{enter}
}
Else
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind75%
}
Return
Activ76:
if EnterOn76 > 0
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind76%{enter}
}
Else
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind76%
}
Return
Activ77:
if EnterOn77 > 0
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind77%{enter}
}
Else
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind77%
}
Return
Activ78:
if EnterOn78 > 0
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind78%{enter}
}
Else
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind78%
}
Return
Activ79:
if EnterOn79 > 0
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind79%{enter}
}
Else
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind79%
}
Return
Activ80:
if EnterOn80 > 0
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind80%{enter}
}
Else
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind80%
}
Return
Activ81:
if EnterOn81 > 0
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind81%{enter}
}
Else
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind81%
}
Return
Activ82:
if EnterOn82 > 0
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind82%{enter}
}
Else
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind82%
}
Return
Activ83:
if EnterOn83 > 0
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind83%{enter}
}
Else
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind83%
}
Return
Activ84:
if EnterOn84 > 0
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind84%{enter}
}
Else
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind84%
}
Return
Activ85:
if EnterOn85 > 0
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind85%{enter}
}
Else
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind85%
}
Return
Activ86:
if EnterOn86 > 0
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind86%{enter}
}
Else
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind86%
}
Return
Activ87:
if EnterOn87 > 0
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind87%{enter}
}
Else
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind87%
}
Return
Activ88:
if EnterOn88 > 0
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind88%{enter}
}
Else
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind88%
}
Return
Activ89:
if EnterOn89 > 0
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind89%{enter}
}
Else
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind89%
}
Return
Activ90:
if EnterOn90 > 0
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind90%{enter}
}
Else
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind90%
}
Return
Activ91:
if EnterOn91 > 0
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind91%{enter}
}
Else
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind91%
}
Return
Activ92:
if EnterOn92 > 0
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind92%{enter}
}
Else
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind92%
}
Return
Activ93:
if EnterOn93 > 0
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind93%{enter}
}
Else
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind93%
}
Return
Activ94:
if EnterOn94 > 0
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind94%{enter}
}
Else
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind94%
}
Return
Activ95:
if EnterOn95 > 0
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind95%{enter}
}
Else
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind95%
}
Return
Activ96:
if EnterOn96 > 0
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind96%{enter}
}
Else
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind96%
}
Return
Activ97:
if EnterOn97 > 0
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind97%{enter}
}
Else
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind97%
}
Return
Activ98:
if EnterOn98 > 0
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind98%{enter}
}
Else
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind98%
}
Return
Activ99:
if EnterOn99 > 0
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind99%{enter}
}
Else
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind99%
}
Return
Activ100:
if EnterOn100 > 0
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind100%{enter}
}
Else
{
SendMessage, 0x50,, 0x4190419,, A
SendInput {f6}%Bind100%
}
Return
BindDel:
loop 100
{
GuiControl,, Hot%A_Index%,
GuiControl,, Bind%A_Index%,
GuiControl,, EnterOn%A_Index%, 0
}
Goto, BinderActive
return
GuiClose:
Gui, Submit, NoHide
IniWrite, %Drop%, %A_WorkingDir%\Settings.ini, Main, Profile
ExitApp