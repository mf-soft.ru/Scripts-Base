; ####################
; #### SAMP UDF R15.1 ####
; SAMP Version: 0.3.7
; Written by Chuck_Floyd 
; https://github.com/FrozenBrain
; Modified by Suchty112
; https://github.com/Suchty112
; Modified by: paul-phoenix
; https://github.com/paul-phoenix
; Modified by: Agrippa1994
; https://github.com/agrippa1994
; Modified by: RawDev and ELon
; Do not remove these lines.
; �������� SA-MP-UDF-Addon by [CM}MurKotik
; VK: http://vk.com/id350689538
; ####################

; SAMP-RP CHat Colors
global COLOR_DEPARTAMENT := "0xFFFF8282"
global COLOR_WANTED := "0xFFffc801"
global COLOR_WANTEDME := "0xFFff6347"
global COLOR_NEWS := "0xFF00d900"
global COLOR_SMS := "0xFFffff00"
global COLOR_ME := "0xFFc2a2da"
global COLOR_ADMIN := "0xFFff6347"
global COLOR_MDC_HEADER := "0xFF8d8dff"
global COLOR_MDC_CONTENT := "0xFFFFFFFF"
global COLOR_CHAT1 := "0xFFc8c8c8"
global COLOR_CHAT2 := "0xFFaaaaaa"
global COLOR_CHAT3 := "0xFF6e6e6e"
global COLOR_WHISPER := "0xFF6e6e6e"
global COLOR_GOV_HEADER := "0xFFFFFFFF"
global COLOR_GOV_CONTENT := "0xFF2641fe"
global COLOR_RACION := "0xFF8d8dff"
global COLOR_AMMO_TAKE := "0xFF09b000"
global COLOR_TRY := "0xFFff8000"
global COLOR_TRY_COMPLETE := "0xFF00ab06"
global COLOR_TRY_FAILURE := "0xFFc42100"
global COLOR_ANTIFLOOD := "0xFFffd5bb"
global COLOR_PASS_HEADER := "0xFFffff00"
global COLOR_PASS_CONTENT := "0xFFf5deb3"
global COLOR_LIC_HEADER := "0xFF059bd3"
global COLOR_LIC_CONTENT := "0xFFc5eefe"

; Samp-udf-addon
global ADDR_SET_POSITION := 0xB7CD98
global ADDR_SET_POSITION_OFFSET := 0x14
global ADDR_SET_POSITION_X_OFFSET := 0x30
global ADDR_SET_POSITION_Y_OFFSET := 0x34
global ADDR_SET_POSITION_Z_OFFSET := 0x38
global ADDR_SET_INTERIOR_OFFSET := 0xB72914
global SAMP_SZIP_OFFSET := 0x20
global SAMP_SZHOSTNAME_OFFSET := 0x121
global SAMP_INFO_SETTINGS_OFFSET := 0x3C5

; ErrorLevels
global ERROR_OK                     := 0
global ERROR_PROCESS_NOT_FOUND      := 1
global ERROR_OPEN_PROCESS           := 2
global ERROR_INVALID_HANDLE         := 3
global ERROR_MODULE_NOT_FOUND       := 4
global ERROR_ENUM_PROCESS_MODULES   := 5
global ERROR_ZONE_NOT_FOUND         := 6
global ERROR_CITY_NOT_FOUND         := 7
global ERROR_READ_MEMORY            := 8
global ERROR_WRITE_MEMORY           := 9
global ERROR_ALLOC_MEMORY           := 10
global ERROR_FREE_MEMORY            := 11
global ERROR_WAIT_FOR_OBJECT        := 12
global ERROR_CREATE_THREAD          := 13

; GTA Addresses
global ADDR_ZONECODE                := 0xA49AD4      ;Player Zone
global ADDR_POSITION_X              := 0xB6F2E4      ;Player X Position
global ADDR_POSITION_Y              := 0xB6F2E8      ;Player Y Position
global ADDR_POSITION_Z              := 0xB6F2EC      ;Player Z Position
global ADDR_CPED_PTR                := 0xB6F5F0      ;Player CPED Pointer
global ADDR_CPED_HPOFF              := 0x540         ;Player Health
global ADDR_CPED_ARMOROFF           := 0x548         ;Player Armour
global ADDR_CPED_MONEY              := 0x0B7CE54     ;Player Money
global ADDR_CPED_INTID              := 0xA4ACE8      ;Player Interior-ID
global ADDR_CPED_SKINIDOFF          := 0x22          ;Player Skin-ID
;
global ADDR_VEHICLE_PTR             := 0xBA18FC      ;Vehicle CPED Pointer
global ADDR_VEHICLE_HPOFF           := 0x4C0         ;Vehicle Health
global ADDR_VEHICLE_DOORSTATE       := 0x4F8         ;Vehicle Door Status
global ADDR_VEHICLE_ENGINESTATE     := 0x428         ;Vehicle Engine Status
global ADDR_VEHICLE_LIGHTSTATE      := 0x584         ;Vehicle Light Status
global ADDR_VEHICLE_MODEL           := 0x22          ;Vehicle Car-ID & Car-Name
global ADDR_VEHICLE_TYPE            := 0x590         ;Vehicle Typ-ID (1 = Car)
global ADDR_VEHICLE_DRIVER          := 0x460         ;Vehicle Driver
global ADDR_VEHICLE_X               := 0x44          ;Vehicle Speed X
global ADDR_VEHICLE_Y               := 0x48          ;Vehicle Speed Y
global ADDR_VEHICLE_Z               := 0x4C          ;Vehicle Speed Z


global oAirplaneModels := [417, 425, 447, 460, 469, 476, 487, 488, 497, 511, 512, 513, 519, 520, 548, 553, 563, 577, 592, 593]
global oBikeModels := [481,509,510]
global ovehicleNames := ["Landstalker","Bravura","Buffalo","Linerunner","Perrenial","Sentinel","Dumper","Firetruck","Trashmaster","Stretch","Manana","Infernus","Voodoo","Pony","Mule","Cheetah","Ambulance","Leviathan","Moonbeam","Esperanto","Taxi","Washington","Bobcat","Whoopee","BFInjection","Hunter","Premier","Enforcer","Securicar","Banshee","Predator","Bus","Rhino","Barracks","Hotknife","Trailer","Previon","Coach","Cabbie","Stallion","Rumpo","RCBandit","Romero","Packer","Monster","Admiral","Squalo","Seasparrow","Pizzaboy","Tram","Trailer","Turismo","Speeder","Reefer","Tropic","Flatbed","Yankee","Caddy","Solair","Berkley'sRCVan","Skimmer","PCJ-600","Faggio","Freeway","RCBaron","RCRaider","Glendale","Oceanic","Sanchez","Sparrow","Patriot","Quad","Coastguard","Dinghy","Hermes","Sabre","Rustler","ZR-350","Walton","Regina","Comet","BMX","Burrito","Camper","Marquis","Baggage","Dozer","Maverick","NewsChopper","Rancher","FBIRancher","Virgo","Greenwood","Jetmax","Hotring","Sandking","BlistaCompact","PoliceMaverick","Boxvillde","Benson","Mesa","RCGoblin","HotringRacerA","HotringRacerB","BloodringBanger","Rancher","SuperGT","Elegant","Journey","Bike","MountainBike","Beagle","Cropduster","Stunt","Tanker","Roadtrain","Nebula","Majestic","Buccaneer","Shamal","hydra","FCR-900","NRG-500","HPV1000","CementTruck","TowTruck","Fortune","Cadrona","FBITruck","Willard","Forklift","Tractor","Combine","Feltzer","Remington","Slamvan","Blade","Freight","Streak","Vortex","Vincent","Bullet","Clover","Sadler","Firetruck","Hustler","Intruder","Primo","Cargobob","Tampa","Sunrise","Merit","Utility","Nevada","Yosemite","Windsor","Monster","Monster","Uranus","Jester","Sultan","Stratum","Elegy","Raindance","RCTiger","Flash","Tahoma","Savanna","Bandito","FreightFlat","StreakCarriage","Kart","Mower","Dune","Sweeper","Broadway","Tornado","AT-400","DFT-30","Huntley","Stafford","BF-400","NewsVan","Tug","Trailer","Emperor","Wayfarer","Euros","Hotdog","Club","FreightBox","Trailer","Andromada","Dodo","RCCam","Launch","PoliceCar","PoliceCar","PoliceCar","PoliceRanger","Picador","S.W.A.T","Alpha","Phoenix","GlendaleShit","SadlerShit","Luggage","Luggage","Stairs","Boxville","Tiller","UtilityTrailer"]
global oweaponNames := ["Fist","Brass Knuckles","Golf Club","Nightstick","Knife","Baseball Bat","Shovel","Pool Cue","Katana","Chainsaw","Purple Dildo","Dildo","Vibrator","Silver Vibrator","Flowers","Cane","Grenade","Tear Gas","Molotov Cocktail", "", "", "", "9mm","Silenced 9mm","21Desert Eagle","Shotgun","Sawnoff Shotgun","Combat Shotgun","Micro SMG/Uzi","MP5","AK-47","M4","Tec-9","Country Rifle","Sniper Rifle","RPG","HS Rocket","Flamethrower","Minigun","Satchel Charge","Detonator","Spraycan","Fire Extinguisher","Camera","Night Vis Goggles","Thermal Goggles","Parachute"]
global oradiostationNames := ["Playback FM", "K Rose", "K-DST", "Bounce FM", "SF-UR", "Radio Los Santos", "Radio X", "CSR 103.9", "K-JAH West", "Master Sounds 98.3", "WCTR Talk Radio", "User Track Player", "Radio Off"]
global oweatherNames := ["EXTRASUNNY_LA", "SUNNY_LA", "EXTRASUNNY_SMOG_LA", "SUNNY_SMOG_LA", "CLOUDY_LA", "SUNNY_SF", "EXTRASUNNY_SF", "CLOUDY_SF", "RAINY_SF", "FOGGY_SF", "SUNNY_VEGAS", "EXTRASUNNY_VEGAS", "CLOUDY_VEGAS", "EXTRASUNNY_COUNTRYSIDE", "SUNNY_COUNTRYSIDE", "CLOUDY_COUNTRYSIDE", "RAINY_COUNTRYSIDE", "EXTRASUNNY_DESERT", "SUNNY_DESERT", "SANDSTORM_DESERT", "UNDERWATER", "EXTRACOLOURS_1", "EXTRACOLOURS_2"]

; SAMP Addresses
global ADDR_SAMP_INCHAT_PTR             := 0x21a10c
global ADDR_SAMP_INCHAT_PTR_OFF         := 0x55
global ADDR_SAMP_USERNAME               := 0x219A6F
global FUNC_SAMP_SENDCMD                := 0x65c60
global FUNC_SAMP_SENDSAY                := 0x57f0
global FUNC_SAMP_ADDTOCHATWND           := 0x64520
global ADDR_SAMP_CHATMSG_PTR            := 0x21a0e4
global FUNC_SAMP_SHOWGAMETEXT           := 0x9c2c0
global FUNC_SAMP_PLAYAUDIOSTR           := 0x62da0
global FUNC_SAMP_STOPAUDIOSTR           := 0x629a0
; ########################## Dialog Styles ##########################
global DIALOG_STYLE_MSGBOX			:= 0
global DIALOG_STYLE_INPUT 			:= 1
global DIALOG_STYLE_LIST			:= 2
global DIALOG_STYLE_PASSWORD		:= 3
global DIALOG_STYLE_TABLIST			:= 4
global DIALOG_STYLE_TABLIST_HEADERS	:= 5


; ######################### Dialog Structure #########################
global SAMP_DIALOG_STRUCT_PTR					:= 0x21A0B8
global SAMP_DIALOG_PTR1_OFFSET				:= 0x1C
global SAMP_DIALOG_LINES_OFFSET 			:= 0x44C
global SAMP_DIALOG_INDEX_OFFSET				:= 0x443
global SAMP_DIALOG_BUTTON_HOVERING_OFFSET	:= 0x465
global SAMP_DIALOG_BUTTON_CLICKED_OFFSET	:= 0x466
global SAMP_DIALOG_PTR2_OFFSET 				:= 0x20
global SAMP_DIALOG_LINECOUNT_OFFSET			:= 0x150
global SAMP_DIALOG_OPEN_OFFSET				:= 0x28
global SAMP_DIALOG_STYLE_OFFSET				:= 0x2C
global SAMP_DIALOG_ID_OFFSET				:= 0x30
global SAMP_DIALOG_TEXT_PTR_OFFSET			:= 0x34
global SAMP_DIALOG_CAPTION_OFFSET			:= 0x40
global FUNC_SAMP_SHOWDIALOG				 	:= 0x6B9C0
global FUNC_SAMP_CLOSEDIALOG				:= 0x6C040

global FUNC_UPDATESCOREBOARD                := 0x8A10
global SAMP_INFO_OFFSET                     := 0x21A0F8
global ADDR_SAMP_CRASHREPORT 				:= 0x5CF2C
global SAMP_PPOOLS_OFFSET                   := 0x3CD
global SAMP_PPOOL_PLAYER_OFFSET             := 0x18
global SAMP_SLOCALPLAYERID_OFFSET           := 0x4
global SAMP_ISTRLEN_LOCALPLAYERNAME_OFFSET  := 0x1A
global SAMP_SZLOCALPLAYERNAME_OFFSET        := 0xA
global SAMP_PSZLOCALPLAYERNAME_OFFSET       := 0xA
global SAMP_PREMOTEPLAYER_OFFSET            := 0x2E
global SAMP_ISTRLENNAME___OFFSET            := 0x1C
global SAMP_SZPLAYERNAME_OFFSET             := 0xC
global SAMP_PSZPLAYERNAME_OFFSET            := 0xC
global SAMP_ILOCALPLAYERPING_OFFSET         := 0x26
global SAMP_ILOCALPLAYERSCORE_OFFSET        := 0x2A
global SAMP_IPING_OFFSET                    := 0x28
global SAMP_ISCORE_OFFSET                   := 0x24
global SAMP_ISNPC_OFFSET                    := 0x4
global SAMP_PLAYER_MAX                      := 1004
global CheckpointCheck 						:= 0xC7DEEA
global rmaddrs 								:= [0xC7DEC8, 0xC7DECC, 0xC7DED0]

; Sizes
global SIZE_SAMP_CHATMSG := 0xFC
; Internal
global hGTA := 0x0
global dwGTAPID := 0x0
global dwSAMP := 0x0
global pMemory := 0x0
global pParam1 := 0x0
global pParam2 := 0x0
global pParam3 := 0x0
global pParam4                         := 0x0
global pParam5                         := 0x0
global pInjectFunc := 0x0
global nZone := 1
global nCity := 1
global bInitZaC := 0
global iRefreshScoreboard := 0
global oScoreboardData := ""
global iRefreshHandles := 0
global iUpdateTick := 2500 ;time in ms, used for getPlayerNameById etc. to refresh data

; ###############################################################################################################################
; # 														                                                                    #
; # SAMP-Funktionen:                                                                                                            #
; #     - IsSAMPAvailable()                         PrÃ¼ft, ob man in den Chat schreiben kann & ob GTA geladen ist            #
; #     - isInChat()                                PrÃ¼ft, ob der Spieler gerade chattet oder in einem Dialog ist            #
; #     - getUsername()                             Liest den Namen des Spielers aus                                            #
; #     - getId()                                   Liest die Id des Spielers aus                                               #
; #     - SendChat(wText)                           Sendet eine Nachricht od. einen Befehl direkt an den Server                 #
; #     - addChatMessage(wText)                     FÃ¼gt eine Zeile in den Chat ein (nur fÃ¼r den Spieler sichtbar)        #
; #     - showGameText(wText, dwTime, dwTextsize)   Zeigt einen Text inmitten des Bildschirmes an  					            #
; #     - playAudioStream(wUrl)                     Spielt einen "Audio Stream" ab                                              #
; #     - stopAudioStream()                         Stoppt den aktuellen Audio Stream                                           #
; #	    - GetChatLine(Line, Output)		            Liest die eingestellte Zeile aus,				                            #
; #						                            Optionale Parameter (timestamp=0, color=0)			                        #
; # 	- blockChatInput() 							Eine Funktion um Messages zum Server zu blockieren			                #
; # 	- unBlockChatInput() 						Eine Funktion um Messages zum Server zu entblockieren			            #
; # --------------------------------------------------------------------------------------------------------------------------- #
; # 														                                                                    #
; #     - getServerName()                           Ermittelt den Server-Namen (HostName)  										#
; #     - getServerIP()                             Ermittelt die IP des Servers  										        #
; #     - getServerPort()                           Ermittelt den Port des Servers  										    #
; #     - CountOnlinePlayers()                      Ermittelt wie viele Spieler auf dem Server Online sind.                     #
; # 														                                                                    #
; # --------------------------------------------------------------------------------------------------------------------------- #
; # 														                                                                    #
; #	    - getWeatherID()			                Ermittelt ob der Spieler freezed ist                                        #
; #	    - getWeatherName()			                Ermittelt ob der Spieler freezed ist                                        #
; # 														                                                                    #
; # --------------------------------------------------------------------------------------------------------------------------- #
; # 														                                                                    #
; #     - patchRadio() (interner stuff) 										                                                #
; #     - unPatchRadio() (interner stuff)											                                            #
; # 														                                                                    #
; ###############################################################################################################################
; # SAMP Dialog Funktionen (v0.3.7):																	                        #
; # --------------------------------------------------------------------------------------------------------------------------- #
; #	- isDialogOpen() - Prüft, ob gerade ein Dialog angezeigt wird (gibt true oder false zurück)	                       		#
; #	- getDialogStyle() - Liest den Typ des (zuletzt) angezeigten Dialogs aus (0-5)                      						#
; #	- getDialogID() - Liest die ID des (zuletzt) angezeigten Dialogs aus (auch vom Server)	                        			#
; #	- setDialogID(id) - Setzt die ID des (zuletzt) angezeigten Dialogs auf [id]				                        			#
; #	- getDialogIndex() - Liest die (zuletzt) ausgewählte Zeile des Dialogs aus 				                        		#
; #	- getDialogCaption() - Liest die Überschrift des (zuletzt) angezeigten Dialogs aus 			                       		#
; #	- getDialogText() - Liest den Text des (zuletzt) angezeigten Dialogs aus (auch bei Listen)                              	#
; #	- getDialogLineCount() - Liest die Anzahl der Zeilen/Items des (zuletzt) angezeigten Dialogs aus                        	#
; #	- getDialogLine(index) - Liest die Zeile an der Stelle [index] mittels getDialogText aus 		                        	#
; #	- getDialogLines() - Liest die Zeilen mittels getDialogText aus (gibt ein Array zurück)			                      	#
; #	- isDialogButton1Selected() - Prüft, ob Button1 des Dialogs ausgewählt ist 						                       	#
; # - getDialogStructPtr() - Liest den Base Pointer zur Dialogstruktur aus (intern genutzt)			                        	#
; #																									                        	#
; #	- showDialog(style, caption, text, button1, button2, id) - Zeigt einen Dialog an (nur lokal)	                        	#
; ###############################################################################################################################
; ###############################################################################################################################
; # 														                                                                    #
; # Extra-Player-Funktionen:                                                                                                    #
; #	    - getTargetPed(dwPED)   			        Zeigt die PED-ID, des Spielers, auf den man zielt.                          #
; #     - getPedById(dwId)                          Zeigt die PED-Id zu der Id.                                                 #
; #     - getIdByPed(dwId)                          Zeigt die Id der PED-Id.                                                    #
; #     - getStreamedInPlayersInfo()                Zeigt Informationen Ã¼ber die gestreamten Spieler.                        #
; #     - callFuncForAllStreamedInPlayers()         FÃ¼hrt bestimmte Funktionen, fÃ¼r die gestreamten Spieler aus.          #
; #	    - getDist(pos1,pos2)   			            Rechnet den Abstand zwischen zwei Positionen aus.                           #
; #     - getClosestPlayerPed()                     Zeigt die PED-ID, des Spielers, der am nahesten zu einem steht.             #
; #     - getClosestPlayerId()                      Zeigt die Id, des Spielers, der am nahesten zu einem steht.                 #
; #	    - getPedCoordinates(dwPED)   			    Zeigt die Koordinaten, der PED-ID.                                          #
; #     - getTargetPosById(dwId)                    Zeigt die Position, zu der angegebenen Id.                                  #
; #     - getTargetPlayerSkinIdByPed(dwPED)         Zeigt den Skin, zu der angegebenen PED-ID.                                  #
; #     - getTargetPlayerSkinIdById(dwId)           Zeigt den Skin, zu der angegebenen ID.                                      #
; #     - calcScreenCoors(fX,fY,fZ)                 > WordToScreen Funktion <                                                   #
; # 														                                                                    #
; # Extra-Player-Fahrzeug-Funktionen:                                                                                           #
; #	    - getVehiclePointerByPed(dwPED)   			Zeigt die PED-ID des Autos.                                                 #
; #	    - getVehiclePointerById(dwId)   			Zeigt die PED-ID des Autos.                                                 #
; #     - isTargetInAnyVehicleByPed(dwPED)          Zeigt ob der Spieler in einem Auto ist.                                     #
; #     - isTargetInAnyVehicleById(dwId)            Zeigt ob der Spieler in einem Auto ist.                                     #
; #     - getTargetVehicleHealthByPed(dwPED)        Zeigt ob der Spieler in einem Auto ist.                                     #
; #     - getTargetVehicleHealthById(dwId)          Zeigt ob der Spieler in einem Auto ist.                                     #
; #     - getTargetVehicleTypeByPed(dwPED)          Ermittelt den FahrzeugTyp (Auto, LKW etc.)                                  #
; #     - getTargetVehicleTypeById(dwId)            Ermittelt den FahrzeugTyp (Auto, LKW etc.)                                  #
; #     - getTargetVehicleModelIdByPed(dwPED)       Ermittelt die Fahrzeugmodell ID                                             #
; #     - getTargetVehicleModelIdById(dwId)         Ermittelt die Fahrzeugmodell ID                                             #
; #     - getTargetVehicleModelNameByPed(dwPED)     Ermittelt den Fahrzeugmodell Namen 				                            #
; #     - getTargetVehicleModelNameById(dwId)       Ermittelt den Fahrzeugmodell Namen 				                            #
; #     - getTargetVehicleLightStateByPed(dwPED)    Ermittelt den Lichtzustand des Autos 			                            #
; #     - getTargetVehicleLightStateById(dwId)      Ermittelt den Lichtzustand des Autos 			                            #
; #     - getTargetVehicleEngineStateByPed(dwPED)   Ermittelt den Motorzustand des Autos 			                            #
; #     - getTargetVehicleEngineStateById(dwId)     Ermittelt den Motorzustand des Autos 			                            #
; #     - getTargetVehicleLockStateByPed(dwPED)     Ermittelt ob das Auto auf oder zu ist 			                            #
; #     - getTargetVehicleLockStateById(dwId)       Ermittelt ob das Auto auf oder zu ist 			                            #
; #     - getTargetVehicleColor1ByPed(dwPED)        Ermittelt die 1. Color-ID des Autos 			                            #
; #     - getTargetVehicleColor1ById(dwId)          Ermittelt die 1. Color-ID des Autos 			                            #
; #     - getTargetVehicleColor2ByPed(dwPED)        Ermittelt die 2. Color-ID des Autos 			                            #
; #     - getTargetVehicleColor2ById(dwId)          Ermittelt die 2. Color-ID des Autos 			                            #
; #     - getTargetVehicleSpeedByPed(dwPED)         Ermittelt die Geschwindigkeit des Autos			                            #
; #     - getTargetVehicleSpeedById(dwId)           Ermittelt die Geschwindigkeit des Autos			                            #
; # 														                                                                    #
; ###############################################################################################################################
; # 														                                                                    #
; # Scoreboard-Funktionen:                                                                                                      #
; #     - getPlayerScoreById(dwId)                  Zeigt den Score zu der Id                                                   #
; #     - getPlayerPingById(dwId)                   Zeigt den Ping zu der Id                                                    #
; #     - getPlayerNameById(dwId)                   Zeigt den Namen zu der Id                                                   #
; #     - getPlayerIdByName(wName)                  Zeigt die Id zu dem Namen                                                   #
; #     - updateScoreboardDataEx()                  Aktualisiert Scoreboard Inhalte (wird implizit aufgerufen)                  #
; #     - updateOScoreboardData()                   Aktualisiert Scoreboard Inhalte (wird implizit aufgerufen)                  #
; #	    - isNPCById(dwId)   			            Zeigt an ob die ID ein NPC 						                            #
; # 														                                                                    #
; ###############################################################################################################################
; # 														                                                                    #
; # Spielerfunktionen:                                                                                                          #
; #     - getPlayerHealth()                         Ermittelt die HP des Spielers                                               #
; #     - getPlayerArmour()                         Ermittelt den RÃ¼stungswert des Spielers                                  #
; # 	- getPlayerInteriorId()			            Ermittelt die Interior ID wo der Spieler ist 		                        #
; # 	- getPlayerSkinId()			                Ermittelt die Skin ID des Spielers           		                        #
; # 	- getPlayerMoney() 			                Ermittelt den Kontostand des Spielers (nur GTA Intern)                      #
; #	    - getPlayerWanteds()			            Ermittelt die Wantedanzahl des Spielers (nur bis 6 Wanteds)                 #
; #	    - getPlayerWeaponId()			            Ermittelt die Waffen ID des Spielers                                        #
; #	    - getPlayerWeaponName()			            Ermittelt den Namen, der Waffe des Spielers                                 #
; #	    - getPlayerState()			                Ermittelt den "Status" des Spielers (Zu FuÃŸ, Fahrer, Tot)                #
; #	    - getPlayerMapPosX()			            Ermittelt die X-Position auf der Map im Menu                                #
; #	    - getPlayerMapPosY()			            Ermittelt die Y-Position auf der Map im Menu                                #
; #	    - getPlayerMapZoom()			            Ermittelt den Zoom auf der Map im Menu                                      #
; #	    - IsPlayerFreezed()			                Ermittelt ob der Spieler freezed ist                                        #
; # 														                                                                    #
; ###############################################################################################################################
; # 														                                                                    #
; # Fahrzeugfunktionen:                                                                                                         #
; #     - isPlayerInAnyVehicle()                    Ermittelt, ob sich der Spieler in einem Fahrzeug befindet                   #
; #     - getVehicleHealth()                        Ermittelt die HP des Fahrzeugs, in dem der Spieler sitzt                    #
; # 	- isPlayerDriver() 			                Ermittelt ob der Spieler Fahrer des Auto's ist		                        #
; # 	- getVehicleType() 			                Ermittelt den FahrzeugTyp (Auto, LKW etc.)                                  #
; # 	- getVehicleModelId()			            Ermittelt die Fahrzeugmodell ID 				                            #
; # 	- getVehicleModelName() 		            Ermittelt den Fahrzeugmodell Namen 				                            #
; # 	- getVehicleLightState() 		            Ermittelt den Lichtzustand des Autos 			                            #
; # 	- getVehicleEngineState() 		            Ermittelt den Motorzustand des Autos 			                            #
; # 	- getVehicleLockState() 		            Ermittelt ob das Auto auf oder zu ist 			                            #
; # 	- getVehicleColor1() 		                Ermittelt die 1. Farb ID des Autos   			                            #
; # 	- getVehicleColor2() 		                Ermittelt die 2. Farb ID des Autos   			                            #
; # 	- getVehicleSpeed() 		                Ermittelt die Geschwindigkeit des Autos   			                        #
; # 	- getPlayerRadiostationID() 		        Ermittelt die Radiostation-ID des Autos   			                        #
; # 	- getPlayerRadiostationName() 		        Ermittelt den Radiostation-Namen des Autos   			                    #
; # 														                                                                    #
; ###############################################################################################################################
; # 														                                                                    #
; # Standpunktbestimmung:                                                                                                       #
; #     - getCoordinates()                          Ermittelt die aktuelle Position (Koordinaten)                               #
; #	    - getPlayerPos(X,Y,Z) 			            siehe oben drÃ¼ber 						                                #
; # 														                                                                    #
; # --------------------------------------------------------------------------------------------------------------------------- #
; # 														                                                                    #
; #     - initZonesAndCities()                      Initialisiert eine Liste aller Standartgebiete                              #
; #                                                 (Voraussetzung fÃ¼r die folgenden Funktionen dieser Kategorie)            #
; #     - calculateZone(X, Y, Z)                    Bestimmt die Zone (= Stadtteil) aus den geg. Koordinaten                    #
; #     - calculateCity(X, Y, Z)                    Bestimmt die Stadt aus den geg. Koordinaten                                 #
; #     - getCurrentZonecode()                      Ermittelt die aktulle Zone in Kurzform                                      #
; #     - AddZone(Name, X1, Y1, Z1, X2, Y2, Z2)     FÃ¼gt eine Zone zum Index hinzu                                           #
; #     - AddCity(Name, X1, Y1, Z1, X2, Y2, Z2)     FÃ¼gt eine Stadt zum Index hinzu                                          #
; #	    - IsPlayerInRangeOfPoint(X, Y, Z, Radius)   Bestimmt ob der Spieler in der NÃ¤he der Koordinaten ist                  #
; #	    - IsIsPlayerInRangeOfPoint2D(X, Y, Radius)  Bestimmt ob der Spieler in der NÃ¤he der Koordinaten ist                  #
; #	    - getPlayerZone()                   				                                                                    #
; #	    - getPlayerCity()                   					                                                                #
; # 														                                                                    #
; ###############################################################################################################################
; # Sonstiges:                                                                                                                  #
; #     - checkHandles()                                                                                                        #
; #     - AntiCrash()								Hilft gegen das abstÃ¼rzen bei Warningscodes                              #
; ###############################################################################################################################
; # Speicherfunktionen (intern genutzt):                                                                                        #
; # Memory Functions:                                                                                                           #
; #     - checkHandles()                                                                                                        #
; #     - refreshGTA()                                                                                                          #
; #     - refreshSAMP()                                                                                                         #
; #     - refreshMemory()                                                                                                       #
; #     - getPID(szWindow)                                                                                                      #
; #     - openProcess(dwPID, dwRights)                                                                                          #
; #     - closeProcess(hProcess)                                                                                                #
; #     - getModuleBaseAddress(sModule, dwPID)                                                                                  #
; #     - readString(hProcess, dwAddress, dwLen)                                                                                #
; #     - readFloat(hProcess, dwAddress)                                                                                        #
; #     - readDWORD(hProcess, dwAddress)                                                                                        #
; #     - readMem(hProcess, dwAddress, dwLen=4, type="UInt")                                                                    #
; #     - writeString(hProcess, dwAddress, wString)                                                                             #
; #     - writeRaw(hProcess, dwAddress, data, dwLen)                                                                            #
; #     - Memory_ReadByte(process_handle, address)                                                                              #
; #     - callWithParams(hProcess, dwFunc, aParams, bCleanupStack = true)                                                       #
; #     - virtualAllocEx(hProcess, dwSize, flAllocationType, flProtect)                                                         #
; #     - virtualFreeEx(hProcess, lpAddress, dwSize, dwFreeType)                                                                #
; #     - createRemoteThread(hProcess, lpThreadAttributes, dwStackSize, lpStartAddress, lpParameter,                            #
; #     - dwCreationFlags, lpThreadId)                                                                                          #
; #     - waitForSingleObject(hThread, dwMilliseconds)                                                                          #
; #     - __ansiToUnicode(sString, nLen = 0)                                                                                    #
; #     - __unicodeToAnsi(wString, nLen = 0)                                                                                    #
; ###############################################################################################################################
; ###############################################################################################################################
; # by {CM}MurKotik                                                                                                             #
; # Samp-UDF-Addon function        
; #     - setCoordinates(x, y, z, Interior)         - �������� ��������� 
; #     - getIP()                                   - ����� IP ����� �������                                                   #
; #     - setIP(IP)                                 - ������ IP ����� �������                                                   #
; #     - getHostname()                             - ����� ��� �������                                                        #
; #     - setUsername(Username)                     - ���������� ����� ���                                                      #
; #     - connect(IP)                               - ����������� � ������� �� IP ������                                        #
; #     - colorhud(����)                            - ������� ������ ���� � ���� �� ��������                                   #
; #	    - setTime(hour)	                            - ������������� ����� �� �������                                            #
; #	    - getSkinID()   	                        - ����� ID ����� ������ ��������                                           #
; #	    - getDialogTitle() 	                        - ����� ��������� ���������� �������                                       #
; #	    - getPlayerColor(id)                        - ����� ID ����� ���� ������ �� ID                                         #
; #	    - setPlayerColor(id,color)                  - ������������� ���� ���� ������ �� ��� ID                                  #
; #	    - colorToStr(color)	                        - ����������� ����� �� ���������� � �����������������                       #
; #	    - getWeaponId() 	                        - ����� ID ������ ������ ���������                                         #
; #     - restartGameEx()                           - ������� ���� (�� ��������� �� �������)                                    #
; #     - setrestart()                              - ���������� ���������� (���������)                                         #
; #     - disconnectEx()                            - ���������� �� �������                                                     #
; #     - writeFloat(hProcess, dwAddress, wFloat)   - ������ � ������� �������� � ����������                                    #
; #     - writeByte(hProcess, dwAddress, wInt)      - ������ � ������� �������� � ����������                                    #
; #     - FloatToHex(value)                         - ������ � ������� �������� � �����������������                             #
; #     - writeByte(hProcess, dwAddress, wInt)      - ������ � ������� �������� � �����������������                             #
; #     - IntToHex(int)                             - ������ � ������� �������� � �����������������                             #
; ###############################################################################################################################
; ###############################################################################################################################
; # by McFree                                                                                                            #
; # Samp-UDF-Addon function        
; #     - getPlayerPosById(dwid)         - �������� ���������� ��������� �� ID
; #     - addChatMessageEx(Color, wText)         - ��������� ���� ��������� � ������ ������� ����� (������� timestamp)
; #     - HexToDec(Hex)         - ������� ������������������ ����� � ����������
; #     - hex2rgb(CR )      - ������� ������������������ ����� � rgb ������ (255,255,255)
; #     - rgb2hex(R, G, B)         - ������� RGB ����� � ����������������� �������� (FFFFFF)
; ###############################################################################################################################

; ##### Sa-mp-udf-addon by McFree #####

getPlayerPosById(dwId) {
    dwId += 0
    dwId := Floor(dwId)
    if(dwId < 0 || dwId >= SAMP_PLAYER_MAX)
        return ""
    
    if(iRefreshScoreboard+iUpdateTick > A_TickCount)
    {
        if(oScoreboardData[dwId])
        {
			if(oScoreboardData[dwId].HasKey("PED"))
				return getPedCoordinates(oScoreboardData[dwId].PED)
			if(oScoreboardData[dwId].HasKey("MPOS"))
				return oScoreboardData[dwId].MPOS
		}
        return ""
    }
    
    if(!updateOScoreboardData())
        return ""
    
    if(oScoreboardData[dwId])
    {
		if(oScoreboardData[dwId].HasKey("PED"))
			return getPedCoordinates(oScoreboardData[dwId].PED)
		if(oScoreboardData[dwId].HasKey("MPOS"))
			return oScoreboardData[dwId].MPOS
	}
    return ""
}

addChatMessageEx(Color, wText) {
   wText := "" wText

   if(!checkHandles())
       return false
   
    VarSetCapacity(data2, 4, 0)
   NumPut(Color,data2,0,"Int")
    
   Addrr := readDWORD(hGTA, dwSAMP+ADDR_SAMP_CHATMSG_PTR)
   
   VarSetCapacity(data1, 4, 0)
   NumPut(readDWORD(hGTA, Addrr + 0x12A), data1,0,"Int") 
   
   WriteRaw(hGTA, Addrr + 0x12A, &data2, 4)
    
   dwFunc := dwSAMP + FUNC_SAMP_ADDTOCHATWND
   dwChatInfo := readDWORD(hGTA, dwSAMP + ADDR_SAMP_CHATMSG_PTR)
   if(ErrorLevel) {
       ErrorLevel := ERROR_READ_MEMORY
       return false
   }
   
   callWithParams(hGTA, dwFunc, [["p", dwChatInfo], ["s", wText]], true)
   WriteRaw(hGTA, Addrr + 0x12A, &data1, 4)
   
   ErrorLevel := ERROR_OK
    
   return true
}

HexToDec(hex)
{
    VarSetCapacity(dec, 66, 0)
    , val := DllCall("msvcrt.dll\_wcstoui64", "Str", hex, "UInt", 0, "UInt", 16, "CDECL Int64")
    , DllCall("msvcrt.dll\_i64tow", "Int64", val, "Str", dec, "UInt", 10, "CDECL")
    return dec
}
hex2rgb(CR)
{
    NumPut((InStr(CR, "#") ? "0x" SubStr(CR, 2) : "0x") SubStr(CR, -5), (V := "000000"))
    return NumGet(V, 2, "UChar") "," NumGet(V, 1, "UChar") "," NumGet(V, 0, "UChar")
}
rgb2hex(R, G, B, H := 1)
{
    static U := A_IsUnicode ? "_wcstoui64" : "_strtoui64"
    static V := A_IsUnicode ? "_i64tow"    : "_i64toa"
    rgb := ((R << 16) + (G << 8) + B)
    H := ((H = 1) ? "#" : ((H = 2) ? "0x" : ""))
    VarSetCapacity(S, 66, 0)
    value := DllCall("msvcrt.dll\" U, "Str", rgb , "UInt", 0, "UInt", 10, "CDECL Int64")
    DllCall("msvcrt.dll\" V, "Int64", value, "Str", S, "UInt", 16, "CDECL")
    return H S
}

; ##### Sa-mp-udf-addon by [CM]MurKotik #####

setCoordinates(x, y, z, Interior) {
    if(!checkHandles())
        return False

    dwAddress := readMem(hGTA, ADDR_SET_POSITION)
    if(ErrorLevel) {
        ErrorLevel := ERROR_READ_MEMORY
        Return False
    }

    dwAddress := readMem(hGTA, dwAddress + ADDR_SET_POSITION_OFFSET)
    if(ErrorLevel) {
        ErrorLevel := ERROR_READ_MEMORY
        Return False
    }

    Sleep 100

    writeByte(hGTA, ADDR_SET_INTERIOR_OFFSET, Interior)
    if(ErrorLevel) {
        ErrorLevel := ERROR_WRITE_MEMORY
        Return False
    }

    writeFloat(hGTA, dwAddress + ADDR_SET_POSITION_X_OFFSET, x)
    if(ErrorLevel) {
        ErrorLevel := ERROR_WRITE_MEMORY
        Return False
    }

    writeFloat(hGTA, dwAddress + ADDR_SET_POSITION_Y_OFFSET, y)
    if(ErrorLevel) {
        ErrorLevel := ERROR_WRITE_MEMORY
        Return False
    }

    writeFloat(hGTA, dwAddress + ADDR_SET_POSITION_Z_OFFSET, z)
    if(ErrorLevel) {
        ErrorLevel := ERROR_WRITE_MEMORY
        Return False
    }

    Return True
}
colorhud(colorhud) 
{ 
VarSetCapacity(idvar,32,0) 
VarSetCapacity(processhandle,32,0) 
VarSetCapacity(value, 32, 0) 
NumPut(colorhud,value,0,Uint) 
address=0xBAB230
WinGet ,idvar,PID,GTA:SA:MP
processhandle:=DllCall("OpenProcess","Uint",0x38,"int",0,"int",idvar) 
Bvar:=DllCall("WriteProcessMemory","Uint",processhandle,"Uint",address+0,"Uint",&value,"Uint","4","Uint",0) 
}
getIP() {
    if(!checkHandles())
        return ""

    dwAddress := readDWORD(hGTA, dwSAMP + SAMP_INFO_OFFSET)
    if(ErrorLevel || dwAddress==0) {
        ErrorLevel := ERROR_READ_MEMORY
        return ""
    }

    ipAddr := readString(hGTA, dwAddress + SAMP_SZIP_OFFSET, 257)
    if(ErrorLevel) {
        ErrorLevel := ERROR_READ_MEMORY
        return ""
    }

    return ipAddr
}

; ����� Ip �������
setIP(IP) {
    if(!checkHandles())
        return False

    dwAddress := readDWORD(hGTA, dwSAMP + SAMP_INFO_OFFSET)
    if(ErrorLevel || dwAddress==0) {
        ErrorLevel := ERROR_READ_MEMORY
        return False
    }

    writeString(hGTA, dwAddress + SAMP_SZIP_OFFSET, IP)
    if(ErrorLevel) {
        ErrorLevel := ERROR_WRITE_MEMORY
        return False
    }

    return True
}

; returns server hostname or empty string on error
getHostname() {
    if(!checkHandles())
        return ""

    dwAddress := readDWORD(hGTA, dwSAMP + SAMP_INFO_OFFSET)
    if(ErrorLevel || dwAddress==0) {
        ErrorLevel := ERROR_READ_MEMORY
        return ""
    }

    hostname := readString(hGTA, dwAddress + SAMP_SZHOSTNAME_OFFSET, 259)
    if(ErrorLevel) {
        ErrorLevel := ERROR_READ_MEMORY
        return ""
    }

    return hostname
}

; set nickname
setUsername(Username) {
    if(!checkHandles())
        return False

    dwAddress := dwSAMP + ADDR_SAMP_USERNAME
    writeString(hGTA, dwAddress, Username)
    if(ErrorLevel) {
        ErrorLevel := ERROR_WRITE_MEMORY
        return False
    }

    ErrorLevel := ERROR_OK
    return True
}
setChatLine(line, msg) {
	if(!checkHandles())
		return -1

	dwPtr := dwSAMP + ADDR_SAMP_CHATMSG_PTR
	dwAddress := readDWORD(hGTA,dwPtr)
	if(ErrorLevel) {
		ErrorLevel := ERROR_READ_MEMORY
		return -1
  }

	; 0x152, offset for first msg
	; 0xFC, size of a msg
	; 99, max msg
	writeString(hGTA, dwAddress + 0x152 + ( (99-line) * 0xFC), msg)
	if(ErrorLevel) {
		ErrorLevel := ERROR_WRITE_MEMORY
		return -1
  }

	ErrorLevel := ERROR_OK
	return True
}

getTagNameDistance() {
	if(!checkHandles())
		return -1

	dwSAMPInfo := readDWORD(hGTA, dwSAMP + SAMP_INFO_OFFSET)
	if(ErrorLevel) {
		ErrorLevel := ERROR_READ_MEMORY
		return -1
  }

	dwSAMPInfoSettings := readDWORD(hGTA, dwSAMPInfo + SAMP_INFO_SETTINGS_OFFSET)
	if(ErrorLevel) {
		ErrorLevel := ERROR_READ_MEMORY
		return -1
  }

  distance := readFloat(hGTA, dwSAMPInfoSettings + 0x27)
	if(ErrorLevel) {
		ErrorLevel := ERROR_READ_MEMORY
		return -1
  }

	ErrorLevel := ERROR_OK
	return distance
}

setTagNameDistance(status, distance) {
	if(!checkHandles())
		return -1

	status := status ? 1 : 0

	dwSAMPInfo := readDWORD(hGTA, dwSAMP + SAMP_INFO_OFFSET)
	if(ErrorLevel) {
		ErrorLevel := ERROR_READ_MEMORY
		return -1
  }

	dwSAMPInfoSettings := readDWORD(hGTA, dwSAMPInfo + SAMP_INFO_SETTINGS_OFFSET)
	if(ErrorLevel) {
		ErrorLevel := ERROR_READ_MEMORY
		return -1
  }

	writeByte(hGTA, dwSAMPInfoSettings + 0x38, 1)
	if(ErrorLevel) {
		ErrorLevel := ERROR_WRITE_MEMORY
		return -1
  }

	writeByte(hGTA, dwSAMPInfoSettings + 0x2F, status)
	if(ErrorLevel) {
		ErrorLevel := ERROR_WRITE_MEMORY
		return -1
  }

	writeFloat(hGTA, dwSAMPInfoSettings + 0x27, distance)
	if(ErrorLevel) {
		ErrorLevel := ERROR_WRITE_MEMORY
		return -1
  }

	ErrorLevel := ERROR_OK
	return
}

; sets the ingame hour (day/night)
setTime(hour)
{
	if(!checkHandles())
		return
	; disable gta setTime function
	VarSetCapacity(nop, 6, 0)
	Loop 6 {
		NumPut(0x90, nop, A_INDEX-1, "UChar")
	}
	writeRaw(hGTA, 0x52D168, &nop, 6)

	; set our own weather
	VarSetCapacity(time, 1, 0)
	NumPut(hour, time, 0, "Int")
	writeRaw(hGTA, 0xB70153, &time, 1)
}

; sets the weather
; @param weather id
setWeather(id)
{
	if(!checkHandles())
		return
	VarSetCapacity(weather, 1, 0)
	NumPut(id, weather, 0, "Int")
	writeRaw(hGTA, 0xC81320, &weather, 1)
	if(ErrorLevel)
		return false


	return true
}

; get the id of your skin
getSkinID() {
	if(!checkHandles())
		return -1

	dwAddress := readDWORD(hGTA, 0xB6F3B8)

	if(ErrorLevel || dwAddress==0) {
		ErrorLevel := ERROR_READ_MEMORY
		return -1
	}

	id := readMem(hGTA, dwAddress + 0x22, 2, "UShort")
	if(ErrorLevel) {
		ErrorLevel := ERROR_READ_MEMORY
		return -1
	}
	ErrorLevel := ERROR_OK
	return id
}

; get the title of dialog
getDialogTitle()
{
	if(!checkHandles())
		return ""
	dwAddress := readDWORD(hGTA, dwSAMP + 0x21A0B8)
	if(ErrorLevel) {
		ErrorLevel := ERROR_READ_MEMORY
		return ""
	}
	text := readString(hGTA, dwAddress + 0x40, 128)
	if(ErrorLevel) {
		ErrorLevel := ERROR_READ_MEMORY
		return ""
	}

	ErrorLevel := ERROR_OK
	return text
}
; get the color of player
; @author democrazy
getPlayerColor(id)
{
    id += 0

    if(!checkHandles())
        return -1

    color := readDWORD(hGTA, dwSAMP + 0x216378 + 4*id)
    if(ErrorLevel) {
        ErrorLevel := ERROR_READ_MEMORY
        return -1
    }
    return color
}

; set the color of player
; @author democrazy
setPlayerColor(id,color)
{
    id += 0
    color +=0

    if(!checkHandles())
        return

    VarSetCapacity(bla, 4, 0)
    NumPut(color,bla,0,"UInt")

    writeRaw(hGTA, dwSAMP + 0x216378 + 4*id, &bla, 4)
}
; convert color from decimal into hex
; @author democrazy
colorToStr(color)
{
    color += 0
    color >>= 8
    color &= 0xffffff
    SetFormat, IntegerFast, hex
    color += 0
    color .= ""
    StringTrimLeft, color, color, 2
    SetFormat, IntegerFast, d
    return "{" color "}"
}

; get current weapon id
; @author AnalFatal
getWeaponId()
{
    If(!checkHandles())
    return 0

   c := readDWORD(hGTA, ADDR_CPED_PTR)
   If(ErrorLevel) {
   		ErrorLevel := ERROR_READ_MEMORY
   		return 0
   }
   id := readMem(hGTA, c + 0x740)
   If(ErrorLevel) {
   		ErrorLevel := ERROR_READ_MEMORY
   		return 0
   }

   return id
}
writeFloat(hProcess, dwAddress, wFloat) {
    if(!hProcess) {
        ErrorLevel := ERROR_INVALID_HANDLE
        return false
    }

    wFloat := FloatToHex(wFloat)

    dwRet := DllCall(   "WriteProcessMemory"
                        , "UInt", hProcess
                        , "UInt", dwAddress
                        , "UInt *", wFloat
                        , "UInt", 4
                        , "UInt *", 0)

    ErrorLevel := ERROR_OK
    return true
}

writeByte(hProcess, dwAddress, wInt) {
  if(!hProcess) {
      ErrorLevel := ERROR_INVALID_HANDLE
      return false
  }

  wInt := IntToHex(wInt)

	dwRet := DllCall(     "WriteProcessMemory"
	                      , "UInt", hProcess
	                      , "UInt", dwAddress
	                      , "UInt *", wInt
	                      , "UInt", 1
	                      , "UInt *", 0)
}

FloatToHex(value) {
   format := A_FormatInteger
   SetFormat, Integer, H
   result := DllCall("MulDiv", Float, value, Int, 1, Int, 1, UInt)
   SetFormat, Integer, %format%
   return, result
}

IntToHex(int)
{
	CurrentFormat := A_FormatInteger
	SetFormat, integer, hex
	int += 0
	SetFormat, integer, %CurrentFormat%
	return int
}

disconnectEx() {
	
	if(!checkHandles())
		return 0
	
	dwAddress := readDWORD(hGTA, dwSAMP + SAMP_INFO_OFFSET)			;g_SAMP
	if(ErrorLevel || dwAddress==0) {
		ErrorLevel := ERROR_READ_MEMORY
		return 0
	}
	
	dwAddress := readDWORD(hGTA, dwAddress + 0x3c9)			;pRakClientInterface
	if(ErrorLevel || dwAddress==0) {
		ErrorLevel := ERROR_READ_MEMORY
		return 0
	}
    
    ecx := dwAddress        ;this
	
	dwAddress := readDWORD(hGTA, dwAddress)         ;vtable
	if(ErrorLevel || dwAddress==0) {
		ErrorLevel := ERROR_READ_MEMORY
		return 0
	}
	
	
	
	VarSetCapacity(injectData, 24, 0) ;mov, call, retn
	
    NumPut(0xB9, injectData, 0, "UChar")	;mov ecx 	0+1
	NumPut(ecx, injectData, 1, "UInt")			;1+4
    
	NumPut(0xB8, injectData, 5, "UChar")	;mov eax 	5+1
	NumPut(dwAddress, injectData, 6, "UInt")			;6+4
	
	;NumPut(0x006A006A, injectData, 10, "UInt")  ; 2x push			10+4
    
    NumPut(0x68, injectData, 10, "UChar")		;10 + 1		;push style
	NumPut(0, injectData, 11, "UInt")		;11 + 4
    
    NumPut(0x68, injectData, 15, "UChar")		;15 + 1		;push style
	NumPut(500, injectData, 16, "UInt")		;16 + 4
    
    ;---
	
	NumPut(0x50FF, injectData, 20, "UShort")			;20 + 2
	NumPut(0x08, injectData, 22, "UChar")			;22 + 1
	
	NumPut(0xC3, injectData, 23, "UChar")	;retn		23+1
	
	writeRaw(hGTA, pInjectFunc, &injectData, 24)
	if(ErrorLevel)
		return false
	
	hThread := createRemoteThread(hGTA, 0, 0, pInjectFunc, 0, 0, 0)
	if(ErrorLevel)
		return false
	
	waitForSingleObject(hThread, 0xFFFFFFFF)

  closeProcess(hThread)

	return true
}
setrestart()
{
  VarSetCapacity(old, 4, 0)
  dwAddress := readDWORD(hGTA, dwSAMP + SAMP_INFO_OFFSET)			;g_SAMP
    if(ErrorLevel || dwAddress==0) {
      ErrorLevel := ERROR_READ_MEMORY
      return 0
  }

  NumPut(9,old,0,"Int")
  writeRaw(hGTA, dwAddress + 957, &old, 4)
}
restartGameEx() {
	
	if(!checkHandles())
		return -1
	
	dwAddress := readDWORD(hGTA, dwSAMP + SAMP_INFO_OFFSET)			;g_SAMP
	if(ErrorLevel || dwAddress==0) {
		ErrorLevel := ERROR_READ_MEMORY
		return -1
	}
	
	dwFunc := dwSAMP + 0xA060
	
	VarSetCapacity(injectData, 11, 0) ;mov, call, retn
	
	NumPut(0xB9, injectData, 0, "UChar")	;mov ecx 	0+1
	NumPut(dwAddress, injectData, 1, "UInt")			;1+4
	NumPut(0xE8, injectData, 5, "UChar")	;call 		5+1
	offset := dwFunc - (pInjectFunc + 10)
	NumPut(offset, injectData, 6, "Int")	;			6+4
    NumPut(0xC3, injectData, 10, "UChar")	;		10+1
	
	writeRaw(hGTA, pInjectFunc, &injectData, 11)
	if(ErrorLevel)
		return false

	hThread := createRemoteThread(hGTA, 0, 0, pInjectFunc, 0, 0, 0)
	if(ErrorLevel)
		return false

	waitForSingleObject(hThread, 0xFFFFFFFF)

  closeProcess(hThread)

	return true
}
; ##### SAMP-Funktionen #####
IsSAMPAvailable() {
    if(!checkHandles())
        return false
	
	dwChatInfo := readDWORD(hGTA, dwSAMP + ADDR_SAMP_CHATMSG_PTR)
	
	if(dwChatInfo == 0 || dwChatInfo == "ERROR")
	{
		return false
	}
	else
	{
		return true
	}
}

isInChat() {
    if(!checkHandles())
        return -1
    
    dwPtr := dwSAMP + ADDR_SAMP_INCHAT_PTR
    dwAddress := readDWORD(hGTA, dwPtr) + ADDR_SAMP_INCHAT_PTR_OFF
    if(ErrorLevel) {
        ErrorLevel := ERROR_READ_MEMORY
        return -1
    }
    
    dwInChat := readDWORD(hGTA, dwAddress)
    if(ErrorLevel) {
        ErrorLevel := ERROR_READ_MEMORY
        return -1
    }
    
    ErrorLevel := ERROR_OK
    if(dwInChat > 0) {
        return true
    } else {
        return false
    }
}

getUsername() {
    if(!checkHandles())
        return ""
    
    dwAddress := dwSAMP + ADDR_SAMP_USERNAME
    sUsername := readString(hGTA, dwAddress, 25)
    if(ErrorLevel) {
        ErrorLevel := ERROR_READ_MEMORY
        return ""
    }
    
    ErrorLevel := ERROR_OK
    return sUsername
}

getId() {
    s:=getUsername()
    return getPlayerIdByName(s)
}


SendChat(wText) {
     wText := "" wText
    
    if(!checkHandles())
        return false
    
    dwFunc:=0
    if(SubStr(wText, 1, 1) == "/") {
        dwFunc := dwSAMP + FUNC_SAMP_SENDCMD
    } else {
        dwFunc := dwSAMP + FUNC_SAMP_SENDSAY
    }
    
    callWithParams(hGTA, dwFunc, [["s", wText]], false)
    
    ErrorLevel := ERROR_OK
    return true
}

addChatMessage(wText) {
    wText := "" wText

    if(!checkHandles())
        return false
    
    dwFunc := dwSAMP + FUNC_SAMP_ADDTOCHATWND
    dwChatInfo := readDWORD(hGTA, dwSAMP + ADDR_SAMP_CHATMSG_PTR)
    if(ErrorLevel) {
        ErrorLevel := ERROR_READ_MEMORY
        return false
    }
    
    callWithParams(hGTA, dwFunc, [["p", dwChatInfo], ["s", wText]], true)
    
    ErrorLevel := ERROR_OK
    return true
}

showGameText(wText, dwTime, dwSize) {
    wText := "" wText
    dwTime += 0
    dwTime := Floor(dwTime)
    dwSize += 0
    dwSize := Floor(dwSize)

    if(!checkHandles())
        return false
    
    dwFunc := dwSAMP + FUNC_SAMP_SHOWGAMETEXT
    
    callWithParams(hGTA, dwFunc, [["s", wText], ["i", dwTime], ["i", dwSize]], false)
    
    ErrorLevel := ERROR_OK
    return true
}

playAudioStream(wUrl) {
    wUrl := "" wUrl
    
    if(!checkHandles())
        return false
    
    dwFunc := dwSAMP + FUNC_SAMP_PLAYAUDIOSTR
    
    patchRadio()
    
    callWithParams(hGTA, dwFunc, [["s", wUrl], ["i", 0], ["i", 0], ["i", 0], ["i", 0], ["i", 0]], false)
    
    unPatchRadio()
    
    ErrorLevel := ERROR_OK
    return true
}

stopAudioStream() {
    if(!checkHandles())
        return false
    
    dwFunc := dwSAMP + FUNC_SAMP_STOPAUDIOSTR
    
    patchRadio()
    
    callWithParams(hGTA, dwFunc, [["i", 1]], false)
    
    unPatchRadio()
    
    ErrorLevel := ERROR_OK
    return true
}

patchRadio()
{
    if(!checkHandles())
        return false
    
    VarSetCapacity(nop, 4, 0)
    NumPut(0x90909090,nop,0,"UInt")
    
    dwFunc := dwSAMP + FUNC_SAMP_PLAYAUDIOSTR
    writeRaw(hGTA, dwFunc, &nop, 4)
    writeRaw(hGTA, dwFunc+4, &nop, 1)
    
    dwFunc := dwSAMP + FUNC_SAMP_STOPAUDIOSTR
    writeRaw(hGTA, dwFunc, &nop, 4)
    writeRaw(hGTA, dwFunc+4, &nop, 1)
    return true
}

unPatchRadio()
{
    if(!checkHandles())
        return false
    
    VarSetCapacity(old, 4, 0)
    
    dwFunc := dwSAMP + FUNC_SAMP_PLAYAUDIOSTR
    NumPut(0x74003980,old,0,"UInt")
    writeRaw(hGTA, dwFunc, &old, 4)
    NumPut(0x39,old,0,"UChar")
    writeRaw(hGTA, dwFunc+4, &old, 1)
    
    dwFunc := dwSAMP + FUNC_SAMP_STOPAUDIOSTR
    NumPut(0x74003980,old,0,"UInt")
    writeRaw(hGTA, dwFunc, &old, 4)
    NumPut(0x09,old,0,"UChar")
    writeRaw(hGTA, dwFunc+4, &old, 1)
    return true
}

blockChatInput() {
    if(!checkHandles())
        return false
    
    VarSetCapacity(nop, 2, 0)
    
    dwFunc := dwSAMP + FUNC_SAMP_SENDSAY
    NumPut(0x04C2,nop,0,"Short")
    writeRaw(hGTA, dwFunc, &nop, 2)
    
    dwFunc := dwSAMP + FUNC_SAMP_SENDCMD
    writeRaw(hGTA, dwFunc, &nop, 2)
    
    return true
}

unBlockChatInput() {
    if(!checkHandles())
        return false
    
    VarSetCapacity(nop, 2, 0)
    
    dwFunc := dwSAMP + FUNC_SAMP_SENDSAY
    NumPut(0xA164,nop,0,"Short")
    writeRaw(hGTA, dwFunc, &nop, 2)
    
    dwFunc := dwSAMP + FUNC_SAMP_SENDCMD
    writeRaw(hGTA, dwFunc, &nop, 2)
    
    return true
}

getServerName() {
    if(!checkHandles())
        return -1
    
    dwAdress := readMem(hGTA, dwSAMP + 0x21A0F8, 4, "int")
    if(ErrorLevel) {
        ErrorLevel := ERROR_READ_MEMORY
        return -1
    }
    
    if(!dwAdress)
        return -1
    
    ServerName := readString(hGTA, dwAdress + 0x121, 200)
    
    if(ErrorLevel) {
        ErrorLevel := ERROR_READ_MEMORY
        return -1
    }
    
    ErrorLevel := ERROR_OK
    return ServerName
}

getServerIP() {
    if(!checkHandles())
        return -1
    
    dwAdress := readMem(hGTA, dwSAMP + 0x21A0F8, 4, "int")
    if(ErrorLevel) {
        ErrorLevel := ERROR_READ_MEMORY
        return -1
    }
    
    if(!dwAdress)
        return -1
    
    ServerIP := readString(hGTA, dwAdress + 0x20, 100)
    if(ErrorLevel) {
        ErrorLevel := ERROR_READ_MEMORY
        return -1
    }
    
    ErrorLevel := ERROR_OK
    return ServerIP
}

getServerPort() {
    if(!checkHandles())
        return -1
    
    dwAdress := readMem(hGTA, dwSAMP + 0x21A0F8, 4, "int")
    if(ErrorLevel) {
        ErrorLevel := ERROR_READ_MEMORY
        return -1
    }
    
    if(!dwAdress)
        return -1
    
    ServerPort := readMem(hGTA, dwAdress + 0x225, 4, "int")
    if(ErrorLevel) {
        ErrorLevel := ERROR_READ_MEMORY
        return -1
    }
    
    ErrorLevel := ERROR_OK
    return ServerPort
}

getWeatherID() {
    if(!checkHandles())
        return -1
    
    dwGTA := getModuleBaseAddress("gta_sa.exe", hGTA)
    WeatherID := readMem(hGTA, dwGTA + 0xC81320, 2, "byte")
    if(ErrorLevel) {
        ErrorLevel := ERROR_READ_MEMORY
        return -1
    }
    
    ErrorLevel := ERROR_OK    
    return WeatherID
}

getWeatherName() {
    if(isPlayerInAnyVehicle() == 0)
        return -1
    
    if(id >= 0 && id < 23)
    {
        return oweatherNames[id-1]
    }
    return ""
}

; ##### Extra-Player-Funktionen #####

getTargetPed() {
	if(!checkHandles())
        return 0
	
	dwAddress := readDWORD(hGTA, 0xB6F3B8)
    if(ErrorLevel) {
        ErrorLevel := ERROR_READ_MEMORY
        return 0
    }
	if(!dwAddress)
		return 0
		
	dwAddress := readDWORD(hGTA, dwAddress+0x79C)
    if(ErrorLevel) {
        ErrorLevel := ERROR_READ_MEMORY
        return 0
    }
	
	ErrorLevel := ERROR_OK
	return dwAddress
}

calcScreenCoors(fX,fY,fZ)
{
	if(!checkHandles())
		return false
	
	dwM := 0xB6FA2C
	
	m_11 := readFloat(hGTA, dwM + 0*4)
	if(ErrorLevel) {
		ErrorLevel := ERROR_READ_MEMORY
		return false
	}
	
	m_12 := readFloat(hGTA, dwM + 1*4)
	m_13 := readFloat(hGTA, dwM + 2*4)
	m_21 := readFloat(hGTA, dwM + 4*4)
	m_22 := readFloat(hGTA, dwM + 5*4)
	m_23 := readFloat(hGTA, dwM + 6*4)
	m_31 := readFloat(hGTA, dwM + 8*4)
	m_32 := readFloat(hGTA, dwM + 9*4)
	m_33 := readFloat(hGTA, dwM + 10*4)
	m_41 := readFloat(hGTA, dwM + 12*4)
	m_42 := readFloat(hGTA, dwM + 13*4)
	m_43 := readFloat(hGTA, dwM + 14*4)
	
	dwLenX := readDWORD(hGTA, 0xC17044)
	if(ErrorLevel) {
		ErrorLevel := ERROR_READ_MEMORY
		return false
	}
	dwLenY := readDWORD(hGTA, 0xC17048)
	
	frX := fZ * m_31 + fY * m_21 + fX * m_11 + m_41
	frY := fZ * m_32 + fY * m_22 + fX * m_12 + m_42
	frZ := fZ * m_33 + fY * m_23 + fX * m_13 + m_43
	
	fRecip := 1.0/frZ
	frX *= fRecip * dwLenX
	frY *= fRecip * dwLenY
    
    if(frX<=dwLenX && frY<=dwLenY && frZ>1)
        return [frX,frY,frZ]
}

getPedById(dwId) {
    dwId += 0
    dwId := Floor(dwId)
    if(dwId < 0 || dwId >= SAMP_PLAYER_MAX)
        return 0
    
    if(iRefreshScoreboard+iUpdateTick > A_TickCount)
    {
        if(oScoreboardData[dwId])
        {
            if(oScoreboardData[dwId].HasKey("PED"))
                return oScoreboardData[dwId].PED
        }
        return 0
    }
    
    if(!updateOScoreboardData())
        return 0
    
    if(oScoreboardData[dwId])
    {
        if(oScoreboardData[dwId].HasKey("PED"))
            return oScoreboardData[dwId].PED
    }
    return 0
}

getIdByPed(dwPed) {
    dwPed += 0
    dwPed := Floor(dwPed)
	if(!dwPed)
		return -1
	
	if(iRefreshScoreboard+iUpdateTick > A_TickCount)
    {
		For i, o in oScoreboardData
        {
            if(o.HasKey("PED"))
            {
				if(o.PED==dwPed)
					return i
            }
        }
        return -1
    }
    
    if(!updateOScoreboardData())
        return -1
    
	For i, o in oScoreboardData
    {
        if(o.HasKey("PED"))
        {
			if(o.PED==dwPed)
				return i
        }
    }
    return -1
}

getStreamedInPlayersInfo() {
    r:=[]
    if(iRefreshScoreboard+iUpdateTick > A_TickCount)
    {
        For i, o in oScoreboardData
        {
            if(o.HasKey("PED"))
            {
                p := getPedCoordinates(o.PED)
                if(p)
                {
                    o.POS := p
                    r[i] := o
                }
            }
        }
        return r
    }
    
    if(!updateOScoreboardData())
        return ""
    
    For i, o in oScoreboardData
    {
        if(o.HasKey("PED"))
        {
            p := getPedCoordinates(o.PED)
            if(p)
            {
                o.POS := p
                r[i] := o
            }
        }
    }
    return r
}

callFuncForAllStreamedInPlayers(cfunc,dist=0x7fffffff) {
    cfunc := "" cfunc
    dist += 0
    if(!IsFunc(cfunc))
        return false
    p := getStreamedInPlayersInfo()
    if(!p)
        return false
    if(dist<0x7fffffff)
    {
        lpos := getCoordinates()
        if(!lpos)
            return false
        For i, o in p
        {
            if(dist>getDist(lpos,o.POS))
                %cfunc%(o)
        }
    }
    else
    {
        For i, o in p
            %cfunc%(o)
    }
    return true
}

getDist(pos1,pos2) {
	if(!pos1 || !pos2)
		return 0
    return Sqrt((pos1[1]-pos2[1])*(pos1[1]-pos2[1])+(pos1[2]-pos2[2])*(pos1[2]-pos2[2])+(pos1[3]-pos2[3])*(pos1[3]-pos2[3]))
}

getClosestPlayerPed() {
    dist := 0x7fffffff              ;max int32
    p := getStreamedInPlayersInfo()
	if(!p)
		return -1
    lpos := getCoordinates()
    if(!lpos)
        return -1
	id := -1
    For i, o in p
    {
        t:=getDist(lpos,o.POS)
        if(t<dist)
        {
            dist := t
            id := i
        }
    }
    PED := getPedById(id)
    return PED
}

getClosestPlayerId() {
    dist := 0x7fffffff              ;max int32
    p := getStreamedInPlayersInfo()
	if(!p)
		return -1
    lpos := getCoordinates()
    if(!lpos)
        return -1
	id := -1
    For i, o in p
    {
        t:=getDist(lpos,o.POS)
        if(t<dist)
        {
            dist := t
            id := i
        }
    }
    return id
}

CountOnlinePlayers() {
if(!checkHandles())
return -1
dwOnline := readDWORD(hGTA, dwSAMP + 0x21A0B4)
if(ErrorLevel) {
ErrorLevel := ERROR_READ_MEMORY
return -1
}
dwAddr := dwOnline + 0x4
OnlinePlayers := readDWORD(hGTA, dwAddr)
if(ErrorLevel) {
ErrorLevel := ERROR_READ_MEMORY
return -1
}
ErrorLevel := ERROR_OK
return OnlinePlayers
}

getPedCoordinates(dwPED) {
    dwPED += 0
    dwPED := Floor(dwPED)
    if(!dwPED)
        return ""
    
    if(!checkHandles())
        return ""

    dwAddress := readDWORD(hGTA, dwPED + 0x14)
    if(ErrorLevel || dwAddress==0) {
        ErrorLevel := ERROR_READ_MEMORY
        return ""
    }
    
    fX := readFloat(hGTA, dwAddress + 0x30)
    if(ErrorLevel) {
        ErrorLevel := ERROR_READ_MEMORY
        return ""
    }
    
    fY := readFloat(hGTA, dwAddress + 0x34)
    if(ErrorLevel) {
        ErrorLevel := ERROR_READ_MEMORY
        return ""
    }
    
    fZ := readFloat(hGTA, dwAddress + 0x38)
    if(ErrorLevel) {
        ErrorLevel := ERROR_READ_MEMORY
        return ""
    }
    
    ErrorLevel := ERROR_OK
    return [fX, fY, fZ]
}

getTargetPos(dwId) {
    dwId += 0
    dwId := Floor(dwId)
    if(dwId < 0 || dwId >= SAMP_PLAYER_MAX)
        return ""
    
    if(iRefreshScoreboard+iUpdateTick > A_TickCount)
    {
        if(oScoreboardData[dwId])
        {
			if(oScoreboardData[dwId].HasKey("PED"))
				return getPedCoordinates(oScoreboardData[dwId].PED)
			if(oScoreboardData[dwId].HasKey("MPOS"))
				return oScoreboardData[dwId].MPOS
		}
        return ""
    }
    
    if(!updateOScoreboardData())
        return ""
    
    if(oScoreboardData[dwId])
    {
		if(oScoreboardData[dwId].HasKey("PED"))
			return getPedCoordinates(oScoreboardData[dwId].PED)
		if(oScoreboardData[dwId].HasKey("MPOS"))
			return oScoreboardData[dwId].MPOS
	}
    return ""
}

getTargetPlayerSkinIdByPed(dwPED) {
    if(!checkHandles())
        return -1
    
    dwAddr := dwPED + ADDR_CPED_SKINIDOFF
    SkinID := readMem(hGTA, dwAddr, 2, "byte")
    if(ErrorLevel) {
        ErrorLevel := ERROR_READ_MEMORY
        return -1
    }
    
    ErrorLevel := ERROR_OK
    return SkinID
}

getTargetPlayerSkinIdById(dwId) {
    if(!checkHandles())
        return -1
    
    dwPED := getPedById(dwId)
    dwAddr := dwPED + ADDR_CPED_SKINIDOFF
    
    SkinID := readMem(hGTA, dwAddr, 2, "byte")
    if(ErrorLevel) {
        ErrorLevel := ERROR_READ_MEMORY
        return -1
    }
    
    ErrorLevel := ERROR_OK
    return SkinID
}

; ##### Extra-Player-Fahrzeug-Funktionenn #####

getVehiclePointerByPed(dwPED) {
    dwPED += 0
    dwPED := Floor(dwPED)
    if(!dwPED)
        return 0
	if(!checkHandles())
        return 0
	dwAddress := readDWORD(hGTA, dwPED + 0x58C)
    if(ErrorLevel) {
        ErrorLevel := ERROR_READ_MEMORY
        return 0
    }
	ErrorLevel := ERROR_OK
	return dwAddress
}

getVehiclePointerById(dwId) {
    if(!dwId)
        return 0
	if(!checkHandles())
        return 0
    
    dwPed_By_Id := getPedById(dwId)
    
	dwAddress := readDWORD(hGTA, dwPed_By_Id + 0x58C)
    if(ErrorLevel) {
        ErrorLevel := ERROR_READ_MEMORY
        return 0
    }
	ErrorLevel := ERROR_OK
	return dwAddress
}

isTargetInAnyVehicleByPed(dwPED)
{
    if(!checkHandles())
        return -1
    
    dwVehiclePointer := getVehiclePointerByPed(dwPedPointer)
    
    if(dwVehiclePointer > 0)
    {
        return 1
    }
    else if(dwVehiclePointer <= 0)
    {
        return 0
    }
    else
    {
        return -1
    }
}

isTargetInAnyVehiclebyId(dwId)
{
    if(!checkHandles())
        return -1
    
    dwPedPointer := getPedById(dwId)
    dwVehiclePointer := getVehiclePointerByPed(dwPedPointer)
    
    if(dwVehiclePointer > 0)
    {
        return 1
    }
    else if(dwVehiclePointer <= 0)
    {
        return 0
    }
    else
    {
        return -1
    }
}

getTargetVehicleHealthByPed(dwPed) {
    if(!checkHandles())
        return -1
    
    dwVehPtr := getVehiclePointerByPed(dwPed)    
    dwAddr := dwVehPtr + ADDR_VEHICLE_HPOFF
    fHealth := readFloat(hGTA, dwAddr)
    if(ErrorLevel) {
        ErrorLevel := ERROR_READ_MEMORY
        return -1
    }
    
    ErrorLevel := ERROR_OK
    return Round(fHealth)
}

getTargetVehicleHealthById(dwId) {
    if(!checkHandles())
        return -1
    
    dwVehPtr := getVehiclePointerById(dwId)    
    dwAddr := dwVehPtr + ADDR_VEHICLE_HPOFF
    fHealth := readFloat(hGTA, dwAddr)
    if(ErrorLevel) {
        ErrorLevel := ERROR_READ_MEMORY
        return -1
    }
    
    ErrorLevel := ERROR_OK
    return Round(fHealth)
}

getTargetVehicleTypeByPed(dwPED) {
    if(!checkHandles())
        return 0
    
    dwAddr := getVehiclePointerByPed(dwPED)
    
    if(!dwAddr)
        return 0
    
    cVal := readMem(hGTA, dwAddr + ADDR_VEHICLE_TYPE, 1, "Char")
    if(ErrorLevel) {
        ErrorLevel := ERROR_READ_MEMORY
        return 0
    }
    
    ErrorLevel := ERROR_OK
    if(!cVal)
    {
        mid := getVehicleModelId()
        Loop % oAirplaneModels.MaxIndex()
        {
            if(oAirplaneModels[A_Index]==mid)
                return 5
        }
        return 1
    }
    else if(cVal==5)
        return 2
    else if(cVal==6)
        return 3
    else if(cVal==9)
    {
        mid := getVehicleModelId()
        Loop % oBikeModels.MaxIndex()
        {
            if(oBikeModels[A_Index]==mid)
                return 6
        }
        return 4
    }
    return 0
}

getTargetVehicleTypeById(dwId) {
    if(!checkHandles())
        return 0
    
    dwAddr := getVehiclePointerById(dwId)
    
    if(!dwAddr)
        return 0
    
    cVal := readMem(hGTA, dwAddr + ADDR_VEHICLE_TYPE, 1, "Char")
    if(ErrorLevel) {
        ErrorLevel := ERROR_READ_MEMORY
        return 0
    }
    
    ErrorLevel := ERROR_OK
    if(!cVal)
    {
        mid := getVehicleModelId()
        Loop % oAirplaneModels.MaxIndex()
        {
            if(oAirplaneModels[A_Index]==mid)
                return 5
        }
        return 1
    }
    else if(cVal==5)
        return 2
    else if(cVal==6)
        return 3
    else if(cVal==9)
    {
        mid := getVehicleModelId()
        Loop % oBikeModels.MaxIndex()
        {
            if(oBikeModels[A_Index]==mid)
                return 6
        }
        return 4
    }
    return 0
}

getTargetVehicleModelIdByPed(dwPED) {
    if(!checkHandles())
        return 0
    
    dwAddr := getVehiclePointerByPed(dwPED)
    
    if(!dwAddr)
        return 0
    
    sVal := readMem(hGTA, dwAddr + ADDR_VEHICLE_MODEL, 2, "Short")
    if(ErrorLevel) {
        ErrorLevel := ERROR_READ_MEMORY
        return 0
    }
    
    ErrorLevel := ERROR_OK
    return sVal
}

getTargetVehicleModelIdById(dwId) {
    if(!checkHandles())
        return 0
    
    dwAddr := getVehiclePointerById(dwId)
    
    if(!dwAddr)
        return 0
    
    sVal := readMem(hGTA, dwAddr + ADDR_VEHICLE_MODEL, 2, "Short")
    if(ErrorLevel) {
        ErrorLevel := ERROR_READ_MEMORY
        return 0
    }
    
    ErrorLevel := ERROR_OK
    return sVal
}

getTargetVehicleModelNameByPed(dwPED) {
    id := getTargetVehicleModelIdByPed(dwPED)
    if(id > 400 && id < 611)
    {
        return ovehicleNames[id-399]
    }
    return ""
}

getTargetVehicleModelNameById(dwId) {
    id := getTargetVehicleModelIdById(dwId)
    if(id > 400 && id < 611)
    {
        return ovehicleNames[id-399]
    }
    return ""
}

getTargetVehicleLightStateByPed(dwPED) {
    if(!checkHandles())
        return -1
    
    dwAddr := getVehiclePointerByPed(dwPED)
    
    if(!dwAddr)
        return -1
    
    dwVal := readMem(hGTA, dwAddr + ADDR_VEHICLE_LIGHTSTATE, 4, "Int")
    if(ErrorLevel) {
        ErrorLevel := ERROR_READ_MEMORY
        return -1
    }
    
    ErrorLevel := ERROR_OK
    return (dwVal>0)
}

getTargetVehicleLightStateById(dwId) {
    if(!checkHandles())
        return -1
    
    dwAddr := getVehiclePointerById(dwId)
    
    if(!dwAddr)
        return -1
    
    dwVal := readMem(hGTA, dwAddr + ADDR_VEHICLE_LIGHTSTATE, 4, "Int")
    if(ErrorLevel) {
        ErrorLevel := ERROR_READ_MEMORY
        return -1
    }
    
    ErrorLevel := ERROR_OK
    return (dwVal>0)
}

getTargetVehicleLockStateByPed(dwPED) {
    if(!checkHandles())
        return -1
    
    dwAddr := getVehiclePointerByPed(dwPED)
    
    if(!dwAddr)
        return -1
    
    dwVal := readDWORD(hGTA, dwAddr + ADDR_VEHICLE_DOORSTATE)
    if(ErrorLevel) {
        ErrorLevel := ERROR_READ_MEMORY
        return -1
    }
    
    ErrorLevel := ERROR_OK
    return (dwVal==2)
}

getTargetVehicleLockStateById(dwId) {
    if(!checkHandles())
        return -1
    
    dwAddr := getVehiclePointerById(dwId)
    
    if(!dwAddr)
        return -1
    
    dwVal := readDWORD(hGTA, dwAddr + ADDR_VEHICLE_DOORSTATE)
    if(ErrorLevel) {
        ErrorLevel := ERROR_READ_MEMORY
        return -1
    }
    
    ErrorLevel := ERROR_OK
    return (dwVal==2)
}

getTargetVehicleColor1byPed(dwPED) {
    if(!checkHandles())
        return 0
    
    dwAddr := getVehiclePointerByPed(dwPED)
    
    if(!dwAddr)
        return 0
	
    sVal := readMem(hGTA, dwAddr + 1076, 1, "byte")
	
    if(ErrorLevel) {
        ErrorLevel := ERROR_READ_MEMORY
        return 0
    }
    
    ErrorLevel := ERROR_OK
    return sVal
}

getTargetVehicleColor1byId(dwId) {
    if(!checkHandles())
        return 0
    
    dwAddr := getVehiclePointerById(dwId)
    
    if(!dwAddr)
        return 0
	
    sVal := readMem(hGTA, dwAddr + 1076, 1, "byte")
	
    if(ErrorLevel) {
        ErrorLevel := ERROR_READ_MEMORY
        return 0
    }
    
    ErrorLevel := ERROR_OK
    return sVal
}

getTargetVehicleColor2byPed(dwPED) {
    if(!checkHandles())
        return 0
    
    dwAddr := getVehiclePointerByPed(dwPED)
    
    if(!dwAddr)
        return 0
	
    sVal := readMem(hGTA, dwAddr + 1077, 1, "byte")
	
    if(ErrorLevel) {
        ErrorLevel := ERROR_READ_MEMORY
        return 0
    }
    
    ErrorLevel := ERROR_OK
    return sVal
}

getTargetVehicleColor2byId(dwId) {
    if(!checkHandles())
        return 0
    
    dwAddr := getVehiclePointerById(dwId)
    
    if(!dwAddr)
        return 0
	
    sVal := readMem(hGTA, dwAddr + 1077, 1, "byte")
	
    if(ErrorLevel) {
        ErrorLevel := ERROR_READ_MEMORY
        return 0
    }
    
    ErrorLevel := ERROR_OK
    return sVal
}

getTargetVehicleSpeedByPed(dwPED) {
    if(!checkHandles())
        return -1
 
    dwAddr := getVehiclePointerByPed(dwPED)
    
    fSpeedX := readMem(hGTA, dwAddr + ADDR_VEHICLE_X, 4, "float")
    fSpeedY := readMem(hGTA, dwAddr + ADDR_VEHICLE_Y, 4, "float")
    fSpeedZ := readMem(hGTA, dwAddr + ADDR_VEHICLE_Z, 4, "float")
    
    fVehicleSpeed :=  sqrt((fSpeedX * fSpeedX) + (fSpeedY * fSpeedY) + (fSpeedZ * fSpeedZ))
    fVehicleSpeed := (fVehicleSpeed * 100) * 1.43           ;Der Wert "1.43" ist meistens auf jedem Server anders. Die Geschwindigkeit wird somit erhÃ¶ht bzw. verringert
 
	return fVehicleSpeed
}

getTargetVehicleSpeedById(dwId) {
    if(!checkHandles())
        return -1
 
    dwAddr := getVehiclePointerById(dwId)
    
    fSpeedX := readMem(hGTA, dwAddr + ADDR_VEHICLE_X, 4, "float")
    fSpeedY := readMem(hGTA, dwAddr + ADDR_VEHICLE_Y, 4, "float")
    fSpeedZ := readMem(hGTA, dwAddr + ADDR_VEHICLE_Z, 4, "float")
    
    fVehicleSpeed :=  sqrt((fSpeedX * fSpeedX) + (fSpeedY * fSpeedY) + (fSpeedZ * fSpeedZ))
    fVehicleSpeed := (fVehicleSpeed * 100) * 1.43           ;Der Wert "1.43" ist meistens auf jedem Server anders. Die Geschwindigkeit wird somit erhÃ¶ht bzw. verringert
 
	return fVehicleSpeed
}
; ##### Scoreboard-Funktionen #####

getPlayerNameById(dwId) {
    dwId += 0
    dwId := Floor(dwId)
    if(dwId < 0 || dwId >= SAMP_PLAYER_MAX)
        return ""
    
    if(iRefreshScoreboard+iUpdateTick > A_TickCount)
    {
        if(oScoreboardData[dwId])
            return oScoreboardData[dwId].NAME
        return ""
    }
    
    if(!updateOScoreboardData())
        return ""
    
    if(oScoreboardData[dwId])
        return oScoreboardData[dwId].NAME
    return ""
}

getPlayerIdByName(wName) {
    wName := "" wName
    if(StrLen(wName) < 1 || StrLen(wName) > 24)
        return -1
    
    if(iRefreshScoreboard+iUpdateTick > A_TickCount)
    {
        For i, o in oScoreboardData
        {
            if(InStr(o.NAME,wName)==1)
                return i
        }
        return -1
    }
    
    if(!updateOScoreboardData())
        return -1
    
    For i, o in oScoreboardData
    {
        if(InStr(o.NAME,wName)==1)
            return i
    }
    return -1
}

getPlayerScoreById(dwId) {
    dwId += 0
    dwId := Floor(dwId)
    if(dwId < 0 || dwId >= SAMP_PLAYER_MAX)
        return ""
    
    if(iRefreshScoreboard+iUpdateTick > A_TickCount)
    {
        if(oScoreboardData[dwId])
            return oScoreboardData[dwId].SCORE
        return ""
    }
    
    if(!updateOScoreboardData())
        return ""
    
    if(oScoreboardData[dwId])
        return oScoreboardData[dwId].SCORE
    return ""
}

getPlayerPingById(dwId) {
    dwId += 0
    dwId := Floor(dwId)
    if(dwId < 0 || dwId >= SAMP_PLAYER_MAX)
        return -1
        
    if(iRefreshScoreboard+iUpdateTick > A_TickCount)
    {
        if(oScoreboardData[dwId])
            return oScoreboardData[dwId].PING
        return -1
    }
    
    if(!updateOScoreboardData())
        return -1
    
    if(oScoreboardData[dwId])
        return oScoreboardData[dwId].PING
    return -1
}

isNPCById(dwId) {
    dwId += 0
    dwId := Floor(dwId)
    if(dwId < 0 || dwId >= SAMP_PLAYER_MAX)
        return -1
    
    if(iRefreshScoreboard+iUpdateTick > A_TickCount)
    {
        if(oScoreboardData[dwId])
            return oScoreboardData[dwId].ISNPC
        return -1
    }
    
    if(!updateOScoreboardData())
        return -1
    
    if(oScoreboardData[dwId])
        return oScoreboardData[dwId].ISNPC
    return -1
}

; internal stuff
updateScoreboardDataEx() {
    
    if(!checkHandles())
        return false
    
    dwAddress := readDWORD(hGTA, dwSAMP + SAMP_INFO_OFFSET)            ;g_SAMP
    if(ErrorLevel || dwAddress==0) {
        ErrorLevel := ERROR_READ_MEMORY
        return false
    }
    
    dwFunc := dwSAMP + FUNC_UPDATESCOREBOARD
    
    VarSetCapacity(injectData, 11, 0) ;mov + call + retn
    
    NumPut(0xB9, injectData, 0, "UChar")
    NumPut(dwAddress, injectData, 1, "UInt")
    
    NumPut(0xE8, injectData, 5, "UChar")
    offset := dwFunc - (pInjectFunc + 10)
    NumPut(offset, injectData, 6, "Int")
    NumPut(0xC3, injectData, 10, "UChar")
    
    writeRaw(hGTA, pInjectFunc, &injectData, 11)
    if(ErrorLevel)
        return false
    
    hThread := createRemoteThread(hGTA, 0, 0, pInjectFunc, 0, 0, 0)
    if(ErrorLevel)
        return false
    
    waitForSingleObject(hThread, 0xFFFFFFFF)
    
    closeProcess(hThread)
    
    return true
    
}

; internal stuff
updateOScoreboardData() {
    if(!checkHandles())
        return 0
    
    oScoreboardData := []
    
    if(!updateScoreboardDataEx())
        return 0
    
    iRefreshScoreboard := A_TickCount
    
    dwAddress := readDWORD(hGTA, dwSAMP + SAMP_INFO_OFFSET)
    if(ErrorLevel || dwAddress==0) {
        ErrorLevel := ERROR_READ_MEMORY
        return 0
    }
    
    dwAddress := readDWORD(hGTA, dwAddress + SAMP_PPOOLS_OFFSET)
    if(ErrorLevel || dwAddress==0) {
        ErrorLevel := ERROR_READ_MEMORY
        return 0
    }
    
    dwPlayers := readDWORD(hGTA, dwAddress + SAMP_PPOOL_PLAYER_OFFSET)
    if(ErrorLevel || dwPlayers==0) {
        ErrorLevel := ERROR_READ_MEMORY
        return 0
    }
    
    wID := readMem(hGTA, dwPlayers + SAMP_SLOCALPLAYERID_OFFSET, 2, "Short")    ;sLocalPlayerID
    if(ErrorLevel) {
        ErrorLevel := ERROR_READ_MEMORY
        return 0
    }
    
    dwPing := readMem(hGTA, dwPlayers + SAMP_ILOCALPLAYERPING_OFFSET, 4, "Int")
    if(ErrorLevel) {
        ErrorLevel := ERROR_READ_MEMORY
        return 0
    }
    
    dwScore := readMem(hGTA, dwPlayers + SAMP_ILOCALPLAYERSCORE_OFFSET, 4, "Int")
    if(ErrorLevel) {
        ErrorLevel := ERROR_READ_MEMORY
        return 0
    }
    
    dwTemp := readMem(hGTA, dwPlayers + SAMP_ISTRLEN_LOCALPLAYERNAME_OFFSET, 4, "Int")    ;iStrlen_LocalPlayerName
    if(ErrorLevel) {
        ErrorLevel := ERROR_READ_MEMORY
        return 0
    }
    
    sUsername := ""
    if(dwTemp <= 0xf) {
        sUsername := readString(hGTA, dwPlayers + SAMP_SZLOCALPLAYERNAME_OFFSET, 16)       ;szLocalPlayerName
        if(ErrorLevel) {
            ErrorLevel := ERROR_READ_MEMORY
            return 0
        }
    }
    else {
        dwAddress := readDWORD(hGTA, dwPlayers + SAMP_PSZLOCALPLAYERNAME_OFFSET)        ;pszLocalPlayerName
        if(ErrorLevel) {
            ErrorLevel := ERROR_READ_MEMORY
            return 0
        }
        sUsername := readString(hGTA, dwAddress, 25)
        if(ErrorLevel) {
            ErrorLevel := ERROR_READ_MEMORY
            return 0
        }
    }
    oScoreboardData[wID] := Object("NAME", sUsername, "ID", wID, "PING", dwPing, "SCORE", dwScore, "ISNPC", 0)
    
    Loop, % SAMP_PLAYER_MAX
    {
        i := A_Index-1
        
        dwRemoteplayer := readDWORD(hGTA, dwPlayers+SAMP_PREMOTEPLAYER_OFFSET+i*4)      ;pRemotePlayer
        if(ErrorLevel) {
            ErrorLevel := ERROR_READ_MEMORY
            return 0
        }
        
        if(dwRemoteplayer==0)
            continue
        
        dwPing := readMem(hGTA, dwRemoteplayer + SAMP_IPING_OFFSET, 4, "Int")
        if(ErrorLevel) {
            ErrorLevel := ERROR_READ_MEMORY
            return 0
        }
        
        dwScore := readMem(hGTA, dwRemoteplayer + SAMP_ISCORE_OFFSET, 4, "Int")
        if(ErrorLevel) {
            ErrorLevel := ERROR_READ_MEMORY
            return 0
        }
        
        dwIsNPC := readMem(hGTA, dwRemoteplayer + SAMP_ISNPC_OFFSET, 4, "Int")
        if(ErrorLevel) {
            ErrorLevel := ERROR_READ_MEMORY
            return 0
        }
        
        dwTemp := readMem(hGTA, dwRemoteplayer + SAMP_ISTRLENNAME___OFFSET, 4, "Int")
        if(ErrorLevel) {
            ErrorLevel := ERROR_READ_MEMORY
            return 0
        }
        sUsername := ""
        if(dwTemp <= 0xf)
        {
            sUsername := readString(hGTA, dwRemoteplayer+SAMP_SZPLAYERNAME_OFFSET, 16)
            if(ErrorLevel) {
                ErrorLevel := ERROR_READ_MEMORY
                return 0
            }
        }
        else {
            dwAddress := readDWORD(hGTA, dwRemoteplayer + SAMP_PSZPLAYERNAME_OFFSET)
            if(ErrorLevel || dwAddress==0) {
                ErrorLevel := ERROR_READ_MEMORY
                return 0
            }
            sUsername := readString(hGTA, dwAddress, 25)
            if(ErrorLevel) {
                ErrorLevel := ERROR_READ_MEMORY
                return 0
            }
        }
        o := Object("NAME", sUsername, "ID", i, "PING", dwPing, "SCORE", dwScore, "ISNPC", dwIsNPC)
        oScoreboardData[i] := o
        
        dwRemoteplayerData := readDWORD(hGTA, dwRemoteplayer + 0x0)                ;pPlayerData
        if(ErrorLevel) {
            ErrorLevel := ERROR_READ_MEMORY
            return 0
        }
        if(dwRemoteplayerData==0)		;this ever happen?
            continue
		
		dwAddress := readDWORD(hGTA, dwRemoteplayerData + 489)        ;iGlobalMarkerLoaded
        if(ErrorLevel) {
            ErrorLevel := ERROR_READ_MEMORY
            return 0
        }
		if(dwAddress)
		{
			ix := readMem(hGTA, dwRemoteplayerData + 493, 4, "Int")        ;x map
			if(ErrorLevel) {
				ErrorLevel := ERROR_READ_MEMORY
				return 0
			}
			iy := readMem(hGTA, dwRemoteplayerData + 497, 4, "Int")        ;y map
			if(ErrorLevel) {
				ErrorLevel := ERROR_READ_MEMORY
				return 0
			}
			iz := readMem(hGTA, dwRemoteplayerData + 501, 4, "Int")        ;z map
			if(ErrorLevel) {
				ErrorLevel := ERROR_READ_MEMORY
				return 0
			}
			o.MPOS := [ix, iy, iz]
		}
        
        dwpSAMP_Actor := readDWORD(hGTA, dwRemoteplayerData + 0x0)                ;pSAMP_Actor
        if(ErrorLevel) {
            ErrorLevel := ERROR_READ_MEMORY
            return 0
        }
        if(dwpSAMP_Actor==0)               ;not streamed in
            continue

        dwPed := readDWORD(hGTA, dwpSAMP_Actor + 676)                ;pGTA_Ped_
        if(ErrorLevel) {
            ErrorLevel := ERROR_READ_MEMORY
            return 0
        }
        if(dwPed==0)
            continue
        o.PED := dwPed
		
		fHP := readFloat(hGTA, dwRemoteplayerData + 444)
		if(ErrorLevel) {
            ErrorLevel := ERROR_READ_MEMORY
            return 0
        }
		fARMOR := readFloat(hGTA, dwRemoteplayerData + 440)
		if(ErrorLevel) {
            ErrorLevel := ERROR_READ_MEMORY
            return 0
        }
		o.HP := fHP
		o.ARMOR := fARMOR
    }
    ErrorLevel := ERROR_OK
    return 1
}


; ##### Sonstiges #####
; written by David_Luchs %
; returns nth message of chatlog (beggining from bottom)
; -1 = error
GetChatLine(Line, ByRef Output, timestamp=0, color=0){
	chatindex := 0
	FileRead, file, %A_MyDocuments%\GTA San Andreas User Files\SAMP\chatlog.txt
	loop, Parse, file, `n, `r
	{
		if(A_LoopField)
			chatindex := A_Index
	}
	loop, Parse, file, `n, `r
	{
		if(A_Index = chatindex - line){
			output := A_LoopField
			break
		}
	}
	file := ""
	if(!timestamp)
		output := RegExReplace(output, "U)^\[\d{2}:\d{2}:\d{2}\]")
	if(!color)
		output := RegExReplace(output, "Ui)\{[a-f0-9]{6}\}")
	return
} 

; ##### Spielerfunktionen #####
getPlayerHealth() {
    if(!checkHandles())
        return -1
    
    dwCPedPtr := readDWORD(hGTA, ADDR_CPED_PTR)
    if(ErrorLevel) {
        ErrorLevel := ERROR_READ_MEMORY
        return -1
    }
    
    dwAddr := dwCPedPtr + ADDR_CPED_HPOFF
    fHealth := readFloat(hGTA, dwAddr)
    if(ErrorLevel) {
        ErrorLevel := ERROR_READ_MEMORY
        return -1
    }
    
    ErrorLevel := ERROR_OK
    return Round(fHealth)
}

getPlayerArmor() {
    if(!checkHandles())
        return -1
    
    dwCPedPtr := readDWORD(hGTA, ADDR_CPED_PTR)
    if(ErrorLevel) {
        ErrorLevel := ERROR_READ_MEMORY
        return -1
    }
    
    dwAddr := dwCPedPtr + ADDR_CPED_ARMOROFF
    fHealth := readFloat(hGTA, dwAddr)
    if(ErrorLevel) {
        ErrorLevel := ERROR_READ_MEMORY
        return -1
    }
    
    ErrorLevel := ERROR_OK
    return Round(fHealth)
}

getPlayerInteriorId() {
    if(!checkHandles())
        return -1
    
    iid := readMem(hGTA, ADDR_CPED_INTID, 4, "Int")
    if(ErrorLevel) {
        ErrorLevel := ERROR_READ_MEMORY
        return -1
    }
    
    ErrorLevel := ERROR_OK
    return iid
}

getPlayerSkinID() {
    if(!checkHandles())
        return -1
    
    dwCPedPtr := readDWORD(hGTA, ADDR_CPED_PTR)
    if(ErrorLevel) {
        ErrorLevel := ERROR_READ_MEMORY
        return -1
    }
    
    dwAddr := dwCPedPtr + ADDR_CPED_SKINIDOFF
    SkinID := readMem(hGTA, dwAddr, 2, "byte")
    if(ErrorLevel) {
        ErrorLevel := ERROR_READ_MEMORY
        return -1
    }
    
    ErrorLevel := ERROR_OK
    return SkinID
}

getPlayerMoney() {
    if(!checkHandles())
        return ""
    
    money := readMem(hGTA, ADDR_CPED_MONEY, 4, "Int")
    if(ErrorLevel) {
        ErrorLevel := ERROR_READ_MEMORY
        return ""
    }
    
    ErrorLevel := ERROR_OK
    return money
}

getPlayerWanteds() {
    if(!checkHandles())
        return -1
 
    dwPtr := 0xB7CD9C
    dwPtr := readDWORD(hGTA, dwPtr)
    if(ErrorLevel) {
        ErrorLevel := ERROR_READ_MEMORY
        return -1
    }
 
    Wanteds := readDWORD(hGTA, dwPtr)
    if(ErrorLevel) {
        ErrorLevel := ERROR_READ_MEMORY
        return -1
    }
 
    ErrorLevel := ERROR_OK
    return Wanteds
}

getPlayerWeaponId() {
    if(!checkHandles())
        return 0
    
    WaffenId := readMem(hGTA, 0xBAA410, 4, "byte")
    if(ErrorLevel) {
        ErrorLevel := ERROR_READ_MEMORY
        return -1
    }

   return WaffenId
}

getPlayerWeaponName() {
    id := getPlayerWeaponId()
    if(id >= 0 && id < 44)
    {
        return oweaponNames[id+1]
    }
    return ""
}

getPlayerState() {
    if(!checkHandles())
        return -1
    
    dwCPedPtr := readDWORD(hGTA, ADDR_CPED_PTR)
    if(ErrorLevel) {
        ErrorLevel := ERROR_READ_MEMORY
        return -1
    }
    
    State := readDWORD(hGTA, dwCPedPtr + 0x530)
    if(ErrorLevel) {
        ErrorLevel := ERROR_READ_MEMORY
        return -1
    }
    
    ErrorLevel := ERROR_OK
    return State
}

IsPlayerInMenu() {
    if(!checkHandles())
        return -1
    
    IsInMenu := readMem(hGTA, 0xBA67A4, 4, "byte")
    if(ErrorLevel) {
        ErrorLevel := ERROR_READ_MEMORY
        return -1
    }
    
    ErrorLevel := ERROR_OK
    return IsInMenu
}

getPlayerMapPosX() {
    if(!checkHandles())
        return -1
    
    MapPosX := readFloat(hGTA, 0xBA67B8)
    if(ErrorLevel) {
        ErrorLevel := ERROR_READ_MEMORY
        return -1
    }
    
    ErrorLevel := ERROR_OK
    return MapPosX
}

getPlayerMapPosY() {
    if(!checkHandles())
        return -1
    
    MapPosY := readFloat(hGTA, 0xBA67BC)
    if(ErrorLevel) {
        ErrorLevel := ERROR_READ_MEMORY
        return -1
    }
    
    ErrorLevel := ERROR_OK
    return MapPosY
}

getPlayerMapZoom() {
    if(!checkHandles())
        return -1
    
    MapZoom := readFloat(hGTA, 0xBA67AC)
    if(ErrorLevel) {
        ErrorLevel := ERROR_READ_MEMORY
        return -1
    }
    
    ErrorLevel := ERROR_OK
    return MapZoom
}

IsPlayerFreezed() {
    if(!checkHandles())
        return -1
    
    dwGTA := getModuleBaseAddress("gta_sa.exe", hGTA)
    IPF := readMem(hGTA, dwGTA + 0x690495, 2, "byte")    
    if(ErrorLevel) {
        ErrorLevel := ERROR_READ_MEMORY
        return -1
    }
    
    ErrorLevel := ERROR_OK    
    return IPF
}

; ##### Fahrzeugfunktionen #####

isPlayerInAnyVehicle()
{
    if(!checkHandles())
        return -1
    
    dwVehPtr := readDWORD(hGTA, ADDR_VEHICLE_PTR)
    if(ErrorLevel) {
        ErrorLevel := ERROR_READ_MEMORY
        return -1
    }
    
    return (dwVehPtr > 0)
}

isPlayerDriver() {
    if(!checkHandles())
        return -1
    
    dwAddr := readDWORD(hGTA, ADDR_VEHICLE_PTR)
    if(ErrorLevel) {
        ErrorLevel := ERROR_READ_MEMORY
        return -1
    }
    
    if(!dwAddr)
        return -1
    
    dwCPedPtr := readDWORD(hGTA, ADDR_CPED_PTR)
    if(ErrorLevel) {
        ErrorLevel := ERROR_READ_MEMORY
        return -1
    }
    
    dwVal := readDWORD(hGTA, dwAddr + ADDR_VEHICLE_DRIVER)
    if(ErrorLevel) {
        ErrorLevel := ERROR_READ_MEMORY
        return -1
    }
    
    ErrorLevel := ERROR_OK
    return (dwVal==dwCPedPtr)
}

getVehicleHealth() {
    if(!checkHandles())
        return -1
    
    dwVehPtr := readDWORD(hGTA, ADDR_VEHICLE_PTR)
    if(ErrorLevel) {
        ErrorLevel := ERROR_READ_MEMORY
        return -1
    }
    
    dwAddr := dwVehPtr + ADDR_VEHICLE_HPOFF
    fHealth := readFloat(hGTA, dwAddr)
    if(ErrorLevel) {
        ErrorLevel := ERROR_READ_MEMORY
        return -1
    }
    
    ErrorLevel := ERROR_OK
    return Round(fHealth)
}

getVehicleType() {
    if(!checkHandles())
        return 0
    
    dwAddr := readDWORD(hGTA, ADDR_VEHICLE_PTR)
    if(ErrorLevel) {
        ErrorLevel := ERROR_READ_MEMORY
        return 0
    }
    
    if(!dwAddr)
        return 0
    
    cVal := readMem(hGTA, dwAddr + ADDR_VEHICLE_TYPE, 1, "Char")
    if(ErrorLevel) {
        ErrorLevel := ERROR_READ_MEMORY
        return 0
    }
    
    ErrorLevel := ERROR_OK
    if(!cVal)
    {
        mid := getVehicleModelId()
        Loop % oAirplaneModels.MaxIndex()
        {
            if(oAirplaneModels[A_Index]==mid)
                return 5
        }
        return 1
    }
    else if(cVal==5)
        return 2
    else if(cVal==6)
        return 3
    else if(cVal==9)
    {
        mid := getVehicleModelId()
        Loop % oBikeModels.MaxIndex()
        {
            if(oBikeModels[A_Index]==mid)
                return 6
        }
        return 4
    }
    return 0
}

getVehicleModelId() {
    if(!checkHandles())
        return 0
    
    dwAddr := readDWORD(hGTA, ADDR_VEHICLE_PTR)
    if(ErrorLevel) {
        ErrorLevel := ERROR_READ_MEMORY
        return 0
    }
    
    if(!dwAddr)
        return 0
    
    sVal := readMem(hGTA, dwAddr + ADDR_VEHICLE_MODEL, 2, "Short")
    if(ErrorLevel) {
        ErrorLevel := ERROR_READ_MEMORY
        return 0
    }
    
    ErrorLevel := ERROR_OK
    return sVal
}

getVehicleModelName() {
    id:=getVehicleModelId()
    if(id > 400 && id < 611)
    {
        return ovehicleNames[id-399]
    }
    return ""
}

getVehicleLightState() {
    if(!checkHandles())
        return -1
    
    dwAddr := readDWORD(hGTA, ADDR_VEHICLE_PTR)
    if(ErrorLevel) {
        ErrorLevel := ERROR_READ_MEMORY
        return -1
    }
    
    if(!dwAddr)
        return -1
    
    dwVal := readMem(hGTA, dwAddr + ADDR_VEHICLE_LIGHTSTATE, 4, "Int")
    if(ErrorLevel) {
        ErrorLevel := ERROR_READ_MEMORY
        return -1
    }
    
    ErrorLevel := ERROR_OK
    return (dwVal>0)
}


getVehicleEngineState() {
    if(!checkHandles())
        return -1
    
    dwAddr := readDWORD(hGTA, ADDR_VEHICLE_PTR)
    if(ErrorLevel) {
        ErrorLevel := ERROR_READ_MEMORY
        return -1
    }
    
    if(!dwAddr)
        return -1
    
    cVal := readMem(hGTA, dwAddr + ADDR_VEHICLE_ENGINESTATE, 1, "Char")
    if(ErrorLevel) {
        ErrorLevel := ERROR_READ_MEMORY
        return -1
    }
    
    ErrorLevel := ERROR_OK
    return (cVal==24 || cVal==56 || cVal==88 || cVal==120)
}


getVehicleLockState() {
    if(!checkHandles())
        return -1
    
    dwAddr := readDWORD(hGTA, ADDR_VEHICLE_PTR)
    if(ErrorLevel) {
        ErrorLevel := ERROR_READ_MEMORY
        return -1
    }
    
    if(!dwAddr)
        return -1
    
    dwVal := readDWORD(hGTA, dwAddr + ADDR_VEHICLE_DOORSTATE)
    if(ErrorLevel) {
        ErrorLevel := ERROR_READ_MEMORY
        return -1
    }
    
    ErrorLevel := ERROR_OK
    return (dwVal==2)
}

getVehicleColor1() {
    if(!checkHandles())
        return 0
    
    dwAddr := readDWORD(hGTA, 0xBA18FC)
    if(ErrorLevel) {
        ErrorLevel := ERROR_READ_MEMORY
        return 0
    }
    
    if(!dwAddr)
        return 0
	
    sVal := readMem(hGTA, dwAddr + 1076, 1, "byte")
	
    if(ErrorLevel) {
        ErrorLevel := ERROR_READ_MEMORY
        return 0
    }
    
    ErrorLevel := ERROR_OK
    return sVal
}

getVehicleColor2() {
    if(!checkHandles())
        return 0
    
    dwAddr := readDWORD(hGTA, 0xBA18FC)
    if(ErrorLevel) {
        ErrorLevel := ERROR_READ_MEMORY
        return 0
    }
    
    if(!dwAddr)
        return 0
	
    sVal := readMem(hGTA, dwAddr + 1077, 1, "byte")
	
    if(ErrorLevel) {
        ErrorLevel := ERROR_READ_MEMORY
        return 0
    }
    
    ErrorLevel := ERROR_OK
    return sVal
}

getVehicleSpeed() {
    if(!checkHandles())
        return -1
 
    dwAddr := readDWORD(hGTA, ADDR_VEHICLE_PTR)
    if(ErrorLevel) {
        ErrorLevel := ERROR_READ_MEMORY
        return ""
    }
    
    fSpeedX := readMem(hGTA, dwAddr + ADDR_VEHICLE_X, 4, "float")
    fSpeedY := readMem(hGTA, dwAddr + ADDR_VEHICLE_Y, 4, "float")
    fSpeedZ := readMem(hGTA, dwAddr + ADDR_VEHICLE_Z, 4, "float")
    
    fVehicleSpeed :=  sqrt((fSpeedX * fSpeedX) + (fSpeedY * fSpeedY) + (fSpeedZ * fSpeedZ))
    fVehicleSpeed := (fVehicleSpeed * 100) * 1.43           ;Der Wert "1.43" ist meistens auf jedem Server anders. Die Geschwindigkeit wird somit erhÃ¶ht bzw. verringert
 
	return fVehicleSpeed
}

getPlayerRadiostationID() {
    if(!checkHandles())
        return -1
    
    if(isPlayerInAnyVehicle() == 0)
        return -1
    
    dwGTA := getModuleBaseAddress("gta_sa.exe", hGTA)
    RadioStationID := readMem(hGTA, dwGTA + 0x4CB7E1, 1, "byte")
    if(ErrorLevel) {
        ErrorLevel := ERROR_READ_MEMORY
        return -1
    }
    
    return RadioStationID
}

getPlayerRadiostationName() {
    if(isPlayerInAnyVehicle() == 0)
        return -1
    
    id := getPlayerRadiostationID()
    
    if(id == 0)
        return -1
    
    if(id >= 0 && id < 14)
    {
        return oradiostationNames[id]
    }
    return ""
}

; ##### Checkpointsachen #####
setCheckpoint(fX, fY, fZ, fSize ) {
    if(!checkHandles())
        return false
    dwFunc := dwSAMP + 0x9D340
    dwAddress := readDWORD(hGTA, dwSAMP + ADDR_SAMP_INCHAT_PTR) ;misc info
    if(ErrorLevel || dwAddress==0) {
        ErrorLevel := ERROR_READ_MEMORY
        return false
    }
    VarSetCapacity(buf, 16, 0)
    NumPut(fX, buf, 0, "Float")
    NumPut(fY, buf, 4, "Float")
    NumPut(fZ, buf, 8, "Float")
    NumPut(fSize, buf, 12, "Float")
    writeRaw(hGTA, pParam1, &buf, 16)
    dwLen := 31
    VarSetCapacity(injectData, dwLen, 0)
    NumPut(0xB9, injectData, 0, "UChar")
    NumPut(dwAddress, injectData, 1, "UInt")
    NumPut(0x68, injectData, 5, "UChar")
    NumPut(pParam1+12, injectData, 6, "UInt")
    NumPut(0x68, injectData, 10, "UChar")
    NumPut(pParam1, injectData, 11, "UInt")
    NumPut(0xE8, injectData, 15, "UChar")
    offset := dwFunc - (pInjectFunc + 20)
    NumPut(offset, injectData, 16, "Int")
    NumPut(0x05C7, injectData, 20, "UShort")
    NumPut(dwAddress+0x24, injectData, 22, "UInt")
    NumPut(1, injectData, 26, "UInt")
    NumPut(0xC3, injectData, 30, "UChar")
    writeRaw(hGTA, pInjectFunc, &injectData, dwLen)
    if(ErrorLevel)
        return false
    hThread := createRemoteThread(hGTA, 0, 0, pInjectFunc, 0, 0, 0)
    if(ErrorLevel)
        return false
    waitForSingleObject(hThread, 0xFFFFFFFF)
    closeProcess(hThread)
    ErrorLevel := ERROR_OK
    return true
}

disableCheckpoint()
{
    if(!checkHandles())
        return false
    dwAddress := readDWORD(hGTA, dwSAMP + ADDR_SAMP_INCHAT_PTR) ;misc info
    if(ErrorLevel || dwAddress==0) {
        ErrorLevel := ERROR_READ_MEMORY
        return false
    }
    VarSetCapacity(enablecp, 4, 0)
    NumPut(0,enablecp,0,"Int")
    writeRaw(hGTA, dwAddress+0x24, &enablecp, 4)
    ErrorLevel := ERROR_OK
    return true
}

IsMarkerCreated(){
    If(!checkHandles())
        return false
    active := readMem(hGTA, CheckpointCheck, 1, "byte")
    If(!active)
        return 0
    else return 1
}
CoordsFromRedmarker(){
    if(!checkhandles())
        return false
    for i, v in rmaddrs
    f%i% := readFloat(hGTA, v)
    return [f1, f2, f3]
}
; ##### Positionsbestimmung #####
getCoordinates() {
    if(!checkHandles())
        return ""
    
    fX := readFloat(hGTA, ADDR_POSITION_X)
    if(ErrorLevel) {
        ErrorLevel := ERROR_READ_MEMORY
        return ""
    }
    
    fY := readFloat(hGTA, ADDR_POSITION_Y)
    if(ErrorLevel) {
        ErrorLevel := ERROR_READ_MEMORY
        return ""
    }
    
    fZ := readFloat(hGTA, ADDR_POSITION_Z)
    if(ErrorLevel) {
        ErrorLevel := ERROR_READ_MEMORY
        return ""
    }
    
    ErrorLevel := ERROR_OK
    return [fX, fY, fZ]
}

GetPlayerPos(ByRef fX,ByRef fY,ByRef fZ) {
        if(!checkHandles())
                return 0
 
        fX := readFloat(hGTA, ADDR_POSITION_X)
        if(ErrorLevel) {
                ErrorLevel := ERROR_READ_MEMORY
                return 0
        }
 
        fY := readFloat(hGTA, ADDR_POSITION_Y)
        if(ErrorLevel) {
                ErrorLevel := ERROR_READ_MEMORY
                return 0
        }
 
        fZ := readFloat(hGTA, ADDR_POSITION_Z)
        if(ErrorLevel) {
                ErrorLevel := ERROR_READ_MEMORY
                return 0
        }
 
        ErrorLevel := ERROR_OK
}

; ######################### Dialog Functions #########################
getDialogStructPtr() {
	if (!checkHandles()) {
		ErrorLevel := ERROR_INVALID_HANDLE
		return false
	}

	dwPointer := readDWORD(hGTA, dwSAMP + SAMP_DIALOG_STRUCT_PTR)
	if (ErrorLevel) {
		ErrorLevel := ERROR_READ_MEMORY
		return false
	}

	ErrorLevel := ERROR_OK
	return dwPointer
}

isDialogOpen() {
	dwPointer := getDialogStructPtr()
	if (ErrorLevel || !dwPointer)
	return false

	dwIsOpen := readMem(hGTA, dwPointer + SAMP_DIALOG_OPEN_OFFSET, 4, "UInt")
	if (ErrorLevel) {
		ErrorLevel := ERROR_READ_MEMORY
		return false
	}
	
	ErrorLevel := ERROR_OK
	return dwIsOpen ? true : false
}

getDialogStyle() {
	dwPointer := getDialogStructPtr()
	if (ErrorLevel || !dwPointer)
		return -1

	style := readMem(hGTA, dwPointer + SAMP_DIALOG_STYLE_OFFSET, 4, "UInt")
	if (ErrorLevel) {
		ErrorLevel := ERROR_READ_MEMORY
		return -1
	}

	ErrorLevel := ERROR_OK
	return style
}

getDialogID() {
	dwPointer := getDialogStructPtr()
	if (ErrorLevel || !dwPointer)
		return -1

	id := readMem(hGTA, dwPointer + SAMP_DIALOG_ID_OFFSET, 4, "UInt")
	if (ErrorLevel) {
		ErrorLevel := ERROR_READ_MEMORY
		return -1
	}

	ErrorLevel := ERROR_OK
	return id
}

setDialogID(id) {
	dwPointer := getDialogStructPtr()
	if (ErrorLevel || !dwPointer)
		return false

	writeMemory(hGTA, dwPointer + SAMP_DIALOG_ID_OFFSET, id, "UInt", 4)
	if (ErrorLevel) {
		ErrorLevel := ERROR_WRITE_MEMORY
		return false
	}

	ErrorLevel := ERROR_OK
	return true
}

getDialogIndex() {
	dwPointer := getDialogStructPtr()
	if (ErrorLevel || !dwPointer)
		return 0

	dwPointer := readDWORD(hGTA, dwPointer + SAMP_DIALOG_PTR1_OFFSET)
	if (ErrorLevel) {
		ErrorLevel := ERROR_READ_MEMORY
		return 0
	}

	index := readMem(hGTA, dwPointer + SAMP_DIALOG_INDEX_OFFSET, 1, "Byte")
	if (ErrorLevel) {
		ErrorLevel := ERROR_READ_MEMORY
		return 0
	}

	ErrorLevel := ERROR_OK
	return index + 1
}

getDialogCaption() {
	dwPointer := getDialogStructPtr()
	if (ErrorLevel || !dwPointer)
		return ""

	text := readString(hGTA, dwPointer + SAMP_DIALOG_CAPTION_OFFSET, 64)
	if (ErrorLevel) {
		ErrorLevel := ERROR_READ_MEMORY
		return ""
	}

	ErrorLevel := ERROR_OK
	return text
}

getDialogTextSize(dwAddress) {
	i := 0
	Loop, 4096 {
		i := A_Index - 1
		byte := Memory_ReadByte(hGTA, dwAddress + i)
		if (!byte)
			break
	}

	return i
}

getDialogText() {
	dwPointer := getDialogStructPtr()
	if (ErrorLevel || !dwPointer)
		return ""

	dwPointer := readDWORD(hGTA, dwPointer + SAMP_DIALOG_TEXT_PTR_OFFSET)
	if (ErrorLevel) {
		ErrorLevel := ERROR_READ_MEMORY
		return ""
	}

	text := readString(hGTA, dwPointer, 4096)
	if (ErrorLevel) {
		text := readString(hGTA, dwPointer, getDialogTextSize(dwPointer))
		if (ErrorLevel) {
			ErrorLevel := ERROR_READ_MEMORY
			return ""
		}
	}

	ErrorLevel := ERROR_OK
	return text
}

getDialogLineCount() {
	dwPointer := getDialogStructPtr()
	if (ErrorLevel || !dwPointer)
		return 0

	dwPointer := readDWORD(hGTA, dwPointer + SAMP_DIALOG_PTR2_OFFSET)
	if (ErrorLevel) {
		ErrorLevel := ERROR_READ_MEMORY
		return 0
	}

	count := readMem(hGTA, dwPointer + SAMP_DIALOG_LINECOUNT_OFFSET, 4, "UInt")
	if (ErrorLevel) {
		ErrorLevel := ERROR_READ_MEMORY
		return 0
	}

	ErrorLevel := ERROR_OK
	return count
}

getDialogLine__(index) {
	if (getDialogLineCount > index)
		return ""

	dwPointer := getDialogStructPtr()
	if (ErrorLevel || !dwPointer)
		return ""

	dwPointer := readDWORD(hGTA, dwPointer + SAMP_DIALOG_PTR1_OFFSET)
	if (ErrorLevel) {
		ErrorLevel := ERROR_READ_MEMORY
		return ""
	}

	dwPointer := readDWORD(hGTA, dwPointer + SAMP_DIALOG_LINES_OFFSET)
	if (ErrorLevel) {
		ErrorLevel := ERROR_READ_MEMORY
		return ""
	}

	dwLineAddress := readDWORD(hGTA, dwPointer + (index - 1) * 0x4)
	line := readString(hGTA, dwLineAddress, 128)

	ErrorLevel := ERROR_OK
	return line
}

getDialogLine(index) {
	lines := getDialogLines()
	if (index > lines.Length())
		return ""

	if (getDialogStyle() == DIALOG_STYLE_TABLIST_HEADERS)
		index++

	return lines[index]
}

getDialogLines() {
	text := getDialogText()
	if (text == "")
		return -1

	lines := StrSplit(text, "`n")
	return lines
}

isDialogButton1Selected() {
	dwPointer := getDialogStructPtr()
	if (ErrorLevel || !dwPointer)
		return false

	dwPointer := readDWORD(hGTA, dwPointer + SAMP_DIALOG_PTR1_OFFSET)
	if (ErrorLevel) {
		ErrorLevel := ERROR_READ_MEMORY
		return false
	}

	selected := readMem(hGTA, dwPointer + SAMP_DIALOG_BUTTON_HOVERING_OFFSET, 1, "Byte")
	if (ErrorLevel) {
		ErrorLevel := ERROR_READ_MEMORY
		return false
	}

	ErrorLevel := ERROR_OK
	return selected
}

getDialogLines__() {
	count := getDialogLineCount()

	dwPointer := getDialogStructPtr()
	if (ErrorLevel || !dwPointer)
		return -1

	dwPointer := readDWORD(hGTA, dwPointer + SAMP_DIALOG_PTR1_OFFSET)
	if (ErrorLevel) {
		ErrorLevel := ERROR_READ_MEMORY
		return -1
	}

	dwPointer := readDWORD(hGTA, dwPointer + SAMP_DIALOG_LINES_OFFSET)
	if (ErrorLevel) {
		ErrorLevel := ERROR_READ_MEMORY
		return -1
	}

	lines := []
	Loop %count% {
		dwLineAddress := readDWORD(hGTA, dwPointer + (A_Index - 1) * 0x4)
		lines[A_Index] := readString(hGTA, dwLineAddress, 128)
	}

	ErrorLevel := ERROR_OK
	return lines
}

showDialog(style, caption, text, button1, button2 := "", id := 1) {
	style += 0
	style := Floor(style)
	id += 0
	id := Floor(id)
	caption := "" caption
	text := "" text
	button1 := "" button1
	button2 := "" button2

	if (id < 0 || id > 32767 || style < 0 || style > 5 || StrLen(caption) > 64 || StrLen(text) > 4096 || StrLen(button1) > 10 || StrLen(button2) > 10)
		return false

	if (!checkHandles())
		return false

	dwFunc := dwSAMP + FUNC_SAMP_SHOWDIALOG

	dwAddress := readDWORD(hGTA, dwSAMP + SAMP_DIALOG_STRUCT_PTR)
	if (ErrorLevel || !dwAddress) {
		ErrorLevel := ERROR_READ_MEMORY
		return false
	}

	writeString(hGTA, pParam5, caption)
	if (ErrorLevel)
		return false
	writeString(hGTA, pParam1, text)
	if (ErrorLevel)
		return false
	writeString(hGTA, pParam5 + 512, button1)
	if (ErrorLevel)
		return false
	writeString(hGTA, pParam5+StrLen(caption) + 1, button2)
	if (ErrorLevel)
		return false

	;mov + 7 * push + call + retn
	dwLen := 5 + 7 * 5 + 5 + 1
	VarSetCapacity(injectData, dwLen, 0)

	NumPut(0xB9, injectData, 0, "UChar")							;0 + 1	;mov ecx
	NumPut(dwAddress, injectData, 1, "UInt")						;1 + 4
	NumPut(0x68, injectData, 5, "UChar")							;5 + 1	;push send
	NumPut(1, injectData, 6, "UInt")								;6 + 4
	NumPut(0x68, injectData, 10, "UChar")							;10 + 1	;push button2
	NumPut(pParam5 + StrLen(caption) + 1, injectData, 11, "UInt")	;11 + 4
	NumPut(0x68, injectData, 15, "UChar")							;15 + 1	;push button1
	NumPut(pParam5 + 512, injectData, 16, "UInt")					;16 + 4
	NumPut(0x68, injectData, 20, "UChar")							;20 + 1	;push text
	NumPut(pParam1, injectData, 21, "UInt")							;21 + 4
	NumPut(0x68, injectData, 25, "UChar")							;25 + 1	;push caption
	NumPut(pParam5, injectData, 26, "UInt")							;26 + 4
	NumPut(0x68, injectData, 30, "UChar")							;30 + 1	;push style
	NumPut(style, injectData, 31, "UInt")							;31 + 4
	NumPut(0x68, injectData, 35, "UChar")							;35 + 1	;push id
	NumPut(id, injectData, 36, "UInt")								;36 + 4

	NumPut(0xE8, injectData, 40, "UChar")							;40 + 1 ;call
	offset := dwFunc - (pInjectFunc + 45)
	NumPut(offset, injectData, 41, "Int")							;41 + 4
	NumPut(0xC3, injectData, 45, "UChar")							;45 + 1	;retn

	writeRaw(hGTA, pInjectFunc, &injectData, dwLen)
	if (ErrorLevel)
		return false

	hThread := createRemoteThread(hGTA, 0, 0, pInjectFunc, 0, 0, 0)
	if (ErrorLevel)
		return false

	waitForSingleObject(hThread, 0xFFFFFFFF)
	closeProcess(hThread)

	return true
}


initZonesAndCities() {
    
AddCity("Las Venturas", 685.0, 476.093, -500.0, 3000.0, 3000.0, 500.0)
AddCity("San Fierro", -3000.0, -742.306, -500.0, -1270.53, 1530.24, 500.0)
AddCity("San Fierro", -1270.53, -402.481, -500.0, -1038.45, 832.495, 500.0)
AddCity("San Fierro", -1038.45, -145.539, -500.0, -897.546, 376.632, 500.0)
AddCity("Los Santos", 480.0, -3000.0, -500.0, 3000.0, -850.0, 500.0)
AddCity("Los Santos", 80.0, -2101.61, -500.0, 1075.0, -1239.61, 500.0)
AddCity("Tierra Robada", -1213.91, 596.349, -242.99, -480.539, 1659.68, 900.0)
AddCity("Red County", -1213.91, -768.027, -242.99, 2997.06, 596.349, 900.0)
AddCity("Flint County", -1213.91, -2892.97, -242.99, 44.6147, -768.027, 900.0)
AddCity("Whetstone", -2997.47, -2892.97, -242.99, -1213.91, -1115.58, 900.0)
    

AddZone("A-1", -3000.000000,2750.000000,-500,-2750.000000,3000.000000,500)
AddZone("A-2", -2750.000000,2750.000000,-500,-2500.000000,3000.000000,500)
AddZone("A-3", -2500.000000,2750.000000,-500,-2250.000000,3000.000000,500)
AddZone("A-4", -2250.000000,2750.000000,-500,-2000.000000,3000.000000,500)
AddZone("A-5", -2000.000000,2750.000000,-500,-1750.000000,3000.000000,500)
AddZone("A-6", -1750.000000,2750.000000,-500,-1500.000000,3000.000000,500)
AddZone("A-7", -1500.000000,2750.000000,-500,-1250.000000,3000.000000,500)
AddZone("A-8", -1250.000000,2750.000000,-500,-1000.000000,3000.000000,500)
AddZone("A-9", -1000.000000,2750.000000,-500,-750.000000,3000.000000,500)
AddZone("A-10", -750.000000,2750.000000,-500,-500.000000,3000.000000,500)
AddZone("A-11", -500.000000,2750.000000,-500,-250.000000,3000.000000,500)
AddZone("A-12", -250.000000,2750.000000,-500,0.000000,3000.000000,500)
AddZone("A-13", 0.000000,2750.000000,-500,250.000000,3000.000000,500)
AddZone("A-14", 250.000000,2750.000000,-500,500.000000,3000.000000,500)
AddZone("A-15", 500.000000,2750.000000,-500,750.000000,3000.000000,500)
AddZone("A-16", 750.000000,2750.000000,-500,1000.000000,3000.000000,500)
AddZone("A-17", 1000.000000,2750.000000,-500,1250.000000,3000.000000,500)
AddZone("A-18", 1250.000000,2750.000000,-500,1500.000000,3000.000000,500)
AddZone("A-19", 1500.000000,2750.000000,-500,1750.000000,3000.000000,500)
AddZone("A-20", 1750.000000,2750.000000,-500,2000.000000,3000.000000,500)
AddZone("A-21", 2000.000000,2750.000000,-500,2250.000000,3000.000000,500)
AddZone("A-22", 2250.000000,2750.000000,-500,2500.000000,3000.000000,500)
AddZone("A-23", 2500.000000,2750.000000,-500,2750.000000,3000.000000,500)
AddZone("A-24", 2750.000000,2750.000000,-500,3000.000000,3000.000000,500)
AddZone("�-1", -3000.000000,2500.000000,-500,-2750.000000,2750.000000,500)
AddZone("�-2", -2750.000000,2500.000000,-500,-2500.000000,2750.000000,500)
AddZone("�-3", -2500.000000,2500.000000,-500,-2250.000000,2750.000000,500)
AddZone("�-4", -2250.000000,2500.000000,-500,-2000.000000,2750.000000,500)
AddZone("�-5", -2000.000000,2500.000000,-500,-1750.000000,2750.000000,500)
AddZone("�-6", -1750.000000,2500.000000,-500,-1500.000000,2750.000000,500)
AddZone("�-7", -1500.000000,2500.000000,-500,-1250.000000,2750.000000,500)
AddZone("�-8", -1250.000000,2500.000000,-500,-1000.000000,2750.000000,500)
AddZone("�-9", -1000.000000,2500.000000,-500,-750.000000,2750.000000,500)
AddZone("�-10", -750.000000,2500.000000,-500,-500.000000,2750.000000,500)
AddZone("�-11", -500.000000,2500.000000,-500,-250.000000,2750.000000,500)
AddZone("�-12", -250.000000,2500.000000,-500,0.000000,2750.000000,500)
AddZone("�-13", 0.000000,2500.000000,-500,250.000000,2750.000000,500)
AddZone("�-14", 250.000000,2500.000000,-500,500.000000,2750.000000,500)
AddZone("�-15", 500.000000,2500.000000,-500,750.000000,2750.000000,500)
AddZone("�-16", 750.000000,2500.000000,-500,1000.000000,2750.000000,500)
AddZone("�-17", 1000.000000,2500.000000,-500,1250.000000,2750.000000,500)
AddZone("�-18", 1250.000000,2500.000000,-500,1500.000000,2750.000000,500)
AddZone("�-19", 1500.000000,2500.000000,-500,1750.000000,2750.000000,500)
AddZone("�-20", 1750.000000,2500.000000,-500,2000.000000,2750.000000,500)
AddZone("�-21", 2000.000000,2500.000000,-500,2250.000000,2750.000000,500)
AddZone("�-22", 2250.000000,2500.000000,-500,2500.000000,2750.000000,500)
AddZone("�-23", 2500.000000,2500.000000,-500,2750.000000,2750.000000,500)
AddZone("�-24", 2750.000000,2500.000000,-500,3000.000000,2750.000000,500)
AddZone("�-1", -3000.000000,2250.000000,-500,-2750.000000,2500.000000,500)
AddZone("�-2", -2750.000000,2250.000000,-500,-2500.000000,2500.000000,500)
AddZone("�-3", -2500.000000,2250.000000,-500,-2250.000000,2500.000000,500)
AddZone("�-4", -2250.000000,2250.000000,-500,-2000.000000,2500.000000,500)
AddZone("�-5", -2000.000000,2250.000000,-500,-1750.000000,2500.000000,500)
AddZone("�-6", -1750.000000,2250.000000,-500,-1500.000000,2500.000000,500)
AddZone("�-7", -1500.000000,2250.000000,-500,-1250.000000,2500.000000,500)
AddZone("�-8", -1250.000000,2250.000000,-500,-1000.000000,2500.000000,500)
AddZone("�-9", -1000.000000,2250.000000,-500,-750.000000,2500.000000,500)
AddZone("�-10", -750.000000,2250.000000,-500,-500.000000,2500.000000,500)
AddZone("�-11", -500.000000,2250.000000,-500,-250.000000,2500.000000,500)
AddZone("�-12", -250.000000,2250.000000,-500,0.000000,2500.000000,500)
AddZone("�-13", 0.000000,2250.000000,-500,250.000000,2500.000000,500)
AddZone("�-14", 250.000000,2250.000000,-500,500.000000,2500.000000,500)
AddZone("�-15", 500.000000,2250.000000,-500,750.000000,2500.000000,500)
AddZone("�-16", 750.000000,2250.000000,-500,1000.000000,2500.000000,500)
AddZone("�-17", 1000.000000,2250.000000,-500,1250.000000,2500.000000,500)
AddZone("�-18", 1250.000000,2250.000000,-500,1500.000000,2500.000000,500)
AddZone("�-19", 1500.000000,2250.000000,-500,1750.000000,2500.000000,500)
AddZone("�-20", 1750.000000,2250.000000,-500,2000.000000,2500.000000,500)
AddZone("�-21", 2000.000000,2250.000000,-500,2250.000000,2500.000000,500)
AddZone("�-22", 2250.000000,2250.000000,-500,2500.000000,2500.000000,500)
AddZone("�-23", 2500.000000,2250.000000,-500,2750.000000,2500.000000,500)
AddZone("�-24", 2750.000000,2250.000000,-500,3000.000000,2500.000000,500)
AddZone("�-1", -3000.000000,2000.000000,-500,-2750.000000,2250.000000,500)
AddZone("�-2", -2750.000000,2000.000000,-500,-2500.000000,2250.000000,500)
AddZone("�-3", -2500.000000,2000.000000,-500,-2250.000000,2250.000000,500)
AddZone("�-4", -2250.000000,2000.000000,-500,-2000.000000,2250.000000,500)
AddZone("�-5", -2000.000000,2000.000000,-500,-1750.000000,2250.000000,500)
AddZone("�-6", -1750.000000,2000.000000,-500,-1500.000000,2250.000000,500)
AddZone("�-7", -1500.000000,2000.000000,-500,-1250.000000,2250.000000,500)
AddZone("�-8", -1250.000000,2000.000000,-500,-1000.000000,2250.000000,500)
AddZone("�-9", -1000.000000,2000.000000,-500,-750.000000,2250.000000,500)
AddZone("�-10", -750.000000,2000.000000,-500,-500.000000,2250.000000,500)
AddZone("�-11", -500.000000,2000.000000,-500,-250.000000,2250.000000,500)
AddZone("�-12", -250.000000,2000.000000,-500,0.000000,2250.000000,500)
AddZone("�-13", 0.000000,2000.000000,-500,250.000000,2250.000000,500)
AddZone("�-14", 250.000000,2000.000000,-500,500.000000,2250.000000,500)
AddZone("�-15", 500.000000,2000.000000,-500,750.000000,2250.000000,500)
AddZone("�-16", 750.000000,2000.000000,-500,1000.000000,2250.000000,500)
AddZone("�-17", 1000.000000,2000.000000,-500,1250.000000,2250.000000,500)
AddZone("�-18", 1250.000000,2000.000000,-500,1500.000000,2250.000000,500)
AddZone("�-19", 1500.000000,2000.000000,-500,1750.000000,2250.000000,500)
AddZone("�-20", 1750.000000,2000.000000,-500,2000.000000,2250.000000,500)
AddZone("�-21", 2000.000000,2000.000000,-500,2250.000000,2250.000000,500)
AddZone("�-22", 2250.000000,2000.000000,-500,2500.000000,2250.000000,500)
AddZone("�-23", 2500.000000,2000.000000,-500,2750.000000,2250.000000,500)
AddZone("�-24", 2750.000000,2000.000000,-500,3000.000000,2250.000000,500)
AddZone("�-1", -3000.000000,1750.000000,-500,-2750.000000,2000.000000,500)
AddZone("�-2", -2750.000000,1750.000000,-500,-2500.000000,2000.000000,500)
AddZone("�-3", -2500.000000,1750.000000,-500,-2250.000000,2000.000000,500)
AddZone("�-4", -2250.000000,1750.000000,-500,-2000.000000,2000.000000,500)
AddZone("�-5", -2000.000000,1750.000000,-500,-1750.000000,2000.000000,500)
AddZone("�-6", -1750.000000,1750.000000,-500,-1500.000000,2000.000000,500)
AddZone("�-7", -1500.000000,1750.000000,-500,-1250.000000,2000.000000,500)
AddZone("�-8", -1250.000000,1750.000000,-500,-1000.000000,2000.000000,500)
AddZone("�-9", -1000.000000,1750.000000,-500,-750.000000,2000.000000,500)
AddZone("�-10", -750.000000,1750.000000,-500,-500.000000,2000.000000,500)
AddZone("�-11", -500.000000,1750.000000,-500,-250.000000,2000.000000,500)
AddZone("�-12", -250.000000,1750.000000,-500,0.000000,2000.000000,500)
AddZone("�-13", 0.000000,1750.000000,-500,250.000000,2000.000000,500)
AddZone("�-14", 250.000000,1750.000000,-500,500.000000,2000.000000,500)
AddZone("�-15", 500.000000,1750.000000,-500,750.000000,2000.000000,500)
AddZone("�-16", 750.000000,1750.000000,-500,1000.000000,2000.000000,500)
AddZone("�-17", 1000.000000,1750.000000,-500,1250.000000,2000.000000,500)
AddZone("�-18", 1250.000000,1750.000000,-500,1500.000000,2000.000000,500)
AddZone("�-19", 1500.000000,1750.000000,-500,1750.000000,2000.000000,500)
AddZone("�-20", 1750.000000,1750.000000,-500,2000.000000,2000.000000,500)
AddZone("�-21", 2000.000000,1750.000000,-500,2250.000000,2000.000000,500)
AddZone("�-22", 2250.000000,1750.000000,-500,2500.000000,2000.000000,500)
AddZone("�-23", 2500.000000,1750.000000,-500,2750.000000,2000.000000,500)
AddZone("�-24", 2750.000000,1750.000000,-500,3000.000000,2000.000000,500)
AddZone("�-1", -3000.000000,1500.000000,-500,-2750.000000,1750.000000,500)
AddZone("�-2", -2750.000000,1500.000000,-500,-2500.000000,1750.000000,500)
AddZone("�-3", -2500.000000,1500.000000,-500,-2250.000000,1750.000000,500)
AddZone("�-4", -2250.000000,1500.000000,-500,-2000.000000,1750.000000,500)
AddZone("�-5", -2000.000000,1500.000000,-500,-1750.000000,1750.000000,500)
AddZone("�-6", -1750.000000,1500.000000,-500,-1500.000000,1750.000000,500)
AddZone("�-7", -1500.000000,1500.000000,-500,-1250.000000,1750.000000,500)
AddZone("�-8", -1250.000000,1500.000000,-500,-1000.000000,1750.000000,500)
AddZone("�-9", -1000.000000,1500.000000,-500,-750.000000,1750.000000,500)
AddZone("�-10", -750.000000,1500.000000,-500,-500.000000,1750.000000,500)
AddZone("�-11", -500.000000,1500.000000,-500,-250.000000,1750.000000,500)
AddZone("�-12", -250.000000,1500.000000,-500,0.000000,1750.000000,500)
AddZone("�-13", 0.000000,1500.000000,-500,250.000000,1750.000000,500)
AddZone("�-14", 250.000000,1500.000000,-500,500.000000,1750.000000,500)
AddZone("�-15", 500.000000,1500.000000,-500,750.000000,1750.000000,500)
AddZone("�-16", 750.000000,1500.000000,-500,1000.000000,1750.000000,500)
AddZone("�-17", 1000.000000,1500.000000,-500,1250.000000,1750.000000,500)
AddZone("�-18", 1250.000000,1500.000000,-500,1500.000000,1750.000000,500)
AddZone("�-19", 1500.000000,1500.000000,-500,1750.000000,1750.000000,500)
AddZone("�-20", 1750.000000,1500.000000,-500,2000.000000,1750.000000,500)
AddZone("�-21", 2000.000000,1500.000000,-500,2250.000000,1750.000000,500)
AddZone("�-22", 2250.000000,1500.000000,-500,2500.000000,1750.000000,500)
AddZone("�-23", 2500.000000,1500.000000,-500,2750.000000,1750.000000,500)
AddZone("�-24", 2750.000000,1500.000000,-500,3000.000000,1750.000000,500)
AddZone("�-1", -3000.000000,1250.000000,-500,-2750.000000,1500.000000,500)
AddZone("�-2", -2750.000000,1250.000000,-500,-2500.000000,1500.000000,500)
AddZone("�-3", -2500.000000,1250.000000,-500,-2250.000000,1500.000000,500)
AddZone("�-4", -2250.000000,1250.000000,-500,-2000.000000,1500.000000,500)
AddZone("�-5", -2000.000000,1250.000000,-500,-1750.000000,1500.000000,500)
AddZone("�-6", -1750.000000,1250.000000,-500,-1500.000000,1500.000000,500)
AddZone("�-7", -1500.000000,1250.000000,-500,-1250.000000,1500.000000,500)
AddZone("�-8", -1250.000000,1250.000000,-500,-1000.000000,1500.000000,500)
AddZone("�-9", -1000.000000,1250.000000,-500,-750.000000,1500.000000,500)
AddZone("�-10", -750.000000,1250.000000,-500,-500.000000,1500.000000,500)
AddZone("�-11", -500.000000,1250.000000,-500,-250.000000,1500.000000,500)
AddZone("�-12", -250.000000,1250.000000,-500,0.000000,1500.000000,500)
AddZone("�-13", 0.000000,1250.000000,-500,250.000000,1500.000000,500)
AddZone("�-14", 250.000000,1250.000000,-500,500.000000,1500.000000,500)
AddZone("�-15", 500.000000,1250.000000,-500,750.000000,1500.000000,500)
AddZone("�-16", 750.000000,1250.000000,-500,1000.000000,1500.000000,500)
AddZone("�-17", 1000.000000,1250.000000,-500,1250.000000,1500.000000,500)
AddZone("�-18", 1250.000000,1250.000000,-500,1500.000000,1500.000000,500)
AddZone("�-19", 1500.000000,1250.000000,-500,1750.000000,1500.000000,500)
AddZone("�-20", 1750.000000,1250.000000,-500,2000.000000,1500.000000,500)
AddZone("�-21", 2000.000000,1250.000000,-500,2250.000000,1500.000000,500)
AddZone("�-22", 2250.000000,1250.000000,-500,2500.000000,1500.000000,500)
AddZone("�-23", 2500.000000,1250.000000,-500,2750.000000,1500.000000,500)
AddZone("�-24", 2750.000000,1250.000000,-500,3000.000000,1500.000000,500)
AddZone("�-1", -3000.000000,1000.000000,-500,-2750.000000,1250.000000,500)
AddZone("�-2", -2750.000000,1000.000000,-500,-2500.000000,1250.000000,500)
AddZone("�-3", -2500.000000,1000.000000,-500,-2250.000000,1250.000000,500)
AddZone("�-4", -2250.000000,1000.000000,-500,-2000.000000,1250.000000,500)
AddZone("�-5", -2000.000000,1000.000000,-500,-1750.000000,1250.000000,500)
AddZone("�-6", -1750.000000,1000.000000,-500,-1500.000000,1250.000000,500)
AddZone("�-7", -1500.000000,1000.000000,-500,-1250.000000,1250.000000,500)
AddZone("�-8", -1250.000000,1000.000000,-500,-1000.000000,1250.000000,500)
AddZone("�-9", -1000.000000,1000.000000,-500,-750.000000,1250.000000,500)
AddZone("�-10", -750.000000,1000.000000,-500,-500.000000,1250.000000,500)
AddZone("�-11", -500.000000,1000.000000,-500,-250.000000,1250.000000,500)
AddZone("�-12", -250.000000,1000.000000,-500,0.000000,1250.000000,500)
AddZone("�-13", 0.000000,1000.000000,-500,250.000000,1250.000000,500)
AddZone("�-14", 250.000000,1000.000000,-500,500.000000,1250.000000,500)
AddZone("�-15", 500.000000,1000.000000,-500,750.000000,1250.000000,500)
AddZone("�-16", 750.000000,1000.000000,-500,1000.000000,1250.000000,500)
AddZone("�-17", 1000.000000,1000.000000,-500,1250.000000,1250.000000,500)
AddZone("�-18", 1250.000000,1000.000000,-500,1500.000000,1250.000000,500)
AddZone("�-19", 1500.000000,1000.000000,-500,1750.000000,1250.000000,500)
AddZone("�-20", 1750.000000,1000.000000,-500,2000.000000,1250.000000,500)
AddZone("�-21", 2000.000000,1000.000000,-500,2250.000000,1250.000000,500)
AddZone("�-22", 2250.000000,1000.000000,-500,2500.000000,1250.000000,500)
AddZone("�-23", 2500.000000,1000.000000,-500,2750.000000,1250.000000,500)
AddZone("�-24", 2750.000000,1000.000000,-500,3000.000000,1250.000000,500)
AddZone("�-1", -3000.000000,750.000000,-500,-2750.000000,1000.000000,500)
AddZone("�-2", -2750.000000,750.000000,-500,-2500.000000,1000.000000,500)
AddZone("�-3", -2500.000000,750.000000,-500,-2250.000000,1000.000000,500)
AddZone("�-4", -2250.000000,750.000000,-500,-2000.000000,1000.000000,500)
AddZone("�-5", -2000.000000,750.000000,-500,-1750.000000,1000.000000,500)
AddZone("�-6", -1750.000000,750.000000,-500,-1500.000000,1000.000000,500)
AddZone("�-7", -1500.000000,750.000000,-500,-1250.000000,1000.000000,500)
AddZone("�-8", -1250.000000,750.000000,-500,-1000.000000,1000.000000,500)
AddZone("�-9", -1000.000000,750.000000,-500,-750.000000,1000.000000,500)
AddZone("�-10", -750.000000,750.000000,-500,-500.000000,1000.000000,500)
AddZone("�-11", -500.000000,750.000000,-500,-250.000000,1000.000000,500)
AddZone("�-12", -250.000000,750.000000,-500,0.000000,1000.000000,500)
AddZone("�-13", 0.000000,750.000000,-500,250.000000,1000.000000,500)
AddZone("�-14", 250.000000,750.000000,-500,500.000000,1000.000000,500)
AddZone("�-15", 500.000000,750.000000,-500,750.000000,1000.000000,500)
AddZone("�-16", 750.000000,750.000000,-500,1000.000000,1000.000000,500)
AddZone("�-17", 1000.000000,750.000000,-500,1250.000000,1000.000000,500)
AddZone("�-18", 1250.000000,750.000000,-500,1500.000000,1000.000000,500)
AddZone("�-19", 1500.000000,750.000000,-500,1750.000000,1000.000000,500)
AddZone("�-20", 1750.000000,750.000000,-500,2000.000000,1000.000000,500)
AddZone("�-21", 2000.000000,750.000000,-500,2250.000000,1000.000000,500)
AddZone("�-22", 2250.000000,750.000000,-500,2500.000000,1000.000000,500)
AddZone("�-23", 2500.000000,750.000000,-500,2750.000000,1000.000000,500)
AddZone("�-24", 2750.000000,750.000000,-500,3000.000000,1000.000000,500)
AddZone("�-1", -3000.000000,500.000000,-500,-2750.000000,750.000000,500)
AddZone("�-2", -2750.000000,500.000000,-500,-2500.000000,750.000000,500)
AddZone("�-3", -2500.000000,500.000000,-500,-2250.000000,750.000000,500)
AddZone("�-4", -2250.000000,500.000000,-500,-2000.000000,750.000000,500)
AddZone("�-5", -2000.000000,500.000000,-500,-1750.000000,750.000000,500)
AddZone("�-6", -1750.000000,500.000000,-500,-1500.000000,750.000000,500)
AddZone("�-7", -1500.000000,500.000000,-500,-1250.000000,750.000000,500)
AddZone("�-8", -1250.000000,500.000000,-500,-1000.000000,750.000000,500)
AddZone("�-9", -1000.000000,500.000000,-500,-750.000000,750.000000,500)
AddZone("�-10", -750.000000,500.000000,-500,-500.000000,750.000000,500)
AddZone("�-11", -500.000000,500.000000,-500,-250.000000,750.000000,500)
AddZone("�-12", -250.000000,500.000000,-500,0.000000,750.000000,500)
AddZone("�-13", 0.000000,500.000000,-500,250.000000,750.000000,500)
AddZone("�-14", 250.000000,500.000000,-500,500.000000,750.000000,500)
AddZone("�-15", 500.000000,500.000000,-500,750.000000,750.000000,500)
AddZone("�-16", 750.000000,500.000000,-500,1000.000000,750.000000,500)
AddZone("�-17", 1000.000000,500.000000,-500,1250.000000,750.000000,500)
AddZone("�-18", 1250.000000,500.000000,-500,1500.000000,750.000000,500)
AddZone("�-19", 1500.000000,500.000000,-500,1750.000000,750.000000,500)
AddZone("�-20", 1750.000000,500.000000,-500,2000.000000,750.000000,500)
AddZone("�-21", 2000.000000,500.000000,-500,2250.000000,750.000000,500)
AddZone("�-22", 2250.000000,500.000000,-500,2500.000000,750.000000,500)
AddZone("�-23", 2500.000000,500.000000,-500,2750.000000,750.000000,500)
AddZone("�-24", 2750.000000,500.000000,-500,3000.000000,750.000000,500)
AddZone("�-1", -3000.000000,250.000000,-500,-2750.000000,500.000000,500)
AddZone("�-2", -2750.000000,250.000000,-500,-2500.000000,500.000000,500)
AddZone("�-3", -2500.000000,250.000000,-500,-2250.000000,500.000000,500)
AddZone("�-4", -2250.000000,250.000000,-500,-2000.000000,500.000000,500)
AddZone("�-5", -2000.000000,250.000000,-500,-1750.000000,500.000000,500)
AddZone("�-6", -1750.000000,250.000000,-500,-1500.000000,500.000000,500)
AddZone("�-7", -1500.000000,250.000000,-500,-1250.000000,500.000000,500)
AddZone("�-8", -1250.000000,250.000000,-500,-1000.000000,500.000000,500)
AddZone("�-9", -1000.000000,250.000000,-500,-750.000000,500.000000,500)
AddZone("�-10", -750.000000,250.000000,-500,-500.000000,500.000000,500)
AddZone("�-11", -500.000000,250.000000,-500,-250.000000,500.000000,500)
AddZone("�-12", -250.000000,250.000000,-500,0.000000,500.000000,500)
AddZone("�-13", 0.000000,250.000000,-500,250.000000,500.000000,500)
AddZone("�-14", 250.000000,250.000000,-500,500.000000,500.000000,500)
AddZone("�-15", 500.000000,250.000000,-500,750.000000,500.000000,500)
AddZone("�-16", 750.000000,250.000000,-500,1000.000000,500.000000,500)
AddZone("�-17", 1000.000000,250.000000,-500,1250.000000,500.000000,500)
AddZone("�-18", 1250.000000,250.000000,-500,1500.000000,500.000000,500)
AddZone("�-19", 1500.000000,250.000000,-500,1750.000000,500.000000,500)
AddZone("�-20", 1750.000000,250.000000,-500,2000.000000,500.000000,500)
AddZone("�-21", 2000.000000,250.000000,-500,2250.000000,500.000000,500)
AddZone("�-22", 2250.000000,250.000000,-500,2500.000000,500.000000,500)
AddZone("�-23", 2500.000000,250.000000,-500,2750.000000,500.000000,500)
AddZone("�-24", 2750.000000,250.000000,-500,3000.000000,500.000000,500)
AddZone("�-1", -3000.000000,0.000000,-500,-2750.000000,250.000000,500)
AddZone("�-2", -2750.000000,0.000000,-500,-2500.000000,250.000000,500)
AddZone("�-3", -2500.000000,0.000000,-500,-2250.000000,250.000000,500)
AddZone("�-4", -2250.000000,0.000000,-500,-2000.000000,250.000000,500)
AddZone("�-5", -2000.000000,0.000000,-500,-1750.000000,250.000000,500)
AddZone("�-6", -1750.000000,0.000000,-500,-1500.000000,250.000000,500)
AddZone("�-7", -1500.000000,0.000000,-500,-1250.000000,250.000000,500)
AddZone("�-8", -1250.000000,0.000000,-500,-1000.000000,250.000000,500)
AddZone("�-9", -1000.000000,0.000000,-500,-750.000000,250.000000,500)
AddZone("�-10", -750.000000,0.000000,-500,-500.000000,250.000000,500)
AddZone("�-11", -500.000000,0.000000,-500,-250.000000,250.000000,500)
AddZone("�-12", -250.000000,0.000000,-500,0.000000,250.000000,500)
AddZone("�-13", 0.000000,0.000000,-500,250.000000,250.000000,500)
AddZone("�-14", 250.000000,0.000000,-500,500.000000,250.000000,500)
AddZone("�-15", 500.000000,0.000000,-500,750.000000,250.000000,500)
AddZone("�-16", 750.000000,0.000000,-500,1000.000000,250.000000,500)
AddZone("�-17", 1000.000000,0.000000,-500,1250.000000,250.000000,500)
AddZone("�-18", 1250.000000,0.000000,-500,1500.000000,250.000000,500)
AddZone("�-19", 1500.000000,0.000000,-500,1750.000000,250.000000,500)
AddZone("�-20", 1750.000000,0.000000,-500,2000.000000,250.000000,500)
AddZone("�-21", 2000.000000,0.000000,-500,2250.000000,250.000000,500)
AddZone("�-22", 2250.000000,0.000000,-500,2500.000000,250.000000,500)
AddZone("�-23", 2500.000000,0.000000,-500,2750.000000,250.000000,500)
AddZone("�-24", 2750.000000,0.000000,-500,3000.000000,250.000000,500)
AddZone("�-1", -3000.000000,-250.000000,-500,-2750.000000,0.000000,500)
AddZone("�-2", -2750.000000,-250.000000,-500,-2500.000000,0.000000,500)
AddZone("�-3", -2500.000000,-250.000000,-500,-2250.000000,0.000000,500)
AddZone("�-4", -2250.000000,-250.000000,-500,-2000.000000,0.000000,500)
AddZone("�-5", -2000.000000,-250.000000,-500,-1750.000000,0.000000,500)
AddZone("�-6", -1750.000000,-250.000000,-500,-1500.000000,0.000000,500)
AddZone("�-7", -1500.000000,-250.000000,-500,-1250.000000,0.000000,500)
AddZone("�-8", -1250.000000,-250.000000,-500,-1000.000000,0.000000,500)
AddZone("�-9", -1000.000000,-250.000000,-500,-750.000000,0.000000,500)
AddZone("�-10", -750.000000,-250.000000,-500,-500.000000,0.000000,500)
AddZone("�-11", -500.000000,-250.000000,-500,-250.000000,0.000000,500)
AddZone("�-12", -250.000000,-250.000000,-500,0.000000,0.000000,500)
AddZone("�-13", 0.000000,-250.000000,-500,250.000000,0.000000,500)
AddZone("�-14", 250.000000,-250.000000,-500,500.000000,0.000000,500)
AddZone("�-15", 500.000000,-250.000000,-500,750.000000,0.000000,500)
AddZone("�-16", 750.000000,-250.000000,-500,1000.000000,0.000000,500)
AddZone("�-17", 1000.000000,-250.000000,-500,1250.000000,0.000000,500)
AddZone("�-18", 1250.000000,-250.000000,-500,1500.000000,0.000000,500)
AddZone("�-19", 1500.000000,-250.000000,-500,1750.000000,0.000000,500)
AddZone("�-20", 1750.000000,-250.000000,-500,2000.000000,0.000000,500)
AddZone("�-21", 2000.000000,-250.000000,-500,2250.000000,0.000000,500)
AddZone("�-22", 2250.000000,-250.000000,-500,2500.000000,0.000000,500)
AddZone("�-23", 2500.000000,-250.000000,-500,2750.000000,0.000000,500)
AddZone("�-24", 2750.000000,-250.000000,-500,3000.000000,0.000000,500)
AddZone("�-1", -3000.000000,-500.000000,-500,-2750.000000,-250.000000,500)
AddZone("�-2", -2750.000000,-500.000000,-500,-2500.000000,-250.000000,500)
AddZone("�-3", -2500.000000,-500.000000,-500,-2250.000000,-250.000000,500)
AddZone("�-4", -2250.000000,-500.000000,-500,-2000.000000,-250.000000,500)
AddZone("�-5", -2000.000000,-500.000000,-500,-1750.000000,-250.000000,500)
AddZone("�-6", -1750.000000,-500.000000,-500,-1500.000000,-250.000000,500)
AddZone("�-7", -1500.000000,-500.000000,-500,-1250.000000,-250.000000,500)
AddZone("�-8", -1250.000000,-500.000000,-500,-1000.000000,-250.000000,500)
AddZone("�-9", -1000.000000,-500.000000,-500,-750.000000,-250.000000,500)
AddZone("�-10", -750.000000,-500.000000,-500,-500.000000,-250.000000,500)
AddZone("�-11", -500.000000,-500.000000,-500,-250.000000,-250.000000,500)
AddZone("�-12", -250.000000,-500.000000,-500,0.000000,-250.000000,500)
AddZone("�-13", 0.000000,-500.000000,-500,250.000000,-250.000000,500)
AddZone("�-14", 250.000000,-500.000000,-500,500.000000,-250.000000,500)
AddZone("�-15", 500.000000,-500.000000,-500,750.000000,-250.000000,500)
AddZone("�-16", 750.000000,-500.000000,-500,1000.000000,-250.000000,500)
AddZone("�-17", 1000.000000,-500.000000,-500,1250.000000,-250.000000,500)
AddZone("�-18", 1250.000000,-500.000000,-500,1500.000000,-250.000000,500)
AddZone("�-19", 1500.000000,-500.000000,-500,1750.000000,-250.000000,500)
AddZone("�-20", 1750.000000,-500.000000,-500,2000.000000,-250.000000,500)
AddZone("�-21", 2000.000000,-500.000000,-500,2250.000000,-250.000000,500)
AddZone("�-22", 2250.000000,-500.000000,-500,2500.000000,-250.000000,500)
AddZone("�-23", 2500.000000,-500.000000,-500,2750.000000,-250.000000,500)
AddZone("�-24", 2750.000000,-500.000000,-500,3000.000000,-250.000000,500)
AddZone("�-1", -3000.000000,-750.000000,-500,-2750.000000,-500.000000,500)
AddZone("�-2", -2750.000000,-750.000000,-500,-2500.000000,-500.000000,500)
AddZone("�-3", -2500.000000,-750.000000,-500,-2250.000000,-500.000000,500)
AddZone("�-4", -2250.000000,-750.000000,-500,-2000.000000,-500.000000,500)
AddZone("�-5", -2000.000000,-750.000000,-500,-1750.000000,-500.000000,500)
AddZone("�-6", -1750.000000,-750.000000,-500,-1500.000000,-500.000000,500)
AddZone("�-7", -1500.000000,-750.000000,-500,-1250.000000,-500.000000,500)
AddZone("�-8", -1250.000000,-750.000000,-500,-1000.000000,-500.000000,500)
AddZone("�-9", -1000.000000,-750.000000,-500,-750.000000,-500.000000,500)
AddZone("�-10", -750.000000,-750.000000,-500,-500.000000,-500.000000,500)
AddZone("�-11", -500.000000,-750.000000,-500,-250.000000,-500.000000,500)
AddZone("�-12", -250.000000,-750.000000,-500,0.000000,-500.000000,500)
AddZone("�-13", 0.000000,-750.000000,-500,250.000000,-500.000000,500)
AddZone("�-14", 250.000000,-750.000000,-500,500.000000,-500.000000,500)
AddZone("�-15", 500.000000,-750.000000,-500,750.000000,-500.000000,500)
AddZone("�-16", 750.000000,-750.000000,-500,1000.000000,-500.000000,500)
AddZone("�-17", 1000.000000,-750.000000,-500,1250.000000,-500.000000,500)
AddZone("�-18", 1250.000000,-750.000000,-500,1500.000000,-500.000000,500)
AddZone("�-19", 1500.000000,-750.000000,-500,1750.000000,-500.000000,500)
AddZone("�-20", 1750.000000,-750.000000,-500,2000.000000,-500.000000,500)
AddZone("�-21", 2000.000000,-750.000000,-500,2250.000000,-500.000000,500)
AddZone("�-22", 2250.000000,-750.000000,-500,2500.000000,-500.000000,500)
AddZone("�-23", 2500.000000,-750.000000,-500,2750.000000,-500.000000,500)
AddZone("�-24", 2750.000000,-750.000000,-500,3000.000000,-500.000000,500)
AddZone("�-1", -3000.000000,-1000.000000,-500,-2750.000000,-750.000000,500)
AddZone("�-2", -2750.000000,-1000.000000,-500,-2500.000000,-750.000000,500)
AddZone("�-3", -2500.000000,-1000.000000,-500,-2250.000000,-750.000000,500)
AddZone("�-4", -2250.000000,-1000.000000,-500,-2000.000000,-750.000000,500)
AddZone("�-5", -2000.000000,-1000.000000,-500,-1750.000000,-750.000000,500)
AddZone("�-6", -1750.000000,-1000.000000,-500,-1500.000000,-750.000000,500)
AddZone("�-7", -1500.000000,-1000.000000,-500,-1250.000000,-750.000000,500)
AddZone("�-8", -1250.000000,-1000.000000,-500,-1000.000000,-750.000000,500)
AddZone("�-9", -1000.000000,-1000.000000,-500,-750.000000,-750.000000,500)
AddZone("�-10", -750.000000,-1000.000000,-500,-500.000000,-750.000000,500)
AddZone("�-11", -500.000000,-1000.000000,-500,-250.000000,-750.000000,500)
AddZone("�-12", -250.000000,-1000.000000,-500,0.000000,-750.000000,500)
AddZone("�-13", 0.000000,-1000.000000,-500,250.000000,-750.000000,500)
AddZone("�-14", 250.000000,-1000.000000,-500,500.000000,-750.000000,500)
AddZone("�-15", 500.000000,-1000.000000,-500,750.000000,-750.000000,500)
AddZone("�-16", 750.000000,-1000.000000,-500,1000.000000,-750.000000,500)
AddZone("�-17", 1000.000000,-1000.000000,-500,1250.000000,-750.000000,500)
AddZone("�-18", 1250.000000,-1000.000000,-500,1500.000000,-750.000000,500)
AddZone("�-19", 1500.000000,-1000.000000,-500,1750.000000,-750.000000,500)
AddZone("�-20", 1750.000000,-1000.000000,-500,2000.000000,-750.000000,500)
AddZone("�-21", 2000.000000,-1000.000000,-500,2250.000000,-750.000000,500)
AddZone("�-22", 2250.000000,-1000.000000,-500,2500.000000,-750.000000,500)
AddZone("�-23", 2500.000000,-1000.000000,-500,2750.000000,-750.000000,500)
AddZone("�-24", 2750.000000,-1000.000000,-500,3000.000000,-750.000000,500)
AddZone("�-1", -3000.000000,-1250.000000,-500,-2750.000000,-1000.000000,500)
AddZone("�-2", -2750.000000,-1250.000000,-500,-2500.000000,-1000.000000,500)
AddZone("�-3", -2500.000000,-1250.000000,-500,-2250.000000,-1000.000000,500)
AddZone("�-4", -2250.000000,-1250.000000,-500,-2000.000000,-1000.000000,500)
AddZone("�-5", -2000.000000,-1250.000000,-500,-1750.000000,-1000.000000,500)
AddZone("�-6", -1750.000000,-1250.000000,-500,-1500.000000,-1000.000000,500)
AddZone("�-7", -1500.000000,-1250.000000,-500,-1250.000000,-1000.000000,500)
AddZone("�-8", -1250.000000,-1250.000000,-500,-1000.000000,-1000.000000,500)
AddZone("�-9", -1000.000000,-1250.000000,-500,-750.000000,-1000.000000,500)
AddZone("�-10", -750.000000,-1250.000000,-500,-500.000000,-1000.000000,500)
AddZone("�-11", -500.000000,-1250.000000,-500,-250.000000,-1000.000000,500)
AddZone("�-12", -250.000000,-1250.000000,-500,0.000000,-1000.000000,500)
AddZone("�-13", 0.000000,-1250.000000,-500,250.000000,-1000.000000,500)
AddZone("�-14", 250.000000,-1250.000000,-500,500.000000,-1000.000000,500)
AddZone("�-15", 500.000000,-1250.000000,-500,750.000000,-1000.000000,500)
AddZone("�-16", 750.000000,-1250.000000,-500,1000.000000,-1000.000000,500)
AddZone("�-17", 1000.000000,-1250.000000,-500,1250.000000,-1000.000000,500)
AddZone("�-18", 1250.000000,-1250.000000,-500,1500.000000,-1000.000000,500)
AddZone("�-19", 1500.000000,-1250.000000,-500,1750.000000,-1000.000000,500)
AddZone("�-20", 1750.000000,-1250.000000,-500,2000.000000,-1000.000000,500)
AddZone("�-21", 2000.000000,-1250.000000,-500,2250.000000,-1000.000000,500)
AddZone("�-22", 2250.000000,-1250.000000,-500,2500.000000,-1000.000000,500)
AddZone("�-23", 2500.000000,-1250.000000,-500,2750.000000,-1000.000000,500)
AddZone("�-24", 2750.000000,-1250.000000,-500,3000.000000,-1000.000000,500)
AddZone("�-1", -3000.000000,-1500.000000,-500,-2750.000000,-1250.000000,500)
AddZone("�-2", -2750.000000,-1500.000000,-500,-2500.000000,-1250.000000,500)
AddZone("�-3", -2500.000000,-1500.000000,-500,-2250.000000,-1250.000000,500)
AddZone("�-4", -2250.000000,-1500.000000,-500,-2000.000000,-1250.000000,500)
AddZone("�-5", -2000.000000,-1500.000000,-500,-1750.000000,-1250.000000,500)
AddZone("�-6", -1750.000000,-1500.000000,-500,-1500.000000,-1250.000000,500)
AddZone("�-7", -1500.000000,-1500.000000,-500,-1250.000000,-1250.000000,500)
AddZone("�-8", -1250.000000,-1500.000000,-500,-1000.000000,-1250.000000,500)
AddZone("�-9", -1000.000000,-1500.000000,-500,-750.000000,-1250.000000,500)
AddZone("�-10", -750.000000,-1500.000000,-500,-500.000000,-1250.000000,500)
AddZone("�-11", -500.000000,-1500.000000,-500,-250.000000,-1250.000000,500)
AddZone("�-12", -250.000000,-1500.000000,-500,0.000000,-1250.000000,500)
AddZone("�-13", 0.000000,-1500.000000,-500,250.000000,-1250.000000,500)
AddZone("�-14", 250.000000,-1500.000000,-500,500.000000,-1250.000000,500)
AddZone("�-15", 500.000000,-1500.000000,-500,750.000000,-1250.000000,500)
AddZone("�-16", 750.000000,-1500.000000,-500,1000.000000,-1250.000000,500)
AddZone("�-17", 1000.000000,-1500.000000,-500,1250.000000,-1250.000000,500)
AddZone("�-18", 1250.000000,-1500.000000,-500,1500.000000,-1250.000000,500)
AddZone("�-19", 1500.000000,-1500.000000,-500,1750.000000,-1250.000000,500)
AddZone("�-20", 1750.000000,-1500.000000,-500,2000.000000,-1250.000000,500)
AddZone("�-21", 2000.000000,-1500.000000,-500,2250.000000,-1250.000000,500)
AddZone("�-22", 2250.000000,-1500.000000,-500,2500.000000,-1250.000000,500)
AddZone("�-23", 2500.000000,-1500.000000,-500,2750.000000,-1250.000000,500)
AddZone("�-24", 2750.000000,-1500.000000,-500,3000.000000,-1250.000000,500)
AddZone("�-1", -3000.000000,-1750.000000,-500,-2750.000000,-1500.000000,500)
AddZone("�-2", -2750.000000,-1750.000000,-500,-2500.000000,-1500.000000,500)
AddZone("�-3", -2500.000000,-1750.000000,-500,-2250.000000,-1500.000000,500)
AddZone("�-4", -2250.000000,-1750.000000,-500,-2000.000000,-1500.000000,500)
AddZone("�-5", -2000.000000,-1750.000000,-500,-1750.000000,-1500.000000,500)
AddZone("�-6", -1750.000000,-1750.000000,-500,-1500.000000,-1500.000000,500)
AddZone("�-7", -1500.000000,-1750.000000,-500,-1250.000000,-1500.000000,500)
AddZone("�-8", -1250.000000,-1750.000000,-500,-1000.000000,-1500.000000,500)
AddZone("�-9", -1000.000000,-1750.000000,-500,-750.000000,-1500.000000,500)
AddZone("�-10", -750.000000,-1750.000000,-500,-500.000000,-1500.000000,500)
AddZone("�-11", -500.000000,-1750.000000,-500,-250.000000,-1500.000000,500)
AddZone("�-12", -250.000000,-1750.000000,-500,0.000000,-1500.000000,500)
AddZone("�-13", 0.000000,-1750.000000,-500,250.000000,-1500.000000,500)
AddZone("�-14", 250.000000,-1750.000000,-500,500.000000,-1500.000000,500)
AddZone("�-15", 500.000000,-1750.000000,-500,750.000000,-1500.000000,500)
AddZone("�-16", 750.000000,-1750.000000,-500,1000.000000,-1500.000000,500)
AddZone("�-17", 1000.000000,-1750.000000,-500,1250.000000,-1500.000000,500)
AddZone("�-18", 1250.000000,-1750.000000,-500,1500.000000,-1500.000000,500)
AddZone("�-19", 1500.000000,-1750.000000,-500,1750.000000,-1500.000000,500)
AddZone("�-20", 1750.000000,-1750.000000,-500,2000.000000,-1500.000000,500)
AddZone("�-21", 2000.000000,-1750.000000,-500,2250.000000,-1500.000000,500)
AddZone("�-22", 2250.000000,-1750.000000,-500,2500.000000,-1500.000000,500)
AddZone("�-23", 2500.000000,-1750.000000,-500,2750.000000,-1500.000000,500)
AddZone("�-24", 2750.000000,-1750.000000,-500,3000.000000,-1500.000000,500)
AddZone("�-1", -3000.000000,-2000.000000,-500,-2750.000000,-1750.000000,500)
AddZone("�-2", -2750.000000,-2000.000000,-500,-2500.000000,-1750.000000,500)
AddZone("�-3", -2500.000000,-2000.000000,-500,-2250.000000,-1750.000000,500)
AddZone("�-4", -2250.000000,-2000.000000,-500,-2000.000000,-1750.000000,500)
AddZone("�-5", -2000.000000,-2000.000000,-500,-1750.000000,-1750.000000,500)
AddZone("�-6", -1750.000000,-2000.000000,-500,-1500.000000,-1750.000000,500)
AddZone("�-7", -1500.000000,-2000.000000,-500,-1250.000000,-1750.000000,500)
AddZone("�-8", -1250.000000,-2000.000000,-500,-1000.000000,-1750.000000,500)
AddZone("�-9", -1000.000000,-2000.000000,-500,-750.000000,-1750.000000,500)
AddZone("�-10", -750.000000,-2000.000000,-500,-500.000000,-1750.000000,500)
AddZone("�-11", -500.000000,-2000.000000,-500,-250.000000,-1750.000000,500)
AddZone("�-12", -250.000000,-2000.000000,-500,0.000000,-1750.000000,500)
AddZone("�-13", 0.000000,-2000.000000,-500,250.000000,-1750.000000,500)
AddZone("�-14", 250.000000,-2000.000000,-500,500.000000,-1750.000000,500)
AddZone("�-15", 500.000000,-2000.000000,-500,750.000000,-1750.000000,500)
AddZone("�-16", 750.000000,-2000.000000,-500,1000.000000,-1750.000000,500)
AddZone("�-17", 1000.000000,-2000.000000,-500,1250.000000,-1750.000000,500)
AddZone("�-18", 1250.000000,-2000.000000,-500,1500.000000,-1750.000000,500)
AddZone("�-19", 1500.000000,-2000.000000,-500,1750.000000,-1750.000000,500)
AddZone("�-20", 1750.000000,-2000.000000,-500,2000.000000,-1750.000000,500)
AddZone("�-21", 2000.000000,-2000.000000,-500,2250.000000,-1750.000000,500)
AddZone("�-22", 2250.000000,-2000.000000,-500,2500.000000,-1750.000000,500)
AddZone("�-23", 2500.000000,-2000.000000,-500,2750.000000,-1750.000000,500)
AddZone("�-24", 2750.000000,-2000.000000,-500,3000.000000,-1750.000000,500)
AddZone("�-1", -3000.000000,-2250.000000,-500,-2750.000000,-2000.000000,500)
AddZone("�-2", -2750.000000,-2250.000000,-500,-2500.000000,-2000.000000,500)
AddZone("�-3", -2500.000000,-2250.000000,-500,-2250.000000,-2000.000000,500)
AddZone("�-4", -2250.000000,-2250.000000,-500,-2000.000000,-2000.000000,500)
AddZone("�-5", -2000.000000,-2250.000000,-500,-1750.000000,-2000.000000,500)
AddZone("�-6", -1750.000000,-2250.000000,-500,-1500.000000,-2000.000000,500)
AddZone("�-7", -1500.000000,-2250.000000,-500,-1250.000000,-2000.000000,500)
AddZone("�-8", -1250.000000,-2250.000000,-500,-1000.000000,-2000.000000,500)
AddZone("�-9", -1000.000000,-2250.000000,-500,-750.000000,-2000.000000,500)
AddZone("�-10", -750.000000,-2250.000000,-500,-500.000000,-2000.000000,500)
AddZone("�-11", -500.000000,-2250.000000,-500,-250.000000,-2000.000000,500)
AddZone("�-12", -250.000000,-2250.000000,-500,0.000000,-2000.000000,500)
AddZone("�-13", 0.000000,-2250.000000,-500,250.000000,-2000.000000,500)
AddZone("�-14", 250.000000,-2250.000000,-500,500.000000,-2000.000000,500)
AddZone("�-15", 500.000000,-2250.000000,-500,750.000000,-2000.000000,500)
AddZone("�-16", 750.000000,-2250.000000,-500,1000.000000,-2000.000000,500)
AddZone("�-17", 1000.000000,-2250.000000,-500,1250.000000,-2000.000000,500)
AddZone("�-18", 1250.000000,-2250.000000,-500,1500.000000,-2000.000000,500)
AddZone("�-19", 1500.000000,-2250.000000,-500,1750.000000,-2000.000000,500)
AddZone("�-20", 1750.000000,-2250.000000,-500,2000.000000,-2000.000000,500)
AddZone("�-21", 2000.000000,-2250.000000,-500,2250.000000,-2000.000000,500)
AddZone("�-22", 2250.000000,-2250.000000,-500,2500.000000,-2000.000000,500)
AddZone("�-23", 2500.000000,-2250.000000,-500,2750.000000,-2000.000000,500)
AddZone("�-24", 2750.000000,-2250.000000,-500,3000.000000,-2000.000000,500)
AddZone("�-1", -3000.000000,-2500.000000,-500,-2750.000000,-2250.000000,500)
AddZone("�-2", -2750.000000,-2500.000000,-500,-2500.000000,-2250.000000,500)
AddZone("�-3", -2500.000000,-2500.000000,-500,-2250.000000,-2250.000000,500)
AddZone("�-4", -2250.000000,-2500.000000,-500,-2000.000000,-2250.000000,500)
AddZone("�-5", -2000.000000,-2500.000000,-500,-1750.000000,-2250.000000,500)
AddZone("�-6", -1750.000000,-2500.000000,-500,-1500.000000,-2250.000000,500)
AddZone("�-7", -1500.000000,-2500.000000,-500,-1250.000000,-2250.000000,500)
AddZone("�-8", -1250.000000,-2500.000000,-500,-1000.000000,-2250.000000,500)
AddZone("�-9", -1000.000000,-2500.000000,-500,-750.000000,-2250.000000,500)
AddZone("�-10", -750.000000,-2500.000000,-500,-500.000000,-2250.000000,500)
AddZone("�-11", -500.000000,-2500.000000,-500,-250.000000,-2250.000000,500)
AddZone("�-12", -250.000000,-2500.000000,-500,0.000000,-2250.000000,500)
AddZone("�-13", 0.000000,-2500.000000,-500,250.000000,-2250.000000,500)
AddZone("�-14", 250.000000,-2500.000000,-500,500.000000,-2250.000000,500)
AddZone("�-15", 500.000000,-2500.000000,-500,750.000000,-2250.000000,500)
AddZone("�-16", 750.000000,-2500.000000,-500,1000.000000,-2250.000000,500)
AddZone("�-17", 1000.000000,-2500.000000,-500,1250.000000,-2250.000000,500)
AddZone("�-18", 1250.000000,-2500.000000,-500,1500.000000,-2250.000000,500)
AddZone("�-19", 1500.000000,-2500.000000,-500,1750.000000,-2250.000000,500)
AddZone("�-20", 1750.000000,-2500.000000,-500,2000.000000,-2250.000000,500)
AddZone("�-21", 2000.000000,-2500.000000,-500,2250.000000,-2250.000000,500)
AddZone("�-22", 2250.000000,-2500.000000,-500,2500.000000,-2250.000000,500)
AddZone("�-23", 2500.000000,-2500.000000,-500,2750.000000,-2250.000000,500)
AddZone("�-24", 2750.000000,-2500.000000,-500,3000.000000,-2250.000000,500)
AddZone("�-1", -3000.000000,-2750.000000,-500,-2750.000000,-2500.000000,500)
AddZone("�-2", -2750.000000,-2750.000000,-500,-2500.000000,-2500.000000,500)
AddZone("�-3", -2500.000000,-2750.000000,-500,-2250.000000,-2500.000000,500)
AddZone("�-4", -2250.000000,-2750.000000,-500,-2000.000000,-2500.000000,500)
AddZone("�-5", -2000.000000,-2750.000000,-500,-1750.000000,-2500.000000,500)
AddZone("�-6", -1750.000000,-2750.000000,-500,-1500.000000,-2500.000000,500)
AddZone("�-7", -1500.000000,-2750.000000,-500,-1250.000000,-2500.000000,500)
AddZone("�-8", -1250.000000,-2750.000000,-500,-1000.000000,-2500.000000,500)
AddZone("�-9", -1000.000000,-2750.000000,-500,-750.000000,-2500.000000,500)
AddZone("�-10", -750.000000,-2750.000000,-500,-500.000000,-2500.000000,500)
AddZone("�-11", -500.000000,-2750.000000,-500,-250.000000,-2500.000000,500)
AddZone("�-12", -250.000000,-2750.000000,-500,0.000000,-2500.000000,500)
AddZone("�-13", 0.000000,-2750.000000,-500,250.000000,-2500.000000,500)
AddZone("�-14", 250.000000,-2750.000000,-500,500.000000,-2500.000000,500)
AddZone("�-15", 500.000000,-2750.000000,-500,750.000000,-2500.000000,500)
AddZone("�-16", 750.000000,-2750.000000,-500,1000.000000,-2500.000000,500)
AddZone("�-17", 1000.000000,-2750.000000,-500,1250.000000,-2500.000000,500)
AddZone("�-18", 1250.000000,-2750.000000,-500,1500.000000,-2500.000000,500)
AddZone("�-19", 1500.000000,-2750.000000,-500,1750.000000,-2500.000000,500)
AddZone("�-20", 1750.000000,-2750.000000,-500,2000.000000,-2500.000000,500)
AddZone("�-21", 2000.000000,-2750.000000,-500,2250.000000,-2500.000000,500)
AddZone("�-22", 2250.000000,-2750.000000,-500,2500.000000,-2500.000000,500)
AddZone("�-23", 2500.000000,-2750.000000,-500,2750.000000,-2500.000000,500)
AddZone("�-24", 2750.000000,-2750.000000,-500,3000.000000,-2500.000000,500)
AddZone("�-1", -3000.000000,-3000.000000,-500,-2750.000000,-2750.000000,500)
AddZone("�-2", -2750.000000,-3000.000000,-500,-2500.000000,-2750.000000,500)
AddZone("�-3", -2500.000000,-3000.000000,-500,-2250.000000,-2750.000000,500)
AddZone("�-4", -2250.000000,-3000.000000,-500,-2000.000000,-2750.000000,500)
AddZone("�-5", -2000.000000,-3000.000000,-500,-1750.000000,-2750.000000,500)
AddZone("�-6", -1750.000000,-3000.000000,-500,-1500.000000,-2750.000000,500)
AddZone("�-7", -1500.000000,-3000.000000,-500,-1250.000000,-2750.000000,500)
AddZone("�-8", -1250.000000,-3000.000000,-500,-1000.000000,-2750.000000,500)
AddZone("�-9", -1000.000000,-3000.000000,-500,-750.000000,-2750.000000,500)
AddZone("�-10", -750.000000,-3000.000000,-500,-500.000000,-2750.000000,500)
AddZone("�-11", -500.000000,-3000.000000,-500,-250.000000,-2750.000000,500)
AddZone("�-12", -250.000000,-3000.000000,-500,0.000000,-2750.000000,500)
AddZone("�-13", 0.000000,-3000.000000,-500,250.000000,-2750.000000,500)
AddZone("�-14", 250.000000,-3000.000000,-500,500.000000,-2750.000000,500)
AddZone("�-15", 500.000000,-3000.000000,-500,750.000000,-2750.000000,500)
AddZone("�-16", 750.000000,-3000.000000,-500,1000.000000,-2750.000000,500)
AddZone("�-17", 1000.000000,-3000.000000,-500,1250.000000,-2750.000000,500)
AddZone("�-18", 1250.000000,-3000.000000,-500,1500.000000,-2750.000000,500)
AddZone("�-19", 1500.000000,-3000.000000,-500,1750.000000,-2750.000000,500)
AddZone("�-20", 1750.000000,-3000.000000,-500,2000.000000,-2750.000000,500)
AddZone("�-21", 2000.000000,-3000.000000,-500,2250.000000,-2750.000000,500)
AddZone("�-22", 2250.000000,-3000.000000,-500,2500.000000,-2750.000000,500)
AddZone("�-23", 2500.000000,-3000.000000,-500,2750.000000,-2750.000000,500)
AddZone("�-24", 2750.000000,-3000.000000,-500,3000.000000,-2750.000000,500)
}

getZoneByName(zName, ByRef CurZone ) {
     if ( bInitZaC == 0 )
    {
        initZonesAndCities()
        bInitZaC := 1
    }
        
    Loop % nZone-1
    {
        if (zone%A_Index%_name == zName)
        {
            ErrorLevel := ERROR_OK
            zone%A_Index%_name
            CurZone[1] :=  zone%A_Index%_name
            CurZone[2] := %A_Index%
            CurZone[3,1,1] := zone%A_Index%_x1
            CurZone[3,1,2] := zone%A_Index%_y1
            CurZone[3,1,3] := zone%A_Index%_z1
            CurZone[3,2,1] := zone%A_Index%_x2
            CurZone[3,2,2] := zone%A_Index%_y2
            CurZone[3,2,3] := zone%A_Index%_z2
            return true
        }
    }
    ErrorLevel := ERROR_ZONE_NOT_FOUND
    return "Unbekannt"
}

; PointPos[1] - X Point
; PointPos[2] - Z Point
getCenterPointToZone(zName, ByRef PointPos) {
    getZoneByName(zName, CurZone)
    PointPos[1] := 125 + CurZone[3,1,1]
    PointPos[2] := 125 + CurZone[3,1,2]
    return true
}



calculateZone(posX, posY, posZ) {
    
    if ( bInitZaC == 0 )
    {
        initZonesAndCities()
        bInitZaC := 1
    }
        
    Loop % nZone-1
    {
        if (posX >= zone%A_Index%_x1) && (posY >= zone%A_Index%_y1) && (posZ >= zone%A_Index%_z1) && (posX <= zone%A_Index%_x2) && (posY <= zone%A_Index%_y2) && (posZ <= zone%A_Index%_z2)
        {
            ErrorLevel := ERROR_OK
            return zone%A_Index%_name
        }
    }
    
    ErrorLevel := ERROR_ZONE_NOT_FOUND
    return "Unbekannt"
}

calculateCity(posX, posY, posZ) {
    
    if ( bInitZaC == 0 )
    {
        initZonesAndCities()
        bInitZaC := 1
    }
    smallestCity := "Unbekannt"
    currentCitySize := 0
    smallestCitySize := 0
    
    Loop % nCity-1
    {
        if (posX >= city%A_Index%_x1) && (posY >= city%A_Index%_y1) && (posZ >= city%A_Index%_z1) && (posX <= city%A_Index%_x2) && (posY <= city%A_Index%_y2) && (posZ <= city%A_Index%_z2)
        {
            currentCitySize := ((city%A_Index%_x2 - city%A_Index%_x1) * (city%A_Index%_y2 - city%A_Index%_y1) * (city%A_Index%_z2 - city%A_Index%_z1))
            if (smallestCity == "Unbekannt") || (currentCitySize < smallestCitySize)
            {
                smallestCity := city%A_Index%_name
                smallestCitySize := currentCitySize
            }
        }
    }
    
    if(smallestCity == "Unbekannt") {
        ErrorLevel := ERROR_CITY_NOT_FOUND
    } else {
        ErrorLevel := ERROR_OK
    }
    return smallestCity
}

/*
;do not work?
getCurrentZonecode() {
    if(!checkHandles())
        return ""
    
    return readString(hGTA, ADDR_ZONECODE, 5)
}
*/

AddZone(sName, x1, y1, z1, x2, y2, z2) {
    global
    zone%nZone%_name := sName
    zone%nZone%_x1 := x1
    zone%nZone%_y1 := y1
    zone%nZone%_z1 := z1
    zone%nZone%_x2 := x2
    zone%nZone%_y2 := y2
    zone%nZone%_z2 := z2
    nZone := nZone + 1
}

AddCity(sName, x1, y1, z1, x2, y2, z2) {
    global
    city%nCity%_name := sName
    city%nCity%_x1 := x1
    city%nCity%_y1 := y1
    city%nCity%_z1 := z1
    city%nCity%_x2 := x2
    city%nCity%_y2 := y2
    city%nCity%_z2 := z2
    nCity := nCity + 1
}

IsPlayerInRangeOfPoint(_posX, _posY, _posZ, _posRadius)
{
	GetPlayerPos(posX, posY, posZ)
	X := posX -_posX
	Y := posY -_posY
	Z := posZ -_posZ
	if(((X < _posRadius) && (X > -_posRadius)) && ((Y < _posRadius) && (Y > -_posRadius)) && ((Z < _posRadius) && (Z > -_posRadius)))
		return TRUE
	return FALSE
}
 
IsPlayerInRangeOfPoint2D(_posX, _posY, _posRadius)
{
 
	GetPlayerPos(posX, posY, posZ)
	X := posX - _posX
	Y := posY - _posY
	if(((X < _posRadius) && (X > -_posRadius)) && ((Y < _posRadius) && (Y > -_posRadius)))
		return TRUE
	return FALSE
}

getPlayerZone()
{
	aktPos := getCoordinates()
	return calculateZone(aktPos[1], aktPos[2], aktPos[3])
}

getPlayerCity()
{
	aktPos := getCoordinates()
	return calculateCity(aktPos[1], aktPos[2], aktPos[3])
}
AntiCrash(){
    If(!checkHandles())
        return false

    cReport := ADDR_SAMP_CRASHREPORT
    writeMemory(hGTA, dwSAMP + cReport, 0x90909090, 4)
    cReport += 0x4
    writeMemory(hGTA, dwSAMP + cReport, 0x90, 1)
    cReport += 0x9
    writeMemory(hGTA, dwSAMP + cReport, 0x90909090, 4)
    cReport += 0x4
    writeMemory(hGTA, dwSAMP + cReport, 0x90, 1)
}

writeMemory(hProcess,address,writevalue,length=4, datatype="int") {
  if(!hProcess) {
    ErrorLevel := ERROR_INVALID_HANDLE
    return false
  }

  VarSetCapacity(finalvalue,length, 0)
  NumPut(writevalue,finalvalue,0,datatype)
  dwRet :=  DllCall(  "WriteProcessMemory"
              ,"Uint",hProcess
              ,"Uint",address
              ,"Uint",&finalvalue
              ,"Uint",length
              ,"Uint",0)
  if(dwRet == 0) {
    ErrorLevel := ERROR_WRITE_MEMORY
    return false
  }

  ErrorLevel := ERROR_OK
  return true
}
; ##### Sonstiges #####
checkHandles() {
    if(iRefreshHandles+500>A_TickCount)
        return true
    iRefreshHandles:=A_TickCount
    if(!refreshGTA() || !refreshSAMP() || !refreshMemory()) {
        return false
    } else {
        return true
    }
    
    return true
}

; internal stuff
refreshGTA() {
    newPID := getPID("GTA:SA:MP")
    if(!newPID) {                            ; GTA not found
        if(hGTA) {                            ; open handle
            virtualFreeEx(hGTA, pMemory, 0, 0x8000)
            closeProcess(hGTA)
            hGTA := 0x0
        }
        dwGTAPID := 0
        hGTA := 0x0
        dwSAMP := 0x0
        pMemory := 0x0
        return false
    }
    
    if(!hGTA || (dwGTAPID != newPID)) {        ; changed PID, closed handle
        hGTA := openProcess(newPID)
        if(ErrorLevel) {                    ; openProcess fail
            dwGTAPID := 0
            hGTA := 0x0
            dwSAMP := 0x0
            pMemory := 0x0
            return false
        }
        dwGTAPID := newPID
        dwSAMP := 0x0
        pMemory := 0x0
        return true
    }
    return true
}

; internal stuff
refreshSAMP() {
    if(dwSAMP)
        return true
    
    dwSAMP := getModuleBaseAddress("samp.dll", hGTA)
    if(!dwSAMP)
        return false
    
    return true
}

; internal stuff
refreshMemory() {
    if(!pMemory) {
        pMemory     := virtualAllocEx(hGTA, 6144, 0x1000 | 0x2000, 0x40)
        if(ErrorLevel) {
            pMemory := 0x0
            return false
        }
        pParam1     := pMemory
        pParam2     := pMemory + 1024
        pParam3     := pMemory + 2048
        pParam4     := pMemory + 3072
        pParam5     := pMemory + 4096
        pInjectFunc := pMemory + 5120
    }
    return true
}

; internal stuff
getPID(szWindow) {
    local dwPID := 0
    WinGet, dwPID, PID, %szWindow%
    return dwPID
}

; internal stuff
openProcess(dwPID, dwRights = 0x1F0FFF) {
    hProcess := DllCall("OpenProcess"
                        , "UInt", dwRights
                        , "int",  0
                        , "UInt", dwPID
                        , "Uint")
    if(hProcess == 0) {
        ErrorLevel := ERROR_OPEN_PROCESS
        return 0
    }
    
    ErrorLevel := ERROR_OK
    return hProcess
}

; internal stuff
closeProcess(hProcess) {
    if(hProcess == 0) {
        ErrorLevel := ERROR_INVALID_HANDLE
        return 0
    }
    
    dwRet := DllCall(    "CloseHandle"
                        , "Uint", hProcess
                        , "UInt")
    ErrorLevel := ERROR_OK
}

; internal stuff
getModuleBaseAddress(sModule, hProcess) {
    if(!sModule) {
        ErrorLevel := ERROR_MODULE_NOT_FOUND
        return 0
    }
    
    if(!hProcess) {
        ErrorLevel := ERROR_INVALID_HANDLE
        return 0
    }
    
    dwSize = 1024*4                    ; 1024 * sizeof(HMODULE = 4)
    VarSetCapacity(hMods, dwSize)    
    VarSetCapacity(cbNeeded, 4)        ; DWORD = 4
    dwRet := DllCall(    "Psapi.dll\EnumProcessModules"
                        , "UInt", hProcess
                        , "UInt", &hMods
                        , "UInt", dwSize
                        , "UInt*", cbNeeded
                        , "UInt")
    if(dwRet == 0) {
        ErrorLevel := ERROR_ENUM_PROCESS_MODULES
        return 0
    }
    
    dwMods := cbNeeded / 4            ; cbNeeded / sizeof(HMDOULE = 4)
    i := 0
    VarSetCapacity(hModule, 4)        ; HMODULE = 4
    VarSetCapacity(sCurModule, 260)    ; MAX_PATH = 260
    while(i < dwMods) {
        hModule := NumGet(hMods, i*4)
        DllCall("Psapi.dll\GetModuleFileNameEx"
                , "UInt", hProcess
                , "UInt", hModule
                , "Str", sCurModule
                , "UInt", 260)
        SplitPath, sCurModule, sFilename
        if(sModule == sFilename) {
            ErrorLevel := ERROR_OK
            return hModule
        }
        i := i + 1
    }
    
    ErrorLevel := ERROR_MODULE_NOT_FOUND
    return 0
}

; internal stuff
readString(hProcess, dwAddress, dwLen) {
    if(!hProcess) {
        ErrorLevel := ERROR_INVALID_HANDLE
        return 0
    }
    
    VarSetCapacity(sRead, dwLen)
    dwRet := DllCall(    "ReadProcessMemory"
                        , "UInt", hProcess
                        , "UInt", dwAddress
                        , "Str", sRead
                        , "UInt", dwLen
                        , "UInt*", 0
                        , "UInt")
    if(dwRet == 0) {
        ErrorLevel := ERROR_READ_MEMORY
        return 0
    }
    
    ErrorLevel := ERROR_OK
    if A_IsUnicode
        return __ansiToUnicode(sRead)
    return sRead
}

; internal stuff
readFloat(hProcess, dwAddress) {
    if(!hProcess) {
        ErrorLevel := ERROR_INVALID_HANDLE
        return 0
    }
    
    VarSetCapacity(dwRead, 4)    ; float = 4
    dwRet := DllCall(    "ReadProcessMemory"
                        , "UInt",  hProcess
                        , "UInt",  dwAddress
                        , "Str",   dwRead
                        , "UInt",  4
                        , "UInt*", 0
                        , "UInt")
    if(dwRet == 0) {
        ErrorLevel := ERROR_READ_MEMORY
        return 0
    }
    
    ErrorLevel := ERROR_OK
    return NumGet(dwRead, 0, "Float")
}

; internal stuff
readDWORD(hProcess, dwAddress) {
    if(!hProcess) {
        ErrorLevel := ERROR_INVALID_HANDLE
        return 0
    }
    
    VarSetCapacity(dwRead, 4)    ; DWORD = 4
    dwRet := DllCall(    "ReadProcessMemory"
                        , "UInt",  hProcess
                        , "UInt",  dwAddress
                        , "Str",   dwRead
                        , "UInt",  4
                        , "UInt*", 0)
    if(dwRet == 0) {
        ErrorLevel := ERROR_READ_MEMORY
        return 0
    }
    
    ErrorLevel := ERROR_OK
    return NumGet(dwRead, 0, "UInt")
}

; internal stuff
readMem(hProcess, dwAddress, dwLen=4, type="UInt") {
    if(!hProcess) {
        ErrorLevel := ERROR_INVALID_HANDLE
        return 0
    }
    
    VarSetCapacity(dwRead, dwLen)
    dwRet := DllCall(    "ReadProcessMemory"
                        , "UInt",  hProcess
                        , "UInt",  dwAddress
                        , "Str",   dwRead
                        , "UInt",  dwLen
                        , "UInt*", 0)
    if(dwRet == 0) {
        ErrorLevel := ERROR_READ_MEMORY
        return 0
    }
    
    ErrorLevel := ERROR_OK
    return NumGet(dwRead, 0, type)
}

; internal stuff
writeString(hProcess, dwAddress, wString) {
    if(!hProcess) {
        ErrorLevel := ERROR_INVALID_HANDLE
        return false
    }
    
    sString := wString
    if A_IsUnicode
        sString := __unicodeToAnsi(wString)
    
    dwRet := DllCall(    "WriteProcessMemory"
                        , "UInt", hProcess
                        , "UInt", dwAddress
                        , "Str", sString
                        , "UInt", StrLen(wString) + 1
                        , "UInt", 0
                        , "UInt")
    if(dwRet == 0) {
        ErrorLEvel := ERROR_WRITE_MEMORY
        return false
    }
    
    ErrorLevel := ERROR_OK
    return true
}

; internal stuff
writeRaw(hProcess, dwAddress, pBuffer, dwLen) {
    if(!hProcess) {
        ErrorLevel := ERROR_INVALID_HANDLE
        return false
    }
    
    dwRet := DllCall(    "WriteProcessMemory"
                        , "UInt", hProcess
                        , "UInt", dwAddress
                        , "UInt", pBuffer
                        , "UInt", dwLen
                        , "UInt", 0
                        , "UInt")
    if(dwRet == 0) {
        ErrorLEvel := ERROR_WRITE_MEMORY
        return false
    }
    
    ErrorLevel := ERROR_OK
    return true
}

; internal stuff
Memory_ReadByte(process_handle, address) {
	VarSetCapacity(value, 1, 0)
	DllCall("ReadProcessMemory", "UInt", process_handle, "UInt", address, "Str", value, "UInt", 1, "UInt *", 0)
	return, NumGet(value, 0, "Byte")
}

; internal stuff
callWithParams(hProcess, dwFunc, aParams, bCleanupStack = true) {
    if(!hProcess) {
        ErrorLevel := ERROR_INVALID_HANDLE
        return false
    }
    validParams := 0
    
    i := aParams.MaxIndex()
    
    ;         i * PUSH + CALL + RETN
    dwLen := i * 5    + 5    + 1
    if(bCleanupStack)
        dwLen += 3
    VarSetCapacity(injectData, i * 5    + 5       + 3       + 1, 0)
    
    i_ := 1
    while(i > 0) {
        if(aParams[i][1] != "") {
            dwMemAddress := 0x0
            if(aParams[i][1] == "p") {
                dwMemAddress := aParams[i][2]
            } else if(aParams[i][1] == "s") {
                if(i_>3)
                    return false
                dwMemAddress := pParam%i_%
                writeString(hProcess, dwMemAddress, aParams[i][2])
                if(ErrorLevel)
                    return false
                i_ += 1
            } else if(aParams[i][1] == "i") {
                dwMemAddress := aParams[i][2]
            } else {
                return false
            }
            NumPut(0x68, injectData, validParams * 5, "UChar")
            NumPut(dwMemAddress, injectData, validParams * 5 + 1, "UInt")
            validParams += 1
        }
        i -= 1
    }
    
    offset := dwFunc - ( pInjectFunc + validParams * 5 + 5 )
    NumPut(0xE8, injectData, validParams * 5, "UChar")
    NumPut(offset, injectData, validParams * 5 + 1, "Int")
    
    if(bCleanupStack) {
        NumPut(0xC483, injectData, validParams * 5 + 5, "UShort")
        NumPut(validParams*4, injectData, validParams * 5 + 7, "UChar")
        
        NumPut(0xC3, injectData, validParams * 5 + 8, "UChar")
    } else {
        NumPut(0xC3, injectData, validParams * 5 + 5, "UChar")
    }
    
    writeRaw(hGTA, pInjectFunc, &injectData, dwLen)
    if(ErrorLevel)
        return false
    
    hThread := createRemoteThread(hGTA, 0, 0, pInjectFunc, 0, 0, 0)
    if(ErrorLevel)
        return false
    
    waitForSingleObject(hThread, 0xFFFFFFFF)
    
    closeProcess(hThread)
    
    return true
}

; internal stuff
virtualAllocEx(hProcess, dwSize, flAllocationType, flProtect) {
    if(!hProcess) {
        ErrorLevel := ERROR_INVALID_HANDLE
        return 0
    }
    
    dwRet := DllCall(    "VirtualAllocEx"
                        , "UInt", hProcess
                        , "UInt", 0
                        , "UInt", dwSize
                        , "UInt", flAllocationType
                        , "UInt", flProtect
                        , "UInt")
    if(dwRet == 0) {
        ErrorLEvel := ERROR_ALLOC_MEMORY
        return 0
    }
    
    ErrorLevel := ERROR_OK
    return dwRet
}

; internal stuff
virtualFreeEx(hProcess, lpAddress, dwSize, dwFreeType) {
    if(!hProcess) {
        ErrorLevel := ERROR_INVALID_HANDLE
        return 0
    }
    
    dwRet := DllCall(    "VirtualFreeEx"
                        , "UInt", hProcess
                        , "UInt", lpAddress
                        , "UInt", dwSize
                        , "UInt", dwFreeType
                        , "UInt")
    if(dwRet == 0) {
        ErrorLEvel := ERROR_FREE_MEMORY
        return 0
    }
    
    ErrorLevel := ERROR_OK
    return dwRet
}

; internal stuff
createRemoteThread(hProcess, lpThreadAttributes, dwStackSize, lpStartAddress, lpParameter, dwCreationFlags, lpThreadId) {
    if(!hProcess) {
        ErrorLevel := ERROR_INVALID_HANDLE
        return 0
    }
    
    dwRet := DllCall(    "CreateRemoteThread"
                        , "UInt", hProcess
                        , "UInt", lpThreadAttributes
                        , "UInt", dwStackSize
                        , "UInt", lpStartAddress
                        , "UInt", lpParameter
                        , "UInt", dwCreationFlags
                        , "UInt", lpThreadId
                        , "UInt")
    if(dwRet == 0) {
        ErrorLEvel := ERROR_ALLOC_MEMORY
        return 0
    }
    
    ErrorLevel := ERROR_OK
    return dwRet
}

; internal stuff
waitForSingleObject(hThread, dwMilliseconds) {
    if(!hThread) {
        ErrorLevel := ERROR_INVALID_HANDLE
        return 0
    }
    
    dwRet := DllCall(    "WaitForSingleObject"
                        , "UInt", hThread
                        , "UInt", dwMilliseconds
                        , "UInt")
    if(dwRet == 0xFFFFFFFF) {
        ErrorLEvel := ERROR_WAIT_FOR_OBJECT
        return 0
    }
    
    ErrorLevel := ERROR_OK
    return dwRet
}

; internal stuff
__ansiToUnicode(sString, nLen = 0) {
   If !nLen
   {
      nLen := DllCall("MultiByteToWideChar"
      , "Uint", 0
      , "Uint", 0
      , "Uint", &sString
      , "int",  -1
      , "Uint", 0
      , "int",  0)
   }

   VarSetCapacity(wString, nLen * 2)

   DllCall("MultiByteToWideChar"
      , "Uint", 0
      , "Uint", 0
      , "Uint", &sString
      , "int",  -1
      , "Uint", &wString
      , "int",  nLen)
      
    return wString
}

; internal stuff
__unicodeToAnsi(wString, nLen = 0) {
   pString := wString + 1 > 65536 ? wString : &wString

   If !nLen
   {
      nLen := DllCall("WideCharToMultiByte"
      , "Uint", 0
      , "Uint", 0
      , "Uint", pString
      , "int",  -1
      , "Uint", 0
      , "int",  0
      , "Uint", 0
      , "Uint", 0)
   }

   VarSetCapacity(sString, nLen)

   DllCall("WideCharToMultiByte"
      , "Uint", 0
      , "Uint", 0
      , "Uint", pString
      , "int",  -1
      , "str",  sString
      , "int",  nLen
      , "Uint", 0
      , "Uint", 0)
    return sString
}
