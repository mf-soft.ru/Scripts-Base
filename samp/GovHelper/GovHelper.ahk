#Include SAMPMAX.ahk
#IfWinActive GTA:SA:MP

version := 1.2
LawsPath:= A_ScriptDir . "\Laws"
LectionsPath:= A_ScriptDir . "\Lections"
strok := 6
stroka := 0
Gmenu :=0
Lmenu := 0
Smenu := 0
SpisDialog := 0
InputDialog := 0
a:= 0
Fract := []
IniRead, TimeWait, Settings.ini, LectionSettings, Milliseconds
if TimeWait == ""
{
	TimeWait:= 3100 
}
Fract[1,1] := "SFA"
Fract[2,1] := "LVA"
Fract[3,1] := "LSPD"
Fract[4,1] := "SFPD"
Fract[5,1] := "LVPD"
Fract[6,1] := "FBI"
Fract[7,1] := "MOH"
Fract[8,1] := "OTHER"

IniRead, FractName, Settings.ini, SFA, ListCount
Fract[1,2] := FractName
IniRead, FractName, Settings.ini, LVA, ListCount
Fract[2,2] := FractName
IniRead, FractName, Settings.ini, LSPD, ListCount
Fract[3,2] := FractName
IniRead, FractName, Settings.ini, SFPD, ListCount
Fract[4,2] := FractName
IniRead, FractName, Settings.ini, LVPD, ListCount
Fract[5,2] := FractName
IniRead, FractName, Settings.ini, FBI, ListCount
Fract[6,2] := FractName
IniRead, FractName, Settings.ini, MOH, ListCount
Fract[7,2] := FractName
IniRead, FractName, Settings.ini, OTHER, ListCount
Fract[7,2] := FractName
i:=0
Loop, 8
{
	i++
	j:=0
	Loop, % Fract[i,2]
	{
		j++
		IniRead, PlayerNick, Settings.ini, % Fract[i,1], Player%j%
		Fract[i,3,j] := PlayerNick
	}
}

checkversion := "true"
try {
HTTP := ComObjCreate("WinHTTP.WinHTTPRequest.5.1")
HTTP.SetTimeouts(2000,2000,2000,2000)
HTTP.Open("GET", "http://mcfree.bplaced.net/govversion.php?Vers=" version )
HTTP.Send()
checkversion :=  HTTP.ResponseText
}catch e {
}
StringSplit, versions, checkversion, |
if (versions1 != "true")
	TrayTip, Goverment Helper %version%, ��� ������ ������� ������ ������ Application `n �����������: McFree,
else {
	TrayTip, Goverment Helper %version%, ����� ����� ����������: v%versions2%`n������������� �������� ������:`n �� �����: mcfree.bplaced.net,
	addChatMessage("{1db0fd}����� ����� ���������� Goverment Helper: {e8e517}" . versions2 )
	addChatMessage("{1db0fd}������� �� ����� mcfree.bplaced.net")
}

^1:: Pause
return
^2:: Reload
return


~Esc::
if (Gmenu == 1)
{
	Gmenu := 0
}
if (Lmenu == 1)
{
	Lmenu := 0
}
if (Lectmenu == 1)
{
	Lectmenu := 0
}
if (Smenu == 1)
{
	Smenu := 0
}
if (InputDialog == 1)
{
	InputDialog := 0
}
if (SpisDialog == 1)
{
	SpisDialog := 0
}
BlockInput, MouseMoveOff
return


~F6::
if (Gmenu == 1)
{
	Gmenu := 0
}
if (Lmenu == 1)
{
	Lmenu := 0
}
if (Lectmenu == 1)
{
	Lectmenu := 0
}
if (Smenu == 1)
{
	Smenu := 0
}
if (InputDialog == 1)
{
	InputDialog := 0
}
if (SpisDialog == 1)
{
	SpisDialog := 0
}
BlockInput, MouseMoveOff
return


AppsKey::
a:=1
Gmenu:=1
BlockInput, MouseMove
ShowDialog("2", "Goverment Helper", "[1] ������ `n[2] ������`n[3] ������`n[4] �������� ������ ������`n[5] ������ ������� �� ������`n[6] �������� ������ � ������ (���)","������")
return


~UP::
KeyWait Up
if (Gmenu == 1 || Lmenu == 1 || Lectmenu == 1 || Smenu == 1) {
	if (a>1)
	{
		a:=a-1
	}
}
return
~Down::
KeyWait Down
if (Gmenu == 1 || Lmenu == 1 || Lectmenu == 1 || Smenu == 1) {
	if (a<strok)
	{
		a:=a+1
	}
}
return


~Enter::
KeyWait Enter
if (Gmenu == 1) {
BlockInput, MouseMoveOff
Gpunk:="Glabel"+a
Gmenu:=0
Gosub, %Gpunk%
}
if (Lmenu == 1) {
BlockInput, MouseMoveOff
Lpunk:="Llabel"+a
Lmenu:=0
Gosub, %Lpunk%
}
if (Lectmenu == 1) {
BlockInput, MouseMoveOff
Lectpunk:="Lectlabel"+a
Lectmenu:=0
Gosub, %Lectpunk%
}
if (Smenu == 1) {
BlockInput, MouseMoveOff
Spunk:="Slabel"+a
Smenu:=0
Gosub, %Spunk%
}
return


;===============================================================
;===============================================================
;===============================================================
Glabel1:
if (a != 1)
{
	return
}
a:=1
Lmenu := 1
strok := 0
BlockInput, MouseMove
if( FileExist( LawsPath . "\Law1.txt"))
{
	FileReadLine, Lname1, Laws\Law1.txt, 1
	strok := 1
}
if( FileExist(LawsPath . "\Law2.txt"))
{
	FileReadLine, Lname2, Laws\Law2.txt, 1
	strok := 2
}
if( FileExist(LawsPath . "\Law3.txt"))
{
	FileReadLine, Lname3, Laws\Law3.txt, 1
	strok := 3
}
if( FileExist(LawsPath . "\Law4.txt"))
{
	FileReadLine, Lname4, Laws\Law4.txt, 1
	strok := 4
}
if( FileExist(LawsPath . "\Law5.txt"))
{
	FileReadLine, Lname5, Laws\Law5.txt, 1
	strok := 5
}
if( FileExist(LawsPath . "\Law6.txt"))
{
	FileReadLine, Lname6, Laws\Law6.txt, 1
	strok := 6
}
if( FileExist(LawsPath . "\Law7.txt"))
{
	FileReadLine, Lname7, Laws\Law7.txt, 1
	strok := 7
}
if( FileExist(LawsPath . "\Law8.txt"))
{
	FileReadLine, Lname8, Laws\Law8.txt, 1
	strok := 8
}
if( FileExist(LawsPath . "\Law9.txt"))
{
	FileReadLine, Lname9, Laws\Law9.txt, 1
	strok := 9
}
Sleep, 400
if  (strok == 0) {
ShowDialog("0", "������", "������, �� ����� ������ �������","������")
}
if  (strok == 1) {
ShowDialog("2", "������", "[1] " Lname1 "","������")
}

if  (strok == 2) {
ShowDialog("2", "������", "[1] " Lname1 "`n[2] " Lname2 "","������")
}

if  (strok == 3) {
ShowDialog("2", "������", "[1] " Lname1 "`n[2] " Lname2 "`n[3] " Lname3 "","������")
}

if  (strok == 4) {
ShowDialog("2", "������", "[1] " Lname1 "`n[2] " Lname2 "`n[3] " Lname3 "`n[4] " Lname4 "","������")
}

if  (strok == 5) {
ShowDialog("2", "������", "[1] " Lname1 "`n[2] " Lname2 "`n[3] " Lname3 "`n[4] " Lname4 "`n[5] " Lname5 "","������")
}

if  (strok == 6) {
ShowDialog("2", "������", "[1] " Lname1 "`n[2] " Lname2 "`n[3] " Lname3 "`n[4] " Lname4 "`n[5] " Lname5 "`n[6] " Lname6 "","������")
}

if  (strok == 7) {
ShowDialog("2", "������", "[1] " Lname1 "`n[2] " Lname2 "`n[3] " Lname3 "`n[4] " Lname4 "`n[5] " Lname5 "`n[6] " Lname6 "`n[7] " Lname7 "","������")
}

if  (strok == 8) {
ShowDialog("2", "������", "[1] " Lname1 "`n[2] " Lname2 "`n[3] " Lname3 "`n[4] " Lname4 "`n[5] " Lname5 "`n[6] " Lname6 "`n[7] " Lname7 "`n[8] " Lname8 "","������")
}

if  (strok == 9) {
ShowDialog("2", "������", "[1] " Lname1 "`n[2] " Lname2 "`n[3] " Lname3 "`n[4] " Lname4 "`n[5] " Lname5 "`n[6] " Lname6 "`n[7] " Lname7 "`n[8] " Lname8 "`n[9] " Lname9 "","������")
}
KeyWait Enter, D
return



Glabel2:
if (a != 2)
{
	return
}
a:=1
Lectmenu := 1
strok := 0
BlockInput, MouseMove
if( FileExist(LectionsPath . "\Lection1.txt"))
{
	FileReadLine, Lname1, Lections\Lection1.txt, 1
	strok := 1
}
if( FileExist(LectionsPath . "\Lection2.txt"))
{
	FileReadLine, Lname2, Lections\Lection2.txt, 1
	strok := 2
}
if( FileExist(LectionsPath . "\Lection3.txt"))
{
	FileReadLine, Lname3, Lections\Lection3.txt, 1
	strok := 3
}
if( FileExist(LectionsPath . "\Lection4.txt"))
{
	FileReadLine, Lname4, Lections\Lection4.txt, 1
	strok := 4
}
if( FileExist(LectionsPath . "\Lection5.txt"))
{
	FileReadLine, Lname5, Lections\Lection5.txt, 1
	strok := 5
}
if( FileExist(LectionsPath . "\Lection6.txt"))
{
	FileReadLine, Lname6, Lections\Lection6.txt, 1
	strok := 6
}
if( FileExist(LectionsPath . "\Lection7.txt"))
{
	FileReadLine, Lname7, Lections\Lection7.txt, 1
	strok := 7
}
if( FileExist(LectionsPath . "\Lection8.txt"))
{
	FileReadLine, Lname8, Lections\Lection8.txt, 1
	strok := 8
}
if( FileExist(LectionsPath . "\Lection9.txt"))
{
	FileReadLine, Lname9, Lections\Lection9.txt, 1
	strok := 9
}
Sleep, 400
if  (strok == 0) {
ShowDialog("0", "������", "������, �� ����� ������ �������","������")
}
if  (strok == 1) {
ShowDialog("2", "������", "[1] " Lname1 "","������")
}

if  (strok == 2) {
ShowDialog("2", "������", "[1] " Lname1 "`n[2] " Lname2 "","������")
}

if  (strok == 3) {
ShowDialog("2", "������", "[1] " Lname1 "`n[2] " Lname2 "`n[3] " Lname3 "","������")
}

if  (strok == 4) {
ShowDialog("2", "������", "[1] " Lname1 "`n[2] " Lname2 "`n[3] " Lname3 "`n[4] " Lname4 "","������")
}

if  (strok == 5) {
ShowDialog("2", "������", "[1] " Lname1 "`n[2] " Lname2 "`n[3] " Lname3 "`n[4] " Lname4 "`n[5] " Lname5 "","������")
}

if  (strok == 6) {
ShowDialog("2", "������", "[1] " Lname1 "`n[2] " Lname2 "`n[3] " Lname3 "`n[4] " Lname4 "`n[5] " Lname5 "`n[6] " Lname6 "","������")
}

if  (strok == 7) {
ShowDialog("2", "������", "[1] " Lname1 "`n[2] " Lname2 "`n[3] " Lname3 "`n[4] " Lname4 "`n[5] " Lname5 "`n[6] " Lname6 "`n[7] " Lname7 "","������")
}

if  (strok == 8) {
ShowDialog("2", "������", "[1] " Lname1 "`n[2] " Lname2 "`n[3] " Lname3 "`n[4] " Lname4 "`n[5] " Lname5 "`n[6] " Lname6 "`n[7] " Lname7 "`n[8] " Lname8 "","������")
}

if  (strok == 9) {
ShowDialog("2", "������", "[1] " Lname1 "`n[2] " Lname2 "`n[3] " Lname3 "`n[4] " Lname4 "`n[5] " Lname5 "`n[6] " Lname6 "`n[7] " Lname7 "`n[8] " Lname8 "`n[9] " Lname9 "","������")
}
KeyWait Enter, D
return



Glabel3:
if (a != 3)
{
	return
}
Sleep, 600
ShowDialog("0", "������", "������ �������� ���� ����� ������ Application`n� ������� ���������� 3 ����:`n 1- �������, Main Menu � ������� ����� ������� ������, ������ � ������`n 2 - ������ - � ������� ����� ������� ������ ������� �� ������� ���� � ����� Laws (�������� 9 �������)`n ������ - � ������� ����� ������� ������ ������ ������� �� ������� � ����� Lections (�������� 9 ������)`n 3 - ������ - � � ������ ������`n`n ��� ���� ����� ������� ����� � ����, �������� ��������� ���� � ����� Laws � �������� ��� Law[����� � ������].txt`n 4 - ��������� ������� �������� ��� ������ � ������������� (1 ��� ~ 1000 ���������� ������� ������� 1100) `n 5 - ������������� ������ ��� ������������ �������, ��� �������� ������� ������������ ������� `n �� � ���� ������ �� ���������� ������������� ������ ������! `n ���������� � �������� - � ����� Lections ������� ���� Lection[����� � ������].txt`n`n ������ ����� ������������� ����������� � ����������� ������������.`n`n ����� � ������������� ������� Goverment Helper !","������")
return



Glabel4:
if (a != 4)
{
	return
}
InputDialog := 1
Sleep, 500
showDialog(1, "{2497dd}���������", "������� ����� � ��������, ������ ������� `n����� ��������� ��������� ������ ������:", "�����")
Input, TSeconds, V, {enter}
KeyWait Enter, D
mSeconds := TSeconds * 1000 + 100
TimeWait := mSeconds
IniWrite, %mSeconds%, Settings.ini, LectionSettings, Milliseconds
return

Glabel5:
if (a != 5)
{
	return
}
strok:=8
a:=1
Smenu:=1
Sleep, 500
BlockInput, MouseMove
ShowDialog("2", "������ �� ��������", "[1] San Fierro Army `n[2] Las Venturas Army`n[3] Las Santos Police Deportament`n[4] San Fierro Police Deportament`n[5] Las Venturas Police Deportament`n[6] FBI`n[7] Ministry Of Health`n[8] Other","������")
KeyWait Enter, D
return

Glabel6:
ImputDialog := 1
Sleep, 500
showDialog(1, "{2497dd}���������� � ������ ������", "������� ������ ��� ������:", "�����")
Input, NickName, V, {enter}
KeyWait Enter, D
Sleep, 200
showDialog(1, "{2497dd}���������� � ������ ������", "������� �������� ����������� �� ����:`nSFA`nLVA`nLSPD`nSFPD`nLVPD`nFBI`nMOH`nOTHER", "�����")
Input, FractionType, V, {enter}
KeyWait Enter, D
if (FractionType == "SFA")
{
	Fraction:=1
}
if (FractionType == "LVA")
{
	Fraction:=2
}
if (FractionType == "LSPD")
{
	Fraction:=3
}
if (FractionType == "SFPD")
{
	Fraction:=4
}
if (FractionType == "LVPD")
{
	Fraction:=5
}
if (FractionType == "FBI")
{
	Fraction:=6
}
if (FractionType == "MOH")
{
	Fraction:=7
}
if (FractionType == "OTHER")
{
	Fraction:=8
}
IniRead, LastPlayer, Settings.ini, %FractionType%, ListCount, 0
LastPlayer++
Fract[Fraction,3,LastPlayer] := NickName
IniWrite, %NickName%, Settings.ini, %FractionType%, Player%LastPlayer%
IniWrite, %LastPlayer%, Settings.ini, %FractionType%, ListCount 
return

;===============================================================
;===============================================================
;===============================================================

Llabel1:
dialogout := ""
if (strok >= 1)
{
	FileReadLine, Lname, Laws\Law1.txt, 1
	Sleep, 600
	stroka :=0
	Loop, Read, %LawsPath%/Law1.txt
	{
		stroka++
		if (stroka != 1)
		{
			dialogout .= A_LoopReadLine  "`n"
		}
	}
	ShowDialog("0", Lname , dialogout, "")
}
return
Llabel2:
dialogout := ""
if (strok >= 2)
{
	FileReadLine, Lname, Laws\Law2.txt, 1
	Sleep, 600
	stroka :=0
	Loop, Read, %LawsPath%/Law2.txt
	{
		stroka++
		if (stroka != 1)
		{
			dialogout .= A_LoopReadLine  "`n"
		}
	}
	ShowDialog("0",Lname, dialogout, "")
}
return
Llabel3:
dialogout := ""
if (strok >= 3)
{
	FileReadLine, Lname, Laws\Law3.txt, 1
	Sleep, 600
	stroka :=0
	Loop, Read, %LawsPath%/Law3.txt
	{
		stroka++
		if (stroka != 1)
		{
			dialogout .= A_LoopReadLine  "`n"
		}
	}
	ShowDialog("0",Lname, dialogout, "")
}
return
Llabel4:
dialogout := ""
if (strok >= 4)
{
	FileReadLine, Lname, Laws\Law4.txt, 1
	Sleep, 600
	stroka :=0
	Loop, Read, %LawsPath%/Law4.txt
	{
		stroka++
		if (stroka != 1)
		{
			dialogout .= A_LoopReadLine  "`n"
		}
	}
	ShowDialog("0",Lname, dialogout, "")
}
return
Llabel5:
dialogout := ""
if (strok >= 5)
{
	FileReadLine, Lname, Laws\Law5.txt, 1
	Sleep, 600
	stroka :=0
	Loop, Read, %LawsPath%/Law5.txt
	{
		stroka++
		if (stroka != 1)
		{
			dialogout .= A_LoopReadLine  "`n"
		}
	}
	ShowDialog("0",Lname, dialogout, "")
}
return
Llabel6:
dialogout := ""
if (strok >= 6)
{
	FileReadLine, Lname, Laws\Law6.txt, 1
	Sleep, 600
	stroka :=0
	Loop, Read, %LawsPath%/Law6.txt
	{
		stroka++
		if (stroka != 1)
		{
			dialogout .= A_LoopReadLine  "`n"
		}
	}
	ShowDialog("0",Lname, dialogout, "")
}
return
Llabel7:
dialogout := ""
if (strok >= 7)
{
	FileReadLine, Lname, Laws\Law7.txt, 1
	Sleep, 600
	stroka :=0
	Loop, Read, %LawsPath%/Law7.txt
	{
		stroka++
		if (stroka != 1)
		{
			dialogout .= A_LoopReadLine  "`n"
		}
	}
	ShowDialog("0",Lname, dialogout, "")
}
return
Llabel8:
dialogout := ""
if (strok >= 8)
{
	FileReadLine, Lname, Laws\Law8.txt, 1
	Sleep, 600
	stroka :=0
	Loop, Read, %LawsPath%/Law8.txt
	{
		stroka++
		if (stroka != 1)
		{
			dialogout .= A_LoopReadLine  "`n"
		}
	}
	ShowDialog("0",Lname, dialogout, "")
}
return
Llabel9:
dialogout := ""
if (strok >= 9)
{
	FileReadLine, Lname, Laws\Law9.txt, 1
	Sleep, 600
	stroka :=0
	Loop, Read, %LawsPath%/Law9.txt
	{
		stroka++
		if (stroka != 1)
		{
			dialogout .= A_LoopReadLine "`n"
		}
	}
	ShowDialog("0",Lname, dialogout, "")
}
return

;===============================================================
;===============================================================
;===============================================================

Lectlabel1:
SendMessage, 0x50,, 0x4190419,, A
if (strok >= 1)
{
	stroka :=0
	Loop, Read, %LectionsPath%/Lection1.txt
	{
		stroka++
		if (stroka != 1)
		{
			SendChat( A_LoopReadLine )
			Sleep, %TimeWait%
		}
	}
}
return
Lectlabel2:
SendMessage, 0x50,, 0x4190419,, A
if (strok >= 2)
{
	stroka :=0
	Loop, Read, %LectionsPath%/Lection2.txt
	{
		stroka++
		if (stroka != 1)
		{
			SendChat( A_LoopReadLine )
			Sleep, %TimeWait%
		}
	}
}
return
Lectlabel3:
SendMessage, 0x50,, 0x4190419,, A
if (strok >= 3)
{
	stroka :=0
	Loop, Read, %LectionsPath%/Lection3.txt
	{
		stroka++
		if (stroka != 1)
		{
			SendChat( A_LoopReadLine )
			Sleep, %TimeWait%
		}
	}
}
return
Lectlabel4:
SendMessage, 0x50,, 0x4190419,, A
if (strok >= 4)
{
	stroka :=0
	Loop, Read, %LectionsPath%/Lection4.txt
	{
		stroka++
		if (stroka != 1)
		{
			SendChat( A_LoopReadLine )
			Sleep, %TimeWait%
		}
	}
}
return
Lectlabel5:
SendMessage, 0x50,, 0x4190419,, A
if (strok >= 5)
{
	stroka :=0
	Loop, Read, %LectionsPath%/Lection5.txt
	{
		stroka++
		if (stroka != 1)
		{
			SendChat( A_LoopReadLine )
			Sleep, %TimeWait%
		}
	}
}
return
Lectlabel6:
SendMessage, 0x50,, 0x4190419,, A
if (strok >= 6)
{
	stroka :=0
	Loop, Read, %LectionsPath%/Lection6.txt
	{
		stroka++
		if (stroka != 1)
		{
			SendChat( A_LoopReadLine )
			Sleep, %TimeWait%
		}
	}
}
return
Lectlabel7:
SendMessage, 0x50,, 0x4190419,, A
if (strok >= 7)
{
	stroka :=0
	Loop, Read, %LectionsPath%/Lection7.txt
	{
		stroka++
		if (stroka != 1)
		{
			SendChat( A_LoopReadLine )
			Sleep, %TimeWait%
		}
	}
}
return
Lectlabel8:
SendMessage, 0x50,, 0x4190419,, A
if (strok >= 8)
{
	stroka :=0
	Loop, Read, %LectionsPath%/Lection8.txt
	{
		stroka++
		if (stroka != 1)
		{
			SendChat( A_LoopReadLine )
			Sleep, %TimeWait%
		}
	}
}
return
Lectlabel9:
SendMessage, 0x50,, 0x4190419,, A
if (strok >= 9)
{
	stroka :=0
	Loop, Read, %LectionsPath%/Lection9.txt
	{
		stroka++
		if (stroka != 1)
		{
			SendChat( A_LoopReadLine )
			Sleep, %TimeWait%
		}
	}
}
return

;===============================================================
;===============================================================
;===============================================================
Slabel1:
i:=0
SpisDialog := 1
IniRead, Last, Settings.ini, SFA, ListCount, 0
Loop, %Last%
{
	i++
	id := getPlayerIdByName(Fract[1,3,i])
	PlayerNick := Fract[1,3,i]
	PlayerID := id
	if (id >= 0)
	{
		PlayerStat := "{1c86ee}Online{e0ffff}"
	}
	else 
	{
		PlayerStat := "{105867}Offline{e0ffff}"
		PlayerID := "none"
	}
	dialogout.= PlayerStat . "`t" . PlayerNick . "`t" . PlayerID . "`n"
}
Sleep, 500
showDialog(0, "{2497dd}������ ������� �� ������ [SFA]", dialogout , "�����")
dialogout:=""
return

Slabel2:
i:=0
SpisDialog := 1
IniRead, Last, Settings.ini, LVA, ListCount, 0
Loop, %Last%
{
	i++
	id := getPlayerIdByName(Fract[2,3,i])
	PlayerNick := Fract[2,3,i]
	PlayerID := id
	if (id >= 0)
	{
		PlayerStat := "{1c86ee}Online{e0ffff}"
	}
	else 
	{
		PlayerStat := "{105867}Offline{e0ffff}"
		PlayerID := "none"
	}
	dialogout.= PlayerStat . "`t" . PlayerNick . "`t" . PlayerID . "`n"
}
Sleep, 500
showDialog(0, "{2497dd}������ ������� �� ������ [LVA]", dialogout , "�����")
dialogout:=""
return

Slabel3:
i:=0
SpisDialog := 1
IniRead, Last, Settings.ini, LSPD, ListCount, 0
Loop, %Last%
{
	i++
	id := getPlayerIdByName(Fract[3,3,i])
	PlayerNick := Fract[3,3,i]
	PlayerID := id
	if (id >= 0)
	{
		PlayerStat := "{1c86ee}Online{e0ffff}"
	}
	else 
	{
		PlayerStat := "{105867}Offline{e0ffff}"
		PlayerID := "none"
	}
	dialogout.= PlayerStat . "`t" . PlayerNick . "`t" . PlayerID . "`n"
}
Sleep, 500
showDialog(0, "{2497dd}������ ������� �� ������ [LSPD]", dialogout , "�����")
dialogout:=""
return

Slabel4:
i:=0
SpisDialog := 1
IniRead, Last, Settings.ini, SFPD, ListCount, 0
Loop, %Last%
{
	i++
	id := getPlayerIdByName(Fract[4,3,i])
	PlayerNick := Fract[4,3,i]
	PlayerID := id
	if (id >= 0)
	{
		PlayerStat := "{1c86ee}Online{e0ffff}"
	}
	else 
	{
		PlayerStat := "{105867}Offline{e0ffff}"
		PlayerID := "none"
	}
	dialogout.= PlayerStat . "`t" . PlayerNick . "`t" . PlayerID . "`n"
}
Sleep, 500
showDialog(0, "{2497dd}������ ������� �� ������ [SFPD]", dialogout , "�����")
dialogout:=""
return

Slabel5:
i:=0
SpisDialog := 1
IniRead, Last, Settings.ini, LVPD, ListCount, 0
Loop, %Last%
{
	i++
	id := getPlayerIdByName(Fract[5,3,i])
	PlayerNick := Fract[5,3,i]
	PlayerID := id
	if (id >= 0)
	{
		PlayerStat := "{1c86ee}Online{e0ffff}"
	}
	else 
	{
		PlayerStat := "{105867}Offline{e0ffff}"
		PlayerID := "none"
	}
	dialogout.= PlayerStat . "`t" . PlayerNick . "`t" . PlayerID . "`n"
}
Sleep, 500
showDialog(0, "{2497dd}������ ������� �� ������ [LVPD]", dialogout , "�����")
dialogout:=""
return

Slabel6:
i:=0
SpisDialog := 1
IniRead, Last, Settings.ini, FBI, ListCount, 0
Loop, %Last%
{
	i++
	id := getPlayerIdByName(Fract[6,3,i])
	PlayerNick := Fract[6,3,i]
	PlayerID := id
	if (id >= 0)
	{
		PlayerStat := "{1c86ee}Online{e0ffff}"
	}
	else 
	{
		PlayerStat := "{105867}Offline{e0ffff}"
		PlayerID := "none"
	}
	dialogout.= PlayerStat . "`t" . PlayerNick . "`t" . PlayerID . "`n"
}
Sleep, 500
showDialog(0, "{2497dd}������ ������� �� ������ [FBI]", dialogout , "�����")
dialogout:=""
return

Slabel7:
i:=0
SpisDialog := 1
IniRead, Last, Settings.ini, MOH, ListCount, 0
Loop, %Last%
{
	i++
	id := getPlayerIdByName(Fract[7,3,i])
	PlayerNick := Fract[7,3,i]
	PlayerID := id
	if (id >= 0)
	{
		PlayerStat := "{1c86ee}Online{e0ffff}"
	}
	else 
	{
		PlayerStat := "{105867}Offline{e0ffff}"
		PlayerID := "none"
	}
	dialogout.= PlayerStat . "`t" . PlayerNick . "`t" . PlayerID . "`n"
}
Sleep, 500
showDialog(0, "{2497dd}������ ������� �� ������ [MOH]", dialogout , "�����")
dialogout:=""
return

Slabel8:
i:=0
SpisDialog := 1
IniRead, Last, Settings.ini, OTHER, ListCount, 0
Loop, %Last%
{
	i++
	id := getPlayerIdByName(Fract[8,3,i])
	PlayerNick := Fract[8,3,i]
	PlayerID := id
	if (id >= 0)
	{
		PlayerStat := "{1c86ee}Online{e0ffff}"
	}
	else 
	{
		PlayerStat := "{105867}Offline{e0ffff}"
		PlayerID := "none"
	}
	dialogout.= PlayerStat . "`t" . PlayerNick . "`t" . PlayerID . "`n"
}
Sleep, 500
showDialog(0, "{2497dd}������ ������� �� ������ [OTHER]", dialogout , "�����")
dialogout:=""
return