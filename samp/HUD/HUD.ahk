#SingleInstance Force
#Include API.ahk
#MaxThreads 5
SetTimer, HP, Off
SetTimer, ARM, Off
SetTimer, WEAP, Off
SetTimer, vHP, Off
HP_Overlay := -1
ARM_Overlay := -1
WEAP_Overlay := -1
vHP_Overlay := -1

;IniSettings
;
;			MAIN
;
IniRead, AutoOn, Settings.ini, Main, AutoOn , 0
IniRead, MessageColor, Settings.ini, Main, MessageColor , 0xFF00d900
;
;			HEALTH
;
IniRead, HPPosX, Settings.ini, Health, PosX, 30
IniRead, HPPosY, Settings.ini, Health, PosY, 360
IniRead, HPColor, Settings.ini, Health, Color , 0xFFFFFF00
IniRead, HPFont, Settings.ini, Health, Font, "Tahoma"
IniRead, HPFontSize, Settings.ini, Health, FontSize, 7
IniRead, HPFontBold, Settings.ini, Health, FontBold, "false"
IniRead, HPFontItalic, Settings.ini, Health, FontItalic, "false"
IniRead, HPTextShadow, Settings.ini, Health, TextShadow, "true"
;
;			ARMOR
;
IniRead, ARMPosX, Settings.ini, Armor, PosX, 30
IniRead, ARMPosY, Settings.ini, Armor, PosY, 380
IniRead, ARMColor, Settings.ini, Armor, Color , 0xFFFFFF00
IniRead, ARMFont, Settings.ini, Armor, Font, "Tahoma"
IniRead, ARMFontSize, Settings.ini, Armor, FontSize, 7
IniRead, ARMFontBold, Settings.ini, Armor, FontBold, "false"
IniRead, ARMFontItalic, Settings.ini, Armor, FontItalic, "false"
IniRead, ARMTextShadow, Settings.ini, Armor, TextShadow, "true"
;
;			WEAPON
;
IniRead, WEAPONPosX, Settings.ini, Weapon, PosX, 30
IniRead, WEAPONPosY, Settings.ini, Weapon, PosY, 400
IniRead, WEAPONColor, Settings.ini, Weapon, Color , 0xFFFFFF00
IniRead, WEAPONFont, Settings.ini, Weapon, Font, "Tahoma"
IniRead, WEAPONFontSize, Settings.ini, Weapon, FontSize, 7
IniRead, WEAPONFontBold, Settings.ini, Weapon, FontBold, "false"
IniRead, WEAPONFontItalic, Settings.ini, Weapon, FontItalic, "false"
IniRead, WEAPONTextShadow, Settings.ini, Weapon, TextShadow, "true"
;
;			VEHICLE
;
IniRead, VEHPosX, Settings.ini, Vehicle, PosX, 30
IniRead, VEHPosY, Settings.ini, Vehicle, PosY, 420
IniRead, VEHColor, Settings.ini, Vehicle, Color , 0xFFFFFF00
IniRead, VEHFont, Settings.ini, Vehicle, Font, "Tahoma"
IniRead, VEHFontSize, Settings.ini, Vehicle, FontSize, 7
IniRead, VEHFontBold, Settings.ini, Vehicle, FontBold, "false"
IniRead, VEHFontItalic, Settings.ini, Vehicle, FontItalic, "false"
IniRead, VEHTextShadow, Settings.ini, Vehicle, TextShadow, "true"

if (AutoOn == 1) {
	SetTimer, HP, 500
	SetTimer, ARM, 500
	SetTimer, WEAP, 500
	SetTimer, vHP, 500
}

;Dark Undergound
HP:
{
	if ( WinActive("GTA:SA:MP"))
	{
		if (HP_Overlay == -1) {
			HP_Overlay := TextCreate(HPFont, HPFontSize, HPFontBold, HPFontItalic, HPPosX, HPPosY, HPColor, "Health: " GetPlayerHealth() "%", HPTextShadow, true)
		} else {
			TextSetString(HP_Overlay,"Health: " GetPlayerHealth() "%")
		}
	}
	return
}

ARM:
{
	if ( WinActive("GTA:SA:MP"))
	{
		if (ARM_Overlay == -1) {
			; 30 380      7
			ARM_Overlay := TextCreate(ARMFont, ARMFontSize, ARMFontBold, ARMFontItalic, ARMPosX, ARMPosY, ARMColor, "Armor: " GetPlayerArmor() "%", ARMTextShadow, true)
		} else {
			TextSetString(ARM_Overlay,"Armor: " GetPlayerArmor() "%")
		}
	}
	return
}

WEAP:
{
	if ( WinActive("GTA:SA:MP"))
	{
			GetPlayerWeaponName(GetPlayerWeaponSlot(), WEAPON)
			TCLIPS := GetPlayerWeaponTotalClip(GetPlayerWeaponSlot())
			CLIPS := GetPlayerWeaponClip(GetPlayerWeaponSlot())
			if (WEAP_Overlay == -1) {
				; 30 400      7
				WEAP_Overlay := TextCreate(WEAPONFont, WEAPONFontSize, WEAPONFontBold, WEAPONFontItalic, WEAPONPosX, WEAPONPosY, WEAPONColor, "Weapon: "   WEAPON  " | " CLIPS ":" TCLIPS , WEAPONTextShadow, true)
			} else {
				TextSetString(WEAP_Overlay,"Weapon: "   WEAPON  " | " CLIPS ":" TCLIPS)
			}
	}
	return
}

vHP:
{
	if ( WinActive("GTA:SA:MP"))
	{
		if (IsPlayerInAnyVehicle())
		{
			if (vHP_Overlay == -1) {
				vHP_Overlay := TextCreate(VEHFont, VEHFontSize, VEHFontBold, VEHFontItalic, VEHPosX, VEHPosY, VEHColor, "Vehicle: " GetVehicleHealth(), VEHTextShadow, true)
			} else {
				TextSetString(vHP_Overlay, "Vehicle: " GetVehicleHealth())
			}
		}
		else 
		{
			TextDestroy(vHP_Overlay)
			vHP_Overlay := -1
		}
		return
	}
	return
}

:?:/hud on::
	SetTimer, HP, 500
	SetTimer, ARM, 500
	SetTimer, WEAP, 500
	SetTimer, vHP, 500
return

:?:/hud off::
	SetTimer, HP, Off
	SetTimer, ARM, Off
	SetTimer, WEAP, Off
	SetTimer, vHP, Off
	HP_Overlay := -1
	ARM_Overlay := -1
	WEAP_Overlay := -1
	vHP_Overlay := -1
	DestroyAllVisual()
return

^1:: Pause
return
^2:: 
DestroyAllVisual()
Reload
return
^3:: DestroyAllVisual()
return