#Persistent
#MaxThreads 5
#MaxThreadsPerHotkey 5
#IfWinActive GTA:SA:MP
#Include UDF_037.ahk

; ==== [Timers]
SetTimer, CheckDrugs, Off
SetTimer, CheckAutoDrugs, Off

; ==== [global Variables]
global AutoDrugs := 0
global AutoUseTime := 0
global AutoUseHP := 0
global MaxDrugs := 0

AddChatMessageEx(0x12B3FE, "[ SCRIPT ]	������ ������� � ����� � ������.")
AddChatMessageEx(0x12B3FE, "[ SCRIPT ]	��� ������ ������ ������� ������� /scrhelp")

$~Enter:: 
if (isInChat() = 1) and (not isDialogOpen()) and (not UseAlternative) 
{ 
	BlockChatInput() 
	sleep 250 
	dwAddress := dwSAMP + 0x12D8F8 
	chatInput := readString(hGTA, dwAddress, 256) 
	if ( InStr(chatInput, "/") ) { 
		if chatInput contains /usedrugs,/audrugs,/scrhelp
		{
			unBlockChatInput() 
			if (StrLen(chatInput) > 0 ) { 
				params := StrSplit(chatInput, " ")
				command := params[1]
				params.Remove(1)
				SendCommand(command, params)
			} 
			dwAddress := dwSAMP + 0x12D8F8 
			writeString(hGTA, dwAddress, "") 
			AntiCrash() 
		} else { 
			unBlockChatInput() 
			SendChat(chatInput)
		}
	} else { 
		unBlockChatInput() 
		SendChat(chatInput) 
		
	}
}
return

SendCommand(command, params) {
	if (command == "/usedrugs") {
		SendChat(command " " params[1])
		SetTimer, CheckDrugs, 60000
	} else if ( command == "/audrugs") {
		if (params[1] == "on") {
			if(params[2] == "" || params[2] == "0" ) 
				AutoUseTime := 60000
			else AutoUseTime := params[2]*60000
			AutoDrugs := 1
			SetTimer, CheckAutoDrugs, %AutoUseTime%
			AddChatMessageEx(0x12B3FE, "[Script]	�������� ���� ������������� �����(1 ��) ����� �������������: " params[2] " ���.")
		} else if (params[1] == "off") {
			AutoDrugs := 0
			SetTimer, CheckAutoDrugs, Off
			AddChatMessageEx(0x12B3FE, "[Script]	��������� ���� ������������� �����!")
		} else if (params[1] == "low") {
			if (params[2] == "" || params[2] == "0" || StrLen(params[2]) > 2 ) 
				AutoUseHP := 40
			else AutoUseHP := params[2]
			if (params[3] == "" || params[3] == "0" || StrLen(params[3]) > 2 ) 
				MaxDrugs := 3
			else MaxDrugs := params[3]
				
			AutoDrugs := 2
			SetTimer, CheckAutoDrugs, 500
			AddChatMessageEx(0x12B3FE, "[Script]	�������� ���� ������������� ����� ��� ����� ��������! �������� ��: " AutoUseHP)
		}
	} else if(command == "/scrhelp") {
		Sleep, 200
		showDialog(0, "������ �� ������� [Auto-Drugs]", "{ffffff}/usedrugs [1-15] - {12B3FE}����������� ������������� ����� � ����������� ����� ������`n{ffffff}/audrugs on [1-59] - {12B3FE}������������� 1 �� ����� ����� ������ ��������� ���-�� �����`n`n{ffffff}/audrugs low [1-99] [1-15] - {12B3FE}������������� ���������� ���-�� ����� ���� �������� HP ����� ������ ����������`n{ffffff}/audrugs off - {12B3FE}���������� ���� ������������� �����`n`n`t`t{ffffff}Created By {12B3FE}FordeD aka McFree", "�������")
	}
}


CheckDrugs:
AddChatMessageEx(0x12B3FE, "[Script]	�� ������ ����� ������� �����.")
showGameText("~y~You ~w~Can ~n~ ~g~use DRUGS", 2000, 1)
SetTimer, CheckDrugs, Off
return

CheckAutoDrugs:
if (AutoDrugs == 1) {
	SendChat("/usedrugs 1")
	AddChatMessageEx(0x12B3FE, "[Script]	���� ������������ ���� �����.")
} else if (AutoDrugs == 2) {
	if (getPlayerHealth() != 0 && getPlayerHealth() <= AutoUseHP) {
		SendChat("/usedrugs " MaxDrugs)
		AddChatMessageEx(0x12B3FE, "[Script]	�� ���� �������� ��������!")
	}
}
return