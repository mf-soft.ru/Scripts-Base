#Include API.ahk
#SingleInstance Force
SetTimer, Overlay, Off
Clock_Overlay := -1
Time := 0
IniRead, AutoOn, Settings.ini, Main, AutoOn , 1
IniRead, PosX, Settings.ini, Main, PosX, 30
IniRead, PosY, Settings.ini, Main, PosY, 360
IniRead, TimeColor, Settings.ini, Main, Color , 0xFFFFFF00
IniRead, TimeFont, Settings.ini, Main, Font, "Tahoma"
IniRead, TimeFontSize, Settings.ini, Main, FontSize, 15
IniRead, TimeFontBold, Settings.ini, Main, FontBold, "false"
IniRead, TimeFontItalic, Settings.ini, Main, FontItalic, "false"
IniRead, TimeTextShadow, Settings.ini, Main, TextShadow, "true"
IniRead, MessageColor, Settings.ini, Main, MessageColor , 0xFF00d900
IniRead, TempAutoOn, Settings.ini, Main, tempautostart, 0
if (AutoOn = 1 ||  TempAutoOn = 1) {
	TextDestroy(Clock_Overlay)
	SetTimer, Overlay, 1000
	Time := 1
	IniDelete, Settings.ini, Main, tempautostart
}


GetCommand(Time, command, params*) {
	if (command = "/times") {
		if (params[1] == "start") {
			if (Time = 0) {
				SetTimer, Overlay, 1000
				Time = 1
				AddChatMessage(0xFFFF00, "[Clock]					���� ��������!")
			}
		}
		if (params[1] == "stop") {
			if (Time = 1) {
				SetTimer, Overlay, Off
				TextDestroy(Clock_Overlay)
				Clock_Overlay := -1
				Time = 0
				AddChatMessage(0xFFFF00, "[Clock]					���� ���������!")
			}
		}
		if (params[1] == "reload") {
			AddChatMessage(0xFFFF00, "[Clock]					������������ �������!")
			TextDestroy(Clock_Overlay)
			Reload
		}
		if (params[1] == "posx") {
			if(params[2] >= 0) {
				res := params[2]
				IniWrite, %res%, Settings.ini, Main, PosX
				AddChatMessage(0xFFFF00, "[Clock]					���������� X �������� ��: " res "!")
				TextDestroy(Clock_Overlay)
				IniWrite, 1, Settings.ini, Main, tempautostart
				Reload
			}
		}
		if (params[1] == "posy") {
			if(params[2] >= 0) {
				res := params[2]
				IniWrite, %res%, Settings.ini, Main, PosY
				AddChatMessage(0xFFFF00, "[Clock]					���������� Y �������� ��: " res "!")
				TextDestroy(Clock_Overlay)
				IniWrite, 1, Settings.ini, Main, tempautostart
				Reload
			}
		}
		if (params[1] == "size") {
			if(params[2] >= 1) {
				res := params[2]
				IniWrite, %res%, Settings.ini, Main, FontSize
				AddChatMessage(0xFFFF00, "[Clock]					������ ����� ������� ��: " res "!")
				TextDestroy(Clock_Overlay)
				IniWrite, 1, Settings.ini, Main, tempautostart
				Reload
			}
		}
		if (params[1] == "font") {
			if(params[2] != "") {
				res := params[2]
				IniWrite, %res%, Settings.ini, Main, Font
				AddChatMessage(0xFFFF00, "[Clock]					����� ������� �� �������� ��: " res "!")
				TextDestroy(Clock_Overlay)
				IniWrite, 1, Settings.ini, Main, tempautostart
				Reload
			}
		}
		if (params[1] == "color") {
			if(InStr(params[2], "0x") = 1 ) {
				res := params[2]
				IniWrite, %res%, Settings.ini, Main, Color
				AddChatMessage(0xFFFF00, "[Clock]					���� ����� �������� ��: " res "!")
				TextDestroy(Clock_Overlay)
				IniWrite, 1, Settings.ini, Main, tempautostart
				Reload
			}
		}
		if (params[1] == "autoon") {
			if(params[2] == 1 ||  params[2] == 0) {
				res := params[2]
				IniWrite, %res%, Settings.ini, Main, AutoOn
				AddChatMessage(0xFFFF00, "[Clock]					�������������� ��������� ����� ��������!")
				TextDestroy(Clock_Overlay)
				IniWrite, 1, Settings.ini, Main, tempautostart
				Reload
			}
		}
	}
}

~F6:: 
Input , chattext, V, {Enter}{ESC} 
if ErrorLevel = EndKey:Enter 
{ 
    if InStr(chattext, "/") = 1 
    { 
		 if StrLen(chattext) > 0 
		 { 
			StringSplit, param, chattext,%A_Space% 
			params := RegExReplace(chattext, param1 . " ", "") 
			StringSplit, params, params,%A_Space% 
			params := RegExReplace(params, params1 . " ", "") 
			sleep 100 
			GetCommand(Time, param1, params1, params ) 
		 } 
    } 
} 
return 

Overlay:
{
	if ( WinActive("GTA:SA:MP"))
	{
		StringTime = %A_Hour%:%A_Min%:%A_Sec%
		if (Clock_Overlay == -1) {
			Clock_Overlay := TextCreate(TimeFont, TimeFontSize, %TimeFontBold%, %TimeFontItalic%, PosX, PosY, TimeColor, StringTime, %TimeTextShadow%, true)
		} else {
			TextSetString(Clock_Overlay, StringTime)
		}
	}
	return
}
return
