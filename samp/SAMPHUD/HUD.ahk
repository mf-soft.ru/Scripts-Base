#SingleInstance Force
#Include SAMPMAXnew.ahk
#MaxThreads 7
SetTimer, HP, Off
SetTimer, ARM, Off
SetTimer, WEAP, Off
SetTimer, vHP, Off
SetTimer, Overlay, Off
HP_Overlay := -1
ARM_Overlay := -1
WEAP_Overlay := -1
vHP_Overlay := -1
Clock_Overlay := -1
PLAYER_Overlay := -1

;IniSettings
;
;			MAIN
;
IniRead, AutoOn, Settings.ini, Main, AutoOn , 0
IniRead, MessageColor, Settings.ini, Main, MessageColor , 0xFF00d900
;
;			HEALTH
;
IniRead, HPUpdate, Settings.ini, Health, Update, 200
IniRead, HPPosX, Settings.ini, Health, PosX, 30
IniRead, HPPosY, Settings.ini, Health, PosY, 360
IniRead, HPColor, Settings.ini, Health, Color , 0xFFFFFF00
IniRead, HPFont, Settings.ini, Health, Font, "Tahoma"
IniRead, HPFontSize, Settings.ini, Health, FontSize, 7
IniRead, HPFontBold, Settings.ini, Health, FontBold, "false"
IniRead, HPFontItalic, Settings.ini, Health, FontItalic, "false"
IniRead, HPTextShadow, Settings.ini, Health, TextShadow, "true"
;
;			ARMOR
;
IniRead, ARMUpdate, Settings.ini, Armor, Update, 200
IniRead, ARMPosX, Settings.ini, Armor, PosX, 30
IniRead, ARMPosY, Settings.ini, Armor, PosY, 380
IniRead, ARMColor, Settings.ini, Armor, Color , 0xFFFFFF00
IniRead, ARMFont, Settings.ini, Armor, Font, "Tahoma"
IniRead, ARMFontSize, Settings.ini, Armor, FontSize, 7
IniRead, ARMFontBold, Settings.ini, Armor, FontBold, "false"
IniRead, ARMFontItalic, Settings.ini, Armor, FontItalic, "false"
IniRead, ARMTextShadow, Settings.ini, Armor, TextShadow, "true"
;
;			WEAPON
;
IniRead, WEAPONUpdate, Settings.ini, Weapon, Update, 150
IniRead, WEAPONPosX, Settings.ini, Weapon, PosX, 30
IniRead, WEAPONPosY, Settings.ini, Weapon, PosY, 400
IniRead, WEAPONColor, Settings.ini, Weapon, Color , 0xFFFFFF00
IniRead, WEAPONFont, Settings.ini, Weapon, Font, "Tahoma"
IniRead, WEAPONFontSize, Settings.ini, Weapon, FontSize, 7
IniRead, WEAPONFontBold, Settings.ini, Weapon, FontBold, "false"
IniRead, WEAPONFontItalic, Settings.ini, Weapon, FontItalic, "false"
IniRead, WEAPONTextShadow, Settings.ini, Weapon, TextShadow, "true"
IniRead, WEAPONShow, Settings.ini, Weapon, Show , "true"
;
;			VEHICLE
;
IniRead, VEHUpdate, Settings.ini, Vehicle, Update, 200
IniRead, VEHPosX, Settings.ini, Vehicle, PosX, 30
IniRead, VEHPosY, Settings.ini, Vehicle, PosY, 420
IniRead, VEHColor, Settings.ini, Vehicle, Color , 0xFFFFFF00
IniRead, VEHFont, Settings.ini, Vehicle, Font, "Tahoma"
IniRead, VEHFontSize, Settings.ini, Vehicle, FontSize, 7
IniRead, VEHFontBold, Settings.ini, Vehicle, FontBold, "false"
IniRead, VEHFontItalic, Settings.ini, Vehicle, FontItalic, "false"
IniRead, VEHTextShadow, Settings.ini, Vehicle, TextShadow, "true"
IniRead, VEHShow, Settings.ini, Vehicle, Show , "true"
;
;			BOX
;
IniRead, BOXPosX, Settings.ini, Box, PosX, 30
IniRead, BOXPosY, Settings.ini, Box, PosY, 340
IniRead, BOXWidth, Settings.ini, Box, Width, 150
IniRead, BOXHeight, Settings.ini, Box, Heigtht, 100
IniRead, BOXColor, Settings.ini, Box, Color , 0xFF555555
IniRead, BOXShow, Settings.ini, Box, Show , "true"
;
;			CLOCK
;
IniRead, TimeUpdate, Settings.ini, Clock, Update, 1000
IniRead, TimePosX, Settings.ini, Clock, PosX, 30
IniRead, TimePosY, Settings.ini, Clock, PosY, 360
IniRead, TimeColor, Settings.ini, Clock, Color , 0xFFFFFF00
IniRead, TimeFont, Settings.ini, Clock, Font, "Tahoma"
IniRead, TimeFontSize, Settings.ini, Clock, FontSize, 15
IniRead, TimeFontBold, Settings.ini, Clock, FontBold, "false"
IniRead, TimeFontItalic, Settings.ini, Clock, FontItalic, "false"
IniRead, TimeTextShadow, Settings.ini, Clock, TextShadow, "true"
;
;			PLAYER
;
IniRead, PlayerUpdate, Settings.ini, Player, Update, 10000
IniRead, PlayerPosX, Settings.ini, Player, PosX, 30
IniRead, PlayerPosY, Settings.ini, Player, PosY, 360
IniRead, PlayerShow, Settings.ini, Player, Show, "true"
IniRead, PlayerColor, Settings.ini, Player, Color , 0xFFFFFF00
IniRead, PlayerFont, Settings.ini, Player, Font, "Tahoma"
IniRead, PlayerFontSize, Settings.ini, Player, FontSize, 15
IniRead, PlayerFontBold, Settings.ini, Player, FontBold, "false"
IniRead, PlayerFontItalic, Settings.ini, Player, FontItalic, "false"
IniRead, PlayerTextShadow, Settings.ini, Player, TextShadow, "true"

if (AutoOn == 1) {
	SetTimer, HP, %HPUpdate%
	SetTimer, ARM, %ARMUpdate%
	SetTimer, WEAP, %WEAPONUpdate%
	SetTimer, vHP, %VEHUpdate%
	SetTimer, Overlay, %TimeUpdate%
	SetTimer, Player, %PlayerUpdate%
}
PlayerName := getUsername()

BOX_Overlay := BoxCreate(BOXPosX,BOXPosY,BOXWidth,BOXHeight,BOXColor,%BOXShow%)
return

Overlay:
{
	if ( WinActive("GTA:SA:MP"))
	{
		StringTime = %A_Hour%:%A_Min%:%A_Sec%
		if (Clock_Overlay == -1) {
			Clock_Overlay := TextCreate(TimeFont, TimeFontSize, %TimeFontBold%, %TimeFontItalic%, TimePosX, TimePosY, TimeColor, StringTime, %TimeTextShadow%, true)
		} else {
			TextSetString(Clock_Overlay, StringTime)
		}
	}
	return
}
return
HP:
{
	if ( WinActive("GTA:SA:MP"))
	{
		if (HP_Overlay == -1) {
			HP_Overlay := TextCreate(HPFont, HPFontSize, %HPFontBold%, %HPFontItalic%, HPPosX, HPPosY, HPColor, "Health: " GetPlayerHealth() "%", %HPTextShadow%, true)
		} else {
			TextSetString(HP_Overlay,"Health: " GetPlayerHealth() "%")
		}
	}
	return
}

ARM:
{
	if ( WinActive("GTA:SA:MP"))
	{
		if (ARM_Overlay == -1) {
			; 30 380      7
			ARM_Overlay := TextCreate(ARMFont, ARMFontSize, %ARMFontBold%, %ARMFontItalic%, ARMPosX, ARMPosY, ARMColor, "Armor: " GetPlayerArmor() "%", %ARMTextShadow%, true)
		} else {
			TextSetString(ARM_Overlay,"Armor: " GetPlayerArmor() "%")
		}
	}
	return
}

WEAP:
{
	if ( WinActive("GTA:SA:MP"))
	{
			GetPlayerWeaponNameDLL(GetPlayerWeaponSlotDLL(), WEAPON)
			TCLIPS := GetPlayerWeaponTotalClipDLL(GetPlayerWeaponSlotDLL())
			CLIPS := GetPlayerWeaponClipDLL(GetPlayerWeaponSlotDLL())
			if (WEAP_Overlay == -1) {
				; 30 400      7
				WEAP_Overlay := TextCreate(WEAPONFont, WEAPONFontSize, %WEAPONFontBold%, %WEAPONFontItalic%, WEAPONPosX, WEAPONPosY, WEAPONColor, "Weapon: "   WEAPON  " | " CLIPS ":" TCLIPS , %WEAPONTextShadow%, %WEAPONShow%)
			} else {
				TextSetString(WEAP_Overlay,"Weapon: "   WEAPON  " | " CLIPS ":" TCLIPS)
			}
	}
	return
}

vHP:
{
	if ( WinActive("GTA:SA:MP"))
	{
		if (IsPlayerInAnyVehicle())
		{
			GetVehicleModelNameDLL(Vehicle)
			if (vHP_Overlay == -1) {
				vHP_Overlay := TextCreate(VEHFont, VEHFontSize, %VEHFontBold%, %VEHFontItalic%, VEHPosX, VEHPosY, VEHColor, "" Vehicle ": " GetVehicleHealth(), %VEHTextShadow%, %VEHShow%)
			} else {
				TextSetString(vHP_Overlay, "" Vehicle ": " GetVehicleHealth())
			}
		}
		else 
		{
			TextDestroy(vHP_Overlay)
			vHP_Overlay := -1
		}
		return
	}
	return
}
Player:
{
	if ( WinActive("GTA:SA:MP"))
	{
		PlayerName := getUsername()
		;getPlayerColor(id)
		if (PLAYER_Overlay == -1) {
			PLAYER_Overlay := TextCreate(PlayerFont, PlayerFontSize, %PlayerFontBold%, %PlayerFontItalic%, PlayerPosX, PlayerPosY, PlayerColor, PlayerName "["getId()"]", %PlayerTextShadow%, %PlayerShow%)
		} else {
			TextSetString(PLAYER_Overlay, PlayerName "["getId()"]")
		}
	}
	return
}

:?:/hud on::
	SetTimer, HP, %HPUpdate%
	SetTimer, ARM, %ARMUpdate%
	SetTimer, WEAP, %WEAPONUpdate%
	SetTimer, vHP, %VEHUpdate%
	SetTimer, Overlay, %TimeUpdate%
	SetTimer, Player, %PlayerUpdate%
	PlayerName := getUsername()
	BOX_Overlay := BoxCreate(BOXPosX,BOXPosY,BOXWidth,BOXHeight,BOXColor,%BOXShow%)
	PLAYER_Overlay := TextCreate(PlayerFont, PlayerFontSize, %PlayerFontBold%, %PlayerFontItalic%, PlayerPosX, PlayerPosY, PlayerColor, PlayerName "["getId()"]", %PlayerTextShadow%, %PlayerShow%)
return

:?:/hud off::
	SetTimer, HP, Off
	SetTimer, ARM, Off
	SetTimer, WEAP, Off
	SetTimer, vHP, Off
	SetTimer, Overlay, Off
	SetTimer, Player, Off
	HP_Overlay := -1
	ARM_Overlay := -1
	WEAP_Overlay := -1
	vHP_Overlay := -1
	BOX_Overlay := -1
	Clock_Overlay := -1
	PLAYER_Overlay := -1
	DestroyAllVisual()
return

^1:: Pause
return
^2:: 
DestroyAllVisual()
Reload
return
^3:: DestroyAllVisual()
return