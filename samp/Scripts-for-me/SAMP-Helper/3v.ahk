﻿#include SAMPMAXnew.ahk
#Persistent
#MaxThreads 3
!1::
Loop
{
 Str := ProcessReadMemory(0x04BDA906, "gta_sa.exe", "Str", 126)
 IfInString, Str, Daniel_Fornazel
 {
  StringReplace, Str, Str, Daniel_Fornazel, Oleg_Koks
  ProcessWriteMemory(Str ,0x04BDA906, "gta_sa.exe", "Str", 126)
 } else {
  Str := ProcessReadMemory(0x04BDA80A, "gta_sa.exe", "Str", 126)
  IfInString, Str, Daniel_Fornazel
  {
   StringReplace, Str, Str, Daniel_Fornazel, Oleg_Koks
   ProcessWriteMemory(Str ,0x04BDA80A, "gta_sa.exe", "Str", 126)
  }
 }
 Sleep, 10
}
return

ProcessReadMemory(address, processIDorName, type := "Int", numBytes := 4) {
    VarSetCapacity(buf, numBytes, 0)

    Process Exist, %processIDorName%

    if !processHandle := DllCall("OpenProcess", "Int", 24, "UInt", 0, "UInt", processID, "Ptr")
        throw Exception("Failed to open process.`n`nError code:`t" . A_LastError)

    result := DllCall("ReadProcessMemory", "Ptr", processHandle, "Ptr", address, "Ptr", &buf, "Ptr", numBytes, "PtrP", numBytesRead, "UInt")

    if !DllCall("CloseHandle", "Ptr", processHandle, "UInt") && !result
        throw Exception("Failed to close process handle.`n`nError code:`t" . A_LastError)

    if !result
        throw Exception("Failed to read process memory.`n`nError code:`t" . A_LastError)
    
    return (type = "Str")
        ? StrGet(&buf, numBytes)
        : NumGet(buf, type)
}

ProcessWriteMemory(data, address, processIDorName, type := "Int", numBytes := 4) {
    VarSetCapacity(buf, numBytes, 0)
    (type = "Str")
        ? StrPut(data, &buf, numBytes)
        : NumPut(data, buf, type)

    Process Exist, %processIDorName%

    if !processHandle := DllCall("OpenProcess", "Int", 40, "UInt", 0, "UInt", processID, "Ptr")
        throw Exception("Failed to open process.`n`nError code:`t" . A_LastError)

    result := DllCall("WriteProcessMemory", "Ptr", processHandle, "Ptr", address, "Ptr", &buf, "Ptr", numBytes, "UInt", 0, "UInt")

    if !DllCall("CloseHandle", "Ptr", processHandle, "UInt") && !result
        throw Exception("Failed to close process handle.`n`nError code:`t" . A_LastError)

    if !result
        throw Exception("Failed to write process memory.`n`nError code:`t" . A_LastError)

    return result
}