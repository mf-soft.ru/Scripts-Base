;351D362F
#Persistent
IniRead, ADDRESS, Settings.ini, Main, Address

!1::
Loop
{
	Str := ProcessReadMemory(ADDRESS, "grand_theft_auto_san_andreas.dll", "Str", 10)
	IfInString, Str, PRESS NUM4
	{
		SendInput, {Numpad4 Down}
		Sleep, 30
		SendInput, {Numpad4 Up}
		Sleep, 150
		;SendInput, {f6}/b num4{enter}
	} 
	IfInString, Str, PRESS NUM6
	{
		SendInput, {Numpad6 Down}
		Sleep, 30
		SendInput, {Numpad6 Up}
		Sleep, 150
		;SendInput, {f6}/b num6{enter}
	} 
	Sleep, 10
}
return

!2::
Reload
return

!3::
IniWrite, 0, Settings.ini, Main, Address
return

ProcessReadMemory(address, processIDorName, type := "Int", numBytes := 4) {
    VarSetCapacity(buf, numBytes, 0)

    Process Exist, %processIDorName%
    if !processID := ErrorLevel
        throw Exception("Invalid process name or process ID:`n`n""" . processIDorName . """")

    if !processHandle := DllCall("OpenProcess", "Int", 24, "UInt", 0, "UInt", processID, "Ptr")
        throw Exception("Failed to open process.`n`nError code:`t" . A_LastError)

    result := DllCall("ReadProcessMemory", "Ptr", processHandle, "Ptr", address, "Ptr", &buf, "Ptr", numBytes, "PtrP", numBytesRead, "UInt")

    if !DllCall("CloseHandle", "Ptr", processHandle, "UInt") && !result
        throw Exception("Failed to close process handle.`n`nError code:`t" . A_LastError)

    if !result
        throw Exception("Failed to read process memory.`n`nError code:`t" . A_LastError)

    if !numBytesRead
        throw Exception("Read 0 bytes from the`n`nprocess:`t" . processIDorName . "`naddress:`t" . address)

    return (type = "Str")
        ? StrGet(&buf, numBytes)
        : NumGet(buf, type)
}
