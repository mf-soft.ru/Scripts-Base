#Include SAMPMAXnew.ahk

#Persistent 
SetFormat, float, 0.0
Players := []

^1::
dout:=""
Players := getStreamedInPlayersInfo()
p := 0
l:= []
For i, o in Players
{	
	l:= getPedCoordinates(o.PED)
	p++
	 pos := getCoordinates()
	NColor := IntToHex(getPlayerColor(i))
	NColor := SubStr(NColor, 3, 6)
	Name := getPlayerNameById(i)
	Dist := getDist(getCoordinates() ,l)
	HP :=Floor(o.HP)
	ARM :=Floor(o.ARMOR)
	dout .= "{" NColor "}" Name  "{f1f1f1}`tID: " i "`t" Dist "m.`tHP: " HP "`tARM: " ARM "`n"
}
 showDialog(4, "Players", dout, "Close", "OK", 0)
return

^2::
Reload
return

