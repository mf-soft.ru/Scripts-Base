#IfWinActive GTA:SA:MP
#Include API.ahk

IniRead, AutoOn, Settings.ini, Main, AutoOn , 0
IniRead, PosX, Settings.ini, Main, PosX, 30
IniRead, PosY, Settings.ini, Main, PosY, 360
IniRead, TimeColor, Settings.ini, Main, Color , 0xFFFFFF00
IniRead, TimeFont, Settings.ini, Main, Font, "Tahoma"
IniRead, TimeFontSize, Settings.ini, Main, FontSize, 15
IniRead, TimeFontBold, Settings.ini, Main, FontBold, "false"
IniRead, TimeFontItalic, Settings.ini, Main, FontItalic, "false"
IniRead, TimeTextShadow, Settings.ini, Main, TextShadow, "true"
IniRead, MessageColor, Settings.ini, Main, MessageColor , 0xFF00d900

SetTimer, CheckPlayer, 600
SetTimer, CheckVeh, 600
return

CheckPlayer:
if ( WinActive("GTA:SA:MP"))
{
	DestroyAllVisual()
	HP_Overlay := TextCreate(Arial, 8, true, false, 23, 400, 0xF62020FF , "HP: " GetPlayerHealth(), false, true)
	ARM_Overlay := TextCreate(Arial, 8, true, false, 23, 410, 0xF62020FF, "ARM: " GetPlayerArmor(), false, true)
	LineCreate(10,420,120,420,10,0xF62020FF ,1)
}
 return
 
CheckVeh:
 if(IsPlayerDriver()) {
	TextCreate(Arial, 8, true, false, 23, 380, 0xF62020FF, "Car: " GetVehicleHealth(), false, true)
	LineCreate(10,390,120,390,10,0xF62020FF ,1)
}
 return

^1:: Pause
return
^2:: Reload
return