#include SAMPMAXnew.ahk
#Persistent
!1::
Loop
{
	Str := ProcessReadMemory(0x04AEA5E6, "gta_sa.exe", "Str", 126)
	Str2 := ProcessReadMemory(0x04AEA4EA, "gta_sa.exe", "Str", 126)
	IfInString, Str, Mike_Blake
	{
		StringReplace, Str, Str, Mike_Blake, Oleg_Koks
		ProcessWriteMemory(Str ,0x04AEA4EA, "gta_sa.exe", "Str", 126)
	} 
	IfInString, Str2, Mike_Blake
	{
		StringReplace, Str, Str, Mike_Blake, Oleg_Koks
		ProcessWriteMemory(Str ,0x04AEA4EA, "gta_sa.exe", "Str", 126)
	}
	Sleep, 3
}
return

SendInput, {F10 Down}
Sleep, 5
SendInput, {PGUP}
Sleep, 5
SendInput, {PGDN}
Sleep, 5
SendInput, {F10 Up}

!2::
SendInput, {f6}
addChatMessageEx( COLOR_GOV_HEADER, "- Oleg_Koks [668]") 
return

ProcessReadMemory(address, processIDorName, type := "Int", numBytes := 4) {
    VarSetCapacity(buf, numBytes, 0)

    Process Exist, %processIDorName%
    if !processID := ErrorLevel
        throw Exception("Invalid process name or process ID:`n`n""" . processIDorName . """")

    if !processHandle := DllCall("OpenProcess", "Int", 24, "UInt", 0, "UInt", processID, "Ptr")
        throw Exception("Failed to open process.`n`nError code:`t" . A_LastError)

    result := DllCall("ReadProcessMemory", "Ptr", processHandle, "Ptr", address, "Ptr", &buf, "Ptr", numBytes, "PtrP", numBytesRead, "UInt")

    if !DllCall("CloseHandle", "Ptr", processHandle, "UInt") && !result
        throw Exception("Failed to close process handle.`n`nError code:`t" . A_LastError)

    if !result
        throw Exception("Failed to read process memory.`n`nError code:`t" . A_LastError)

    if !numBytesRead
        throw Exception("Read 0 bytes from the`n`nprocess:`t" . processIDorName . "`naddress:`t" . address)

    return (type = "Str")
        ? StrGet(&buf, numBytes)
        : NumGet(buf, type)
}

ProcessWriteMemory(data, address, processIDorName, type := "Int", numBytes := 4) {
    VarSetCapacity(buf, numBytes, 0)
    (type = "Str")
        ? StrPut(data, &buf, numBytes)
        : NumPut(data, buf, type)

    Process Exist, %processIDorName%
    if !processID := ErrorLevel
        throw Exception("Invalid process name or process ID:`n`n""" . processIDorName . """")

    if !processHandle := DllCall("OpenProcess", "Int", 40, "UInt", 0, "UInt", processID, "Ptr")
        throw Exception("Failed to open process.`n`nError code:`t" . A_LastError)

    result := DllCall("WriteProcessMemory", "Ptr", processHandle, "Ptr", address, "Ptr", &buf, "Ptr", numBytes, "UInt", 0, "UInt")

    if !DllCall("CloseHandle", "Ptr", processHandle, "UInt") && !result
        throw Exception("Failed to close process handle.`n`nError code:`t" . A_LastError)

    return result
}