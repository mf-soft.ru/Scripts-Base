; <COMPILER: v1.1.22.09>
#SingleInstance, force
#NoEnv
#Include SAMPMAXnew.ahk
WindowTitle2 := "GTA:SA:MP"
gta := new memory(WindowTitle2)
HP_Overlay := -1
TargetHP := 0
Loop
{
	if (TargetHP > 0)
	{
		if ( WinActive("GTA:SA:MP"))
		{
			if (HP_Overlay == -1) {
				HP_Overlay := TextCreate("Tahoma", 7, "false", "false", 30, 420, 0xFFFFFF00, "Health: " TargetHP , "true", "true")
			} else {
				TextSetString(HP_Overlay,"Health: " TargetHP)
			}
		}
		Sleep, 100
	}
	else {
		Sleep, 100
		TextDestroy(HP_Overlay)
		HP_Overlay := -1
	}
}
return
~$vk02::
While GetKeyState("vk01", "P")
{
	Targetped := gta.read(0xB6F5F0, "UInt", 0x79c)
	if (GetPlayerWeaponIdDLL(GetPlayerWeaponSlotDLL()) > 0)
	{
		if (getTargetVehicleModelIdByPed(Targetped) > 0)
		{
			TargetHP := getTargetVehicleHealthByPed(Targetped)
		}
		else {
			TargetHP := 0
		}
		
	}
}
return

class memory
{
	static baseAddress, hProcess
	, insertNullTerminator := True
	, readChunkSize := 128
	, aTypeSize := {	"UChar": 1, "Char":	1
	, "UShort":	2, "Short":	2
	, "UInt": 4, "Int": 4
	, "UFloat": 4, "Float": 4
	,	"Int64": 8, "Double": 8}
	__new(program, dwDesiredAccess := "", byRef handle := "", windowMatchMode := 3)
	{
		if !(handle := this.openProcess(program, dwDesiredAccess, windowMatchMode))
		return ""
		this.BaseAddress := this.getProcessBaseAddress(program, windowMatchMode)
		return this
	}
	__delete()
	{
		this.closeProcess(this.hProcess)
		return
	}
	openProcess(program, dwDesiredAccess := "", windowMatchMode := 3)
	{
		if dwDesiredAccess is not integer
		dwDesiredAccess := (PROCESS_QUERY_INFORMATION := 0x0400) | (PROCESS_VM_OPERATION := 0x8) | (PROCESS_VM_READ := 0x10) | (PROCESS_VM_WRITE := 0x20)
		if windowMatchMode
		{
			mode := A_TitleMatchMode
			SetTitleMatchMode, %windowMatchMode%
		}
		WinGet, pid, pid, % this.currentProgram := program
		if windowMatchMode
		SetTitleMatchMode, %mode%
		if !pid
		return this.hProcess := 0
		return this.hProcess := DllCall("OpenProcess", "UInt", dwDesiredAccess, "Int", False, "UInt", pid)
	}
	closeProcess(hProcess)
	{
		if hProcess
		return DllCall("CloseHandle", "UInt", hProcess)
		return
	}
	read(address, type := "UInt", aOffsets*)
	{
		VarSetCapacity(buffer, bytes := this.aTypeSize[type])
		if !DllCall("ReadProcessMemory","UInt", this.hProcess, "UInt", aOffsets.maxIndex() ? this.getAddressFromOffsets(address, aOffsets*) : address, "Ptr", &buffer, "UInt", bytes, "Ptr",0)
		return !this.hProcess ? "Handle Is closed: " this.hProcess : "Fail"
		return numget(buffer, 0, Type)
	}
	ReadRawMemory(address, byref buffer, bytes := 4, aOffsets*)
	{
		VarSetCapacity(buffer, bytes)
		if !DllCall("ReadProcessMemory", "UInt", this.hProcess, "UInt", aOffsets.maxIndex() ? this.getAddressFromOffsets(address, aOffsets*) : address, "Ptr", &buffer, "UInt", bytes, "Ptr*", bytesRead)
		return !this.hProcess ? "Handle Is closed: " this.hProcess : "Fail"
		return bytesRead
	}
	readString(address, length := 0, encoding := "utf-8", aOffsets*)
	{
		size := (encoding ="utf-16" || encoding = "cp1200") ? 2 : 1
		VarSetCapacity(buffer, length ? length * size : (this.readChunkSize < size ? this.readChunkSize := size : this.readChunkSize), 0)
		if aOffsets.maxIndex()
		address := this.getAddressFromOffsets(address, aOffsets*)
		if !length
		{
			VarSetCapacity(string, this.readChunkSize * 2)
			Loop
			{
				success := DllCall("ReadProcessMemory", "UInt", this.hProcess, "UInt", address + (A_index - 1) * this.readChunkSize, "Ptr", &buffer, "Uint", this.readChunkSize, "Ptr", 0)
				if (ErrorLevel || !success)
				{
					if (A_Index = 1 && !this.hProcess)
					return "Handle Is closed: " this.hProcess
					else if (A_index = 1 && this.hProcess)
					return "Fail"
					else
					break
				}
				loop, % this.readChunkSize / size
				{
					if ("" = char := StrGet(&buffer + (A_Index -1) * size, 1, encoding))
					break, 2
					string .= char
				}
			}
		}
		Else
		{
			if !DllCall("ReadProcessMemory", "UInt", this.hProcess, "UInt", address, "Ptr", &buffer, "Uint", length * size, "Ptr", 0)
			return !this.hProcess ? "Handle Is closed: " this.hProcess : "Fail"
			string := StrGet(&buffer, length, encoding)
		}
		return string
	}
	writeString(address, string, encoding := "utf-8", aOffsets*)
	{
		encodingSize := (encoding = "utf-16" || encoding = "cp1200") ? 2 : 1
		requiredSize := StrPut(string, encoding) * encodingSize - (this.insertNullTerminator ? 0 : encodingSize)
		VarSetCapacity(buffer, requiredSize)
		StrPut(string, &buffer, this.insertNullTerminator ? StrLen(string) : StrLen(string) + 1, encoding)
		DllCall("WriteProcessMemory", "UInt", this.hProcess, "UInt", aOffsets.maxIndex() ? this.getAddressFromOffsets(address, aOffsets*) : address, "Ptr", &buffer, "Uint", requiredSize, "Ptr*", BytesWritten)
		return BytesWritten
	}
	write(address, value, type := "Uint", aOffsets*)
	{
		if !bytes := this.aTypeSize[type]
		return "Non Supported data type"
		VarSetCapacity(buffer, bytes)
		NumPut(value, buffer, 0, type)
		return DllCall("WriteProcessMemory", "UInt", this.hProcess, "UInt", aOffsets.maxIndex() ? this.getAddressFromOffsets(address, aOffsets*) : address, "Ptr", &buffer, "Uint", bytes, "Ptr", 0)
	}
	pointer(base, finalType := "UInt", offsets*)
	{
		For index, offset in offsets
		{
			if (index = offsets.maxIndex() && A_index = 1)
			pointer := offset + this.Read(base)
			Else
			{
				IF (A_Index = 1)
				pointer := this.Read(offset + this.Read(base))
				Else If (index = offsets.MaxIndex())
				pointer += offset
				Else pointer := this.Read(pointer + offset)
			}
		}
		Return this.Read(offsets.maxIndex() ? pointer : base, finalType)
	}
	getAddressFromOffsets(address, aOffsets*)
	{
		lastOffset := aOffsets.Remove()
		return	this.pointer(address, "UInt", aOffsets*) + lastOffset
	}
	getProcessBaseAddress(WindowTitle, windowMatchMode := 3)
	{
		if windowMatchMode
		{
			mode := A_TitleMatchMode
			SetTitleMatchMode, %windowMatchMode%
		}
		WinGet, hWnd, ID, %WindowTitle%
		if windowMatchMode
		SetTitleMatchMode, %mode%
		if !hWnd
		return
		BaseAddress := DllCall(A_PtrSize = 4
		? "GetWindowLong"
		: "GetWindowLongPtr", "Ptr", hWnd, "Uint", -6, "UInt")
		return BaseAddress
	}
	getBaseAddressOfModule(module := "")
	{
		if !this.hProcess
		return -2
		if (A_PtrSize = 4)
		{
			DllCall("IsWow64Process", "Ptr", this.hProcess, "Int*", result)
			if !result
			return -4
		}
		if !module
		{
			VarSetCapacity(mainExeNameBuffer, 2048 * (A_IsUnicode ? 2 : 1))
			DllCall("psapi\GetModuleFileNameEx", "Ptr", this.hProcess, "Uint", 0
			, "Ptr", &mainExeNameBuffer, "Uint", 2048 / (A_IsUnicode ? 2 : 1))
			mainExeName := StrGet(&mainExeNameBuffer)
		}
		size := VarSetCapacity(lphModule, 4)
		loop
		{
			DllCall("psapi\EnumProcessModules", "Ptr", this.hProcess, "Ptr", &lphModule
			, "Uint", size, "Uint*", reqSize)
			if ErrorLevel
			return -3
			else if (size >= reqSize)
			break
			else
			size := VarSetCapacity(lphModule, reqSize)
		}
		VarSetCapacity(lpFilename, 2048 * (A_IsUnicode ? 2 : 1))
		loop % reqSize / A_PtrSize
		{
			DllCall("psapi\GetModuleFileNameEx", "Ptr", this.hProcess, "Uint", numget(lphModule, (A_index - 1) * A_PtrSize)
			, "Ptr", &lpFilename, "Uint", 2048 / (A_IsUnicode ? 2 : 1))
			if (!module && mainExeName = StrGet(&lpFilename) || module && instr(StrGet(&lpFilename), module))
			{
				VarSetCapacity(MODULEINFO, A_PtrSize = 4 ? 12 : 24)
				DllCall("psapi\GetModuleInformation", "Ptr", this.hProcess, "UInt", numget(lphModule, (A_index - 1) * A_PtrSize)
				, "Ptr", &MODULEINFO, "UInt", A_PtrSize = 4 ? 12 : 24)
				return numget(MODULEINFO, 0, "Ptr")
			}
		}
		return -1
	}
}