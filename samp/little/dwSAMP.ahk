#include SAMPMAXnew.ahk
#Persistent
dwPtr := dwSAMP + ADDR_SAMP_CHATMSG_PTR
dwAddress := readDWORD(hGTA,dwPtr)
; 0x152, offset for first msg
; 0xFC, size of a msg
; 99, max msg

!1::
Loop
{
    ;Str := readString(hGTA, dwAddress + 0x152 + ( 99 * 0xFC), 128)
    ;Str1 := readString(hGTA, dwAddress + 0x152 + ( 9 * 0xFC), 128)
	Str := ProcessReadMemory(dwAddress + 0x152 + ( 99 * 0xFC), "gta_sa.exe", "Str", 126)
    Str1 := ProcessReadMemory(dwAddress + 0x152 + ( 98 * 0xFC), "gta_sa.exe", "Str", 126)
    addChatMessage(dwAddress)
	IfInString, Str, Mike_Blake
	{
		StringReplace, Str, Str, Mike_Blake, Aleksandr_Kusak
        StringReplace, Str, Str, ������, �����
        SendInput, {F10 Down}
        Sleep, 5
        writeString(hGTA, dwAddress + 0x152 + ( 99 * 0xFC), Str)
		;ProcessWriteMemory(Str ,dwAddress + 0x152 + ( 99 * 0xFC), "gta_sa.exe", "Str", 126)
        SendInput, {PGUP}
        Sleep, 5
        SendInput, {PGDN}
        Sleep, 5
        SendInput, {F10 Up}
	}
    IfInString, Str1, Daniel_Fornazel
    {
        StringReplace, Str1, Str1, Mike_Blake, Aleksandr_Kusak
        StringReplace, Str1, Str1, ������, �����
        SendInput, {F10 Down}
        Sleep, 5
        writeString(hGTA, dwAddress + 0x152 + ( 98 * 0xFC), Str1)
        ;ProcessWriteMemory(Str1 ,dwAddress + 0x152 + ( 98 * 0xFC), "gta_sa.exe", "Str", 126)
        SendInput, {PGUP}
        Sleep, 5
        SendInput, {PGDN}
        Sleep, 5
        SendInput, {F10 Up}
    }
	Sleep, 5
}
return


ProcessReadMemory(address, processIDorName, type := "Int", numBytes := 4) {
    VarSetCapacity(buf, numBytes, 0)

    Process Exist, %processIDorName%
    if !processID := ErrorLevel
        throw Exception("Invalid process name or process ID:`n`n""" . processIDorName . """")

    if !processHandle := DllCall("OpenProcess", "Int", 24, "UInt", 0, "UInt", processID, "Ptr")
        throw Exception("Failed to open process.`n`nError code:`t" . A_LastError)

    result := DllCall("ReadProcessMemory", "Ptr", processHandle, "Ptr", address, "Ptr", &buf, "Ptr", numBytes, "PtrP", numBytesRead, "UInt")

    if !DllCall("CloseHandle", "Ptr", processHandle, "UInt") && !result
        throw Exception("Failed to close process handle.`n`nError code:`t" . A_LastError)

    if !result
        throw Exception("Failed to read process memory.`n`nError code:`t" . A_LastError)

    if !numBytesRead
        throw Exception("Read 0 bytes from the`n`nprocess:`t" . processIDorName . "`naddress:`t" . address)

    return (type = "Str")
        ? StrGet(&buf, numBytes)
        : NumGet(buf, type)
}

ProcessWriteMemory(data, address, processIDorName, type := "Int", numBytes := 4) {
    VarSetCapacity(buf, numBytes, 0)
    (type = "Str")
        ? StrPut(data, &buf, numBytes)
        : NumPut(data, buf, type)

    Process Exist, %processIDorName%
    if !processID := ErrorLevel
        throw Exception("Invalid process name or process ID:`n`n""" . processIDorName . """")

    if !processHandle := DllCall("OpenProcess", "Int", 40, "UInt", 0, "UInt", processID, "Ptr")
        throw Exception("Failed to open process.`n`nError code:`t" . A_LastError)

    result := DllCall("WriteProcessMemory", "Ptr", processHandle, "Ptr", address, "Ptr", &buf, "Ptr", numBytes, "UInt", 0, "UInt")

    if !DllCall("CloseHandle", "Ptr", processHandle, "UInt") && !result
        throw Exception("Failed to close process handle.`n`nError code:`t" . A_LastError)

    if !result
        throw Exception("Failed to write process memory.`n`nError code:`t" . A_LastError)

    return result
}