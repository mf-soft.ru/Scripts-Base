tpcar(x, y, z)
{
    if(!checkHandles())
        return ""
    
    cVeh := readDWORD(hGTA, 0xB79530)
    if(ErrorLevel || dwAddr==0) {
        ErrorLevel := ERROR_READ_MEMORY
        return ""
    }
    cVec := readDWORD(hGta, cVeh + 0x14)
    if(ErrorLevel || cveh==0) {
        ErrorLevel := ERROR_READ_MEMORY
        return ""
    }
    writeFloat(hGTA, cVec + 0x30, x)
    if(ErrorLevel) {
        ErrorLevel := ERROR_WRITE_MEMORY
        return ""
    }
    
    writeFloat(hGTA, cVec + 0x34, y)
    if(ErrorLevel) {
        ErrorLevel := ERROR_WRITE_MEMORY
        return ""
    }

    writeFloat(hGTA, cVec + 0x38, z)
    if(ErrorLevel) {
        ErrorLevel := ERROR_WRITE_MEMORY
        return ""
    }
    return true
}

tpped(x, y, z)
{
    if(!checkHandles())
        return ""
    
    cPed := readDWORD(hGTA, 0x0B7CD98)
    if(ErrorLevel || dwAddress==0) {
        ErrorLevel := ERROR_READ_MEMORY
        return ""
    }
    
    cVec := readDWORD(hGTA, cPed + 0x14)
    if(ErrorLevel) {
        ErrorLevel := ERROR_READ_MEMORY
        return ""
    }
    
    writeFloat(hGTA, cVec + 0x30, x)
    if(ErrorLevel) {
        ErrorLevel := ERROR_WRITE_MEMORY
        return ""
    }
    
    writeFloat(hGTA, cVec + 0x34, y)
    if(ErrorLevel) {
        ErrorLevel := ERROR_WRITE_MEMORY
        return ""
    }
    
    writeFloat(hGTA, cVec + 0x38, z)
    if(ErrorLevel) {
        ErrorLevel := ERROR_WRITE_MEMORY
        return ""
    }
    
    return true
}